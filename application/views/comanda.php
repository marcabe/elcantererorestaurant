<?php include 'layout/head.php';?>


<!--<link
	href="<?php echo base_url();?>assets/css/pages/lista_clientes.css"
rel="stylesheet">-->

<body>
	<div class="loader"></div>
	<div class="loader1"></div>

	<?php include 'layout/encabezado.php';?>
	<!--<?php include 'layout/menu.php';?>-->

	<div class="container-fluid">
		<div class="row">
			<section class="col-md-12">
				<div class="row">
					<div class="col-md-12" id="sup_top">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
									href="#lista_mesas" role="tab" aria-controls="nav-home"
									aria-selected="true">Comandas</a>
								<!--<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#pedidos"
									role="tab" aria-controls="nav-profile" aria-selected="false">Pedidos</a>-->
							</div>
						</nav>
					</div>
					<div class="col-md-12">
						<div class="tab-content" id="nav-tabContent">

							<div class="row tab-pane fade show active" id="lista_mesas" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<i class="fas fa-utensils"></i> Comanda de la mesa no. <?php echo $venta->idmesa;?>
												<input
													value="<?php echo $venta->idventa;?>"
													style="display:none;" id="idventa">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover ">
															<thead>
																<tr>
																	<td>Cant.</td>
																	<td>Producto</td>
																	<td>Precio Unitario</td>
																	<td>Comentario</td>
																	<td>Subtotal</td>
																</tr>
															</thead>
															<tbody id="lista_comanda">
															</tbody>
														</table>
													</div>
													<div class="col-md-12 text-right">
														<button class="btn btn-success btn-sm" id="mandar_pedido">Mandar
															pedido</button>
													</div>
													<div class="col-md-12 pt-5 text-right">
														<h1>Total:<label id="total"> $ 00.00</label></h1>
													</div>

												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card">
										<div class="card">
											<div class="card-header">
												<label>Adicionar</label>
											</div>
											<div class="card-body">
												<div class="row" id="section_buscarproducto">
													<div class="col-md-12">
														<div class="form-group">
															<label for="exampleInputEmail1">Buscar producto:</label>
															<input type="text" class="form-control" id="producto">
															<!--<small id="emailHelp" class="form-text text-muted">We'll
																never share your email with anyone else.</small>-->
														</div>
													</div>
													<div class="col-md-12">
														<ul id="lista_comanda" class="list show items stated additions">
															<?php foreach ($comanda as $c) { ?>
															<li class="addition">
																<div class="item">
																	<span class="count ng-binding "><?php echo $c->cantidad;?></span>
																	<strong class="item-label">
																		<span
																			class="ng-binding "><?php echo $c->producto->producto;?></span>
																	</strong>
																	<span class="price ng-binding ">$ <?php echo $c->subtotal;?></span>
																</div>
															</li>
															<?php } ?>
														</ul>
													</div>
												</div>



											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

	</div>

	<?php include 'layout/footer.php';?>

	<!--<script src="<?php echo base_url();?>assets/clocks/js/clock.js">
	</script>-->
	<script src="<?php echo base_url();?>assets/develop/js/comanda.js">
	</script>


	</html>