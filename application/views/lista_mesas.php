<?php include 'layout/head.php'; ?>


<!--<link
	href="<?php echo base_url(); ?>assets/css/pages/lista_clientes.css"
rel="stylesheet">-->

<body>

	<div class="loader"></div>
	<div class="loader1"></div>

	<?php include 'layout/encabezado_ventas.php'; ?>
	<!--<?php include 'layout/menu.php'; ?>-->

	<div class="container-fluid">
		<div class="row">
			<section class="col-md-8">
				<div class="row">
					<div class="col-md-12" id="sup_top">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-mesas-tab" data-toggle="tab" href="#lista_mesas" role="tab" aria-controls="nav-home" aria-selected="true">Mesas</a>
								<a class="nav-item nav-link" id="nav-pedidos-tab" data-toggle="tab" href="#pedidos" role="tab" aria-controls="nav-profile" aria-selected="false">Pedidos</a>
								<a class="nav-item nav-link" id="nav-corte_meseros-tab" data-toggle="tab" href="#corte_meseros" role="tab" aria-controls="nav-profile" aria-selected="false">Meseros</a>
							</div>
						</nav>
					</div>
					<div class="col-md-12">
						<div class="tab-content" id="nav-tabContent">

							<div class="row tab-pane fade show active" id="lista_mesas" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="col-md-12 fondo_card">
									<div class="card">
										<div class="card-header">
											<i class="fas fa-utensils"></i> LISTA DE MESAS
										</div>
										<div class="card-body" id="lista_mesas_sistema">
											<div class="row">
												<div class="col-md-4 pt-2 pb-1 pr-2 pl-2" style="border-color: #444; border-style: solid;">
													<div class="row">
														<div class="col-md-12 text-center">
															<h3>COCINA</h3>
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocina[0]->nomesa; ?> name="<?php echo $mesascocina[0]->status; ?>" data-content=<?php echo $mesascocina[0]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocina[0]->status == 1) ? $mesascocina[0]->imagen : $mesascocina[0]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocina[5]->nomesa; ?> name="<?php echo $mesascocina[5]->status; ?>" data-content=<?php echo $mesascocina[5]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocina[5]->status == 1) ? $mesascocina[5]->imagen : $mesascocina[5]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocina[1]->nomesa; ?> name="<?php echo $mesascocina[1]->status; ?>" data-content=<?php echo $mesascocina[1]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocina[1]->status == 1) ? $mesascocina[1]->imagen : $mesascocina[1]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocina[4]->nomesa; ?> name="<?php echo $mesascocina[4]->status; ?>" data-content=<?php echo $mesascocina[4]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocina[4]->status == 1) ? $mesascocina[4]->imagen : $mesascocina[4]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocina[2]->nomesa; ?> name="<?php echo $mesascocina[2]->status; ?>" data-content=<?php echo $mesascocina[2]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocina[2]->status == 1) ? $mesascocina[2]->imagen : $mesascocina[2]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocina[3]->nomesa; ?> name="<?php echo $mesascocina[3]->status; ?>" data-content=<?php echo $mesascocina[3]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocina[3]->status == 1) ? $mesascocina[3]->imagen : $mesascocina[3]->imagen_ocupada; ?>">
														</div>
													</div>
												</div>
												<div class="col-md-4 pt-2 pb-1 pr-3 pl-3" style="border-color: #444; border-style: solid; border-bottom-style:none;">
													<div class="row">
														<div class="col-md-12 text-center">
															<h3>BARRA</h3>
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesaspatio[0]->nomesa; ?> name="<?php echo $mesaspatio[0]->status; ?>" data-content=<?php echo $mesaspatio[0]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesaspatio[0]->status == 1) ? $mesaspatio[0]->imagen : $mesaspatio[0]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesaspatio[3]->nomesa; ?> name="<?php echo $mesaspatio[3]->status; ?>" data-content=<?php echo $mesaspatio[3]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesaspatio[3]->status == 1) ? $mesaspatio[3]->imagen : $mesaspatio[3]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesaspatio[1]->nomesa; ?> name="<?php echo $mesaspatio[1]->status; ?>" data-content=<?php echo $mesaspatio[1]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesaspatio[1]->status == 1) ? $mesaspatio[1]->imagen : $mesaspatio[1]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesaspatio[2]->nomesa; ?> name="<?php echo $mesaspatio[2]->status; ?>" data-content=<?php echo $mesaspatio[2]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesaspatio[2]->status == 1) ? $mesaspatio[2]->imagen : $mesaspatio[2]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-12 text-center">
															<h3>PATIO</h3>
														</div>
													</div>
												</div>
												<div class="col-md-4 pt-2 pb-1 pr-3 pl-3" style="border-color: #444; border-style: solid;">
													<div class="row">
														<div class="col-md-12 text-center">
															<h3>PRESIDENCIAL</h3>
														</div>
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12 text-center mt-3">
																	<img class="mesas_listas" alt=<?php echo $mesasprecidencial[3]->nomesa; ?> name="<?php echo $mesasprecidencial[3]->status; ?>" data-content=<?php echo $mesasprecidencial[3]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesasprecidencial[3]->status == 1) ? $mesasprecidencial[3]->imagen : $mesasprecidencial[3]->imagen_ocupada; ?>">
																</div>
																<div class="col-md-12 text-center mt-3">
																	<img class="mesas_listas" alt=<?php echo $mesasprecidencial[1]->nomesa; ?> name="<?php echo $mesasprecidencial[1]->status; ?>" data-content=<?php echo $mesasprecidencial[1]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesasprecidencial[1]->status == 1) ? $mesasprecidencial[1]->imagen : $mesasprecidencial[1]->imagen_ocupada; ?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12 text-center mt-3">
																	<img class="mesas_listas" alt=<?php echo $mesasprecidencial[2]->nomesa; ?> name="<?php echo $mesasprecidencial[2]->status; ?>" data-content=<?php echo $mesasprecidencial[2]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesasprecidencial[2]->status == 1) ? $mesasprecidencial[2]->imagen : $mesasprecidencial[2]->imagen_ocupada; ?>">
																</div>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesasprecidencial[0]->nomesa; ?> name="<?php echo $mesasprecidencial[0]->status; ?>" data-content=<?php echo $mesasprecidencial[0]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesasprecidencial[0]->status == 1) ? $mesasprecidencial[0]->imagen : $mesasprecidencial[0]->imagen_ocupada; ?>">
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-5 pt-2 pb-1 pr-3 pl-3" style="border-color: #444; border-style: solid;">
													<div class="row">
														<div class="col-md-12 text-center">
															<h3>REDONDA</h3>
														</div>
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12 text-center mt-3">
																	<img class="mesas_listas" alt=<?php echo $mesasredonda[2]->nomesa; ?> name="<?php echo $mesasredonda[2]->status; ?>" data-content=<?php echo $mesasredonda[2]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesasredonda[2]->status == 1) ? $mesasredonda[2]->imagen : $mesasredonda[2]->imagen_ocupada; ?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="row">
																<div class="col-md-12 text-center mt-3">
																	<img class="mesas_listas" alt=<?php echo $mesasredonda[1]->nomesa; ?> name="<?php echo $mesasredonda[1]->status; ?>" data-content=<?php echo $mesasredonda[1]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesasredonda[1]->status == 1) ? $mesasredonda[1]->imagen : $mesasredonda[1]->imagen_ocupada; ?>">
																</div>
																<div class="col-md-12 text-center mt-3">
																	<img class="mesas_listas" alt=<?php echo $mesasredonda[0]->nomesa; ?> name="<?php echo $mesasredonda[0]->status; ?>" data-content=<?php echo $mesasredonda[0]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesasredonda[0]->status == 1) ? $mesasredonda[0]->imagen : $mesasredonda[0]->imagen_ocupada; ?>">
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-2 pt-2 pb-1 pr-3 pl-3" style="border-color: #444; border-style: solid; border-top-style:none; border-bottom-style:none;">
												</div>
												<div class="col-md-5 pt-2 pb-1 pr-3 pl-3" style="border-color: #444; border-style: solid;">
													<div class="row">
														<div class="col-md-12 text-center">
															<h3>CUARTITO</h3>
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocuartito[3]->nomesa; ?> name="<?php echo $mesascocuartito[3]->status; ?>" data-content=<?php echo $mesascocuartito[3]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocuartito[3]->status == 1) ? $mesascocuartito[3]->imagen : $mesascocuartito[3]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocuartito[2]->nomesa; ?> name="<?php echo $mesascocuartito[2]->status; ?>" data-content=<?php echo $mesascocuartito[2]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocuartito[2]->status == 1) ? $mesascocuartito[2]->imagen : $mesascocuartito[2]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocuartito[0]->nomesa; ?> name="<?php echo $mesascocuartito[0]->status; ?>" data-content=<?php echo $mesascocuartito[0]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocuartito[0]->status == 1) ? $mesascocuartito[0]->imagen : $mesascocuartito[0]->imagen_ocupada; ?>">
														</div>
														<div class="col-md-6 text-center mt-3">
															<img class="mesas_listas" alt=<?php echo $mesascocuartito[1]->nomesa; ?> name="<?php echo $mesascocuartito[1]->status; ?>" data-content=<?php echo $mesascocuartito[1]->idmesa; ?> src="<?php echo base_url(); ?>assets/develop/images/<?php echo ($mesascocuartito[1]->status == 1) ? $mesascocuartito[1]->imagen : $mesascocuartito[1]->imagen_ocupada; ?>">
														</div>

													</div>
												</div>
											</div>



										</div>
									</div>
								</div>
							</div>

							<div class="row tab-pane fade" id="pedidos" role="tabpanel" aria-labelledby="nav-profile-tab">
								<div class="col-md-12 fondo_card">
									<div class="card">
										<div class="card-header">
											<div class="row">
												<div class="col-md-6">
													<i class="fas fa-cash-register"></i> PEDIDOS
												</div>
												<div class="col-md-6 text-right">
													<button class="btn btn-sm btn-black" id="nuevo_pedido"><i class="fas fa-plus"></i>
														Nuevo Pedido</button>
													<button class="btn btn-sm btn-black"><i class="fas fa-cloud-download-alt"></i></button>

												</div>
											</div>
										</div>

										<div class="card-body">
											<div class="row">
												<div class="col-md-12">
													<table id="tbl_pedidos" class="table table-border display" style="width:100%">
														<thead>
															<tr>
																<th>No. Pedido</th>
																<th>Mesero</th>
																<th>Status</th>
																<th>Total</th>
															</tr>
														</thead>
														<tbody>
															<?php foreach ($pedidos as $p) { ?>
																<tr class="fila_pedidos" data-content=<?php echo $p->idpedido; ?>>
																	<th><?php echo $p->idpedido; ?>
																	</th>
																	<th><?php echo $p->mesero; ?>
																	</th>
																	<th><?php echo $p->status; ?>
																	</th>
																	<th><?php echo $p->total; ?>
																	</th>
																</tr>
															<?php } ?>
														</tbody>
														<tfoot>
															<tr>
																<th>No. Pedido</th>
																<th>Mesero</th>
																<th>Status</th>
																<th>Total</th>
															</tr>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row tab-pane fade" id="corte_meseros" role="tabpanel" aria-labelledby="nav-profile-tab">
								<div class="col-md-12 fondo_card">
									<div class="card">
										<div class="card-header">
											<div class="row">
												<div class="col-md-6">
													<i class="fas fa-cash-register"></i> CORTE MESEROS
												</div>
												<div class="col-md-6 text-right">
													<button class="btn btn-sm btn-black" id="download_meseros"><i class="fas fa-cloud-download-alt"></i></button>

												</div>
											</div>
										</div>

										<div class="card-body">
											<div class="row">
												<div class="col-md-4">
													<div class="form-group">
														<input type="date" class="form-control form-control-sm" id="fecha_mesero">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<input type="text" class="form-control form-control-sm" id="mesero_uuid" placeholder="Ingrese el uuid del mesero">
													</div>
												</div>
												<div class="col-md-4">
													<button class="btn btn-success btn-sm" id="mostrar_meseros_tabla">Mostrar</button>
												</div>
												<div id="info_meseros" style="display:none;">
													<div class="col-md-6 text-center" id="tabla_ventas_scroll">
														<h5>Lista de Ventas</h5>
														<table id="tbl_ventas_mesero" class="table table-border " style="width:100%">
															<thead>
																<tr>
																	<th>No. Venta</th>
																	<th>Total</th>
																</tr>
															</thead>
															<tbody id="ventas_meseros_lista">
															</tbody>
															<tfoot>
																<tr>
																	<th>No. Venta</th>
																	<th id="total_venta_m">Total</th>
																</tr>
															</tfoot>
														</table>
													</div>
													<div class="col-md-6 text-center" id="tabla_pedidos_scroll">
														<h5>Lista de Pedidos</h5>
														<table id="tbl_pedidos_mesero" class="table table-border " style="width:100%">
															<thead>
																<tr>
																	<th>No. Pedido</th>
																	<th>Total</th>
																</tr>
															</thead>
															<tbody id="pedidos_meseros_lista">
															</tbody>
															<tfoot>
																<tr>
																	<th>No. Pedido</th>
																	<th id="total_pedido_m">Total</th>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>


			</section>
			<section class="col-md-4" id="seccion_informacion">
				<span id="msj_seccion_informacion">
					< Selecciona una mesa</span>
			</section>
		</div>

	</div>

	<?php include 'layout/footer.php'; ?>

	<!--<script src="<?php echo base_url(); ?>assets/clocks/js/clock.js">
	</script>-->
	<script src="<?php echo base_url(); ?>assets/develop/js/lista_mesas.js">
	</script>
	<script script src="<?php echo base_url(); ?>assets/develop/js/pedidos.js">
	</script>
	<script script src="<?php echo base_url(); ?>assets/develop/js/meseros_ventas.js">
	</script>
	<script>
		$(document).ready(function() {
			$("#nav-mesas-tab").on("click", function() {
				$("#seccion_informacion").empty();
			});

			$("#nav-pedidos-tab").on("click", function() {
				$("#seccion_informacion").empty();
			});


		});
	</script>


	</html>