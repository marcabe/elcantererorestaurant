<?php include 'layout/head.php'; ?>


<!--<link
	href="<?php echo base_url(); ?>assets/css/pages/lista_clientes.css"
rel="stylesheet">-->

<body>
	<div class="loader"></div>
	<div class="loader1"></div>

	<?php include 'layout/encabezado.php'; ?>

	<div class="container-fluid">
		<div class="row">
			<section class="col-md-12">
				<div class="row">
					<div class="col-md-12" id="sup_top">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#dashboard" role="tab" aria-controls="nav-home" aria-selected="true">Dashboard</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#ventas" role="tab" aria-controls="nav-profile" aria-selected="false">Ventas</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#caja" role="tab" aria-controls="nav-profile" aria-selected="false">Caja</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#reporte_v" role="tab" aria-controls="nav-profile" aria-selected="false">Reporte de caja</a>
							</div>
						</nav>
					</div>
					<div class="col-md-12">
						<div class="tab-content" id="nav-tabContent">
							<div class="row tab-pane fade show active" id="dashboard" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-12 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														Dashboard
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black"><i class="fas fa-cloud-download-alt"></i></button>
													</div>
												</div>
											</div>
											<div class="card-body">

												<div class="form-row pb-5">
													<div class="col-md-4">
														<select class="form-control" id="mes">
															<option value=1>Enero</option>
															<option value=2>Febrero</option>
															<option value=3>Marzo</option>
															<option value=4>Abril</option>
															<option value=5>Mayo</option>
															<option value=6>Junio</option>
															<option value=7>Julio</option>
															<option value=8>Agosto</option>
															<option value=9>Septiembre</option>
															<option value=10>Octubre</option>
															<option value=11>Noviembre</option>
															<option value=12>Diciembre</option>
														</select>
													</div>
													<div class="col-md-4 text-center">
														<select class="form-control" id="anio">
															<option value=2021>2021</option>
															<option value=2022>2022</option>
															<option value=2023>2023</option>
														</select>
													</div>
													<div class="col-md-4">
														<button class="btn btn-sm btn-success" id="filtrar">Filtrar</button>
													</div>
												</div>
												<div class="row mb-5 pt-2">
													<div class="col-md-12 text-center">
														<h2 id="titulo">Informacion del mes de <?php echo $n_mes . " del año " . $anio_actual; ?>
														</h2>
													</div>
												</div>



												<div class="row pt-2">
													<div class="col-md-2 col-sm-12 pl-2 pr-2">
														<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
															<div class="count text-center">
																<i class="fas fa-receipt fa-2x"></i>
																<label id="total_ventas" class="info_money"><?php echo $total_ventas_mensual; ?></label>
															</div>
															<h6 class="mt-1">Mesas atendidas del mes</h6>
														</div>
													</div>
													<div class="col-md-3 col-sm-12 pl-2 pr-2">
														<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
															<div class="count  text-center">
																<i class="far fa-money-bill-alt fa-2x"></i> <label id="monto_ventas" class="info_money">$ <?php echo $ventas_del_mes; ?></label>
															</div>
															<h6 class="mt-1">Ventas totales del mes</h6>
														</div>
													</div>


													<div class="col-md-2 col-sm-12 pl-2 pr-2">
														<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
															<div class="count  text-center">
																<i class="fas fa-border-all fa-2x"></i>
																<label id="total_pedidos" class="info_money"><?php echo $total_pedidos_mensual; ?></label>
															</div>
															<h6 class="mt-1">Pedidos del mes</h6>
														</div>
													</div>
													<div class="col-md-3 col-sm-12 pl-2 pr-2">
														<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
															<div class="count  text-center">
																<i class="far fa-money-bill-alt fa-2x"></i>
																<label id="monto_pedidos" class="info_money">$ <?php echo $pedidos_del_mes; ?></label>
															</div>
															<h6 class="mt-1">Pedidos totales del mes</h6>
														</div>
													</div>




													<!--<div class="col-md-2 col-sm-12 pl-2 pr-2">
														<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
															<div class="count  text-center">
																<i class="fas fa-drumstick-bite fa-2x"></i>
																<label id="monto_ventas">2</label>
															</div>
															<h6 class="mt-1">Producto más vendido</h6>
														</div>
													</div>-->

													<div class="col-md-6">
														<div id="pie2"></div>
													</div>
													<div class="col-md-6">
														<div id="pie"></div>
													</div>
													<div class="col-md-12">
														<div id="chart"></div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="row tab-pane fade" id="ventas" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-11">
														Lista de Ventas
													</div>

													<div class="col-md-1 text-right">
														<button class="btn btn-sm btn-black"><i class="fas fa-cloud-download-alt"></i></button>
													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-3">
																<div class="input-group">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
																	</div>
																	<input type="date" class="form-control form-control-sm" id="fi">
																</div>
															</div>
															<div class="col-md-3">
																<div class="input-group">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
																	</div>
																	<input type="date" class="form-control form-control-sm" id="ff">
																</div>
															</div>
														</div>
													</div>
													<div class="col-md-12 pt-3">
														<div class="row">
															<div class="col-md-4">
																<div class="input-group">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
																	</div>
																	<select class="form-control form-control-sm" id="tipo">
																		<option value=0>Seleccione una opción</option>
																		<option value="v">Ventas</option>
																		<option value="p">Pedidos</option>
																	</select>
																</div>
															</div>
															<div class="col-md-4">
																<div class="input-group">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
																	</div>
																	<select class="form-control form-control-sm" id="opcion">
																		<option value=0>Seleccione una opción</option>
																		<option value="c">Cerrada</option>
																		<option value="a">Abierta</option>
																	</select>
																</div>
															</div>
															<div class="col-md-2">
																<button class="btn btn-success btn-sm" id="consultar">Consultar</button>
															</div>
														</div>
													</div>
													<div class="col-md-12 pt-5">
														<table id="info_vp" class="table table-border display" style="width:100%">
															<thead>
																<tr>
																	<th>Ticket</th>
																	<th>Mesero</th>
																	<th>Status</th>
																	<th>Total</th>
																</tr>
															</thead>
															<tbody id="fila_prueba">
															</tbody>
															<tfoot>
																<tr>
																	<th>Ticket</th>
																	<th>Mesero</th>
																	<th>Status</th>
																	<th>Total</th>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_info_ventas">
										<div class="card" id="itme_info">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>

										<div class="card" id="info" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NO. TICKET <label id="no_ticket"></label>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert" id="alert_danger_info" style="display:none;">
														</div>
													</div>


													<div class="col-md-12">
														<p><b id="personas"></b><label id="ncliente"></label><label id="fecha_ini"></label></p>
													</div>
													<div class="col-md-4">
														<p><b id="propina"></b></p>
													</div>
													<div class="col-md-4">
														<p><label id="porcentaje_propina"></label></p>
													</div>
													<div class="col-md-4">
														<p><label id="monto_propina"></label></p>
													</div>
													<div class="col-md-12">
														<p><b id="metodo_p"></b></p>
													</div>

													<div class="col-md-12">
														<ul id="lista_comanda" class="list show items stated additions">
														</ul>
													</div>
													<div class="col-md-12">
														<div class="total"><span>Total: </span>
															<strong class="ng-binding" id="total"></strong>
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm" id="cancelar_info">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm" id="imprimir_info">
																	<i class="fas fa-chevron-circle-right"></i> Imprimir
																	Ticket
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row tab-pane fade" id="caja" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-12 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														Administracion de Caja ($ <?php echo number_format($total_al_dia, 2, '.', ' '); ?>)
													</div>
													<div class="col-md-6 text-right">
														<?php if (count($apertura) > 0) { ?>
															<button data-content="<?php echo $apertura[0]->idapertura_principal; ?>" id="cerrar_caja" class="btn btn-sm btn-black"><i class="fas fa-cash-register"></i> Cerrar
																caja
																principal</button>
															<button id="abrir_turno" data-content="<?php echo $apertura[0]->subapertura[$apertura[0]->tam_sub - 1]->idsub_apertura; ?>" class="btn btn-sm btn-black"><i class="fas fa-cash-register"></i> Abrir
																nuevo turno</button>
															<button id="pagar_provedor" data-content="<?php echo $apertura[0]->subapertura[$apertura[0]->tam_sub - 1]->idsub_apertura; ?>" class="btn btn-sm btn-black"><i class="fas fa-cash-register"></i> Pagar
																proveedor</button>
														<?php } else { ?>
															<button id="abrir_caja" class="btn btn-sm btn-black"><i class="fas fa-cash-register"></i> Abrir
																caja
																principal</button>
														<?php } ?>
													</div>
												</div>
											</div>
											<div class="card-body">
												<?php
												if (count($apertura) > 0) {
													$no = 1; ?>
													<div class="row">
														<div class="col-md-3">
															<div class="row">
																<?php foreach ($apertura[0]->subapertura as $sap) { ?>
																	<div class="col-md-12 col-sm-12 pl-2 pr-2">
																		<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
																			<h6 class="mt-1"><i class="fas fa-cash-register fa-2x"></i>
																				Turno No.
																				<?php echo $no; ?>
																				($ <?php echo number_format($sap->total_apertura, 2, '.', ',');  ?>)
																			</h6>
																			<small><?php echo $sap->hora_inicio . " a " . $sap->fecha_cierre; ?></small>
																			<br>
																			<small><?php echo "Monto Apertura: $" . number_format($sap->monto_apertura, 2, '.', ','); ?></small>
																			<br>
																			<small><?php echo "Usuario Apertura: " . $sap->usuario_apertura; ?></small>
																			<br>
																			<small><?php echo "Usuario Cierre: ";
																					echo (isset($sap->usuario_cierre)) ? $sap->usuario_cierre : "---------"; ?></small>
																			<div class="row">
																				<div class="col-md-12 salidas_dash">
																					<i class="fas fa-truck"></i><?php echo "$ " . $sap->pagos_provedor; ?>
																				</div>
																				<div class="col-md-6 montos_dash">
																					<i class="fas fa-store-alt"></i><?php echo "$ " . $sap->monto_venta; ?>
																				</div>
																				<div class="col-md-6 montos_dash">
																					<i class="fas fa-shopping-cart"></i><?php echo "$ " . $sap->monto_pedido; ?>
																				</div>
																			</div>
																		</div>
																	</div>
																<?php $no++;
																} ?>
															</div>
														</div>
														<div class="col-md-9">
															<div class="row">
																<div class="col-md-2 col-sm-12 pl-2 pr-2">
																	<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
																		<div class="count text-center">
																			<i class="fas fa-receipt fa-2x"></i>
																			<label id="monto_ventas"><?php echo $mesas_atendidas; ?></label>
																		</div>
																		<h6 class="mt-1">Mesas atendidas</h6>
																	</div>
																</div>
																<div class="col-md-4 col-sm-12 pl-2 pr-2">
																	<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
																		<div class="count  text-center">
																			<i class="far fa-money-bill-alt fa-2x"></i>
																			<label id="monto_ventas">$ <?php echo $ventas_del_dia; ?></label>
																		</div>
																		<h6 class="mt-1">Ventas del día</h6>
																	</div>
																</div>
																<div class="col-md-2 col-sm-12 pl-2 pr-2">
																	<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
																		<div class="count text-center">
																			<i class="fas fa-shopping-cart fa-2x"></i>
																			<label id="monto_ventas"><?php echo $pedidos_atendidos; ?></label>
																		</div>
																		<h6 class="mt-1">Pedidos atendidas</h6>
																	</div>
																</div>
																<div class="col-md-4 col-sm-12 pl-2 pr-2">
																	<div class="tile-stats text-center pt-2 pb-2 pl-3 pr-5">
																		<div class="count  text-center">
																			<i class="far fa-money-bill-alt fa-2x"></i>
																			<label id="monto_ventas">$ <?php echo $pedidos_del_dia; ?></label>
																		</div>
																		<h6 class="mt-1">Pedidos del día</h6>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-2 col-sm-12 pl-2 pr-2">
																	<div id="venta_pedidos"></div>
																</div>
															</div>
														</div>
													</div>
												<?php
												} ?>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row tab-pane fade" id="reporte_v" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-12 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-11">
														Reporte de Aperturas
													</div>

													<div class="col-md-1 text-right">
														<button class="btn btn-sm btn-black"><i class="fas fa-cloud-download-alt"></i></button>
													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="row">
															<div class="col-md-3">
																<div class="input-group">
																	<div class="input-group-prepend">
																		<div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
																	</div>
																	<input type="date" class="form-control form-control-sm" id="fc_caj">
																</div>
															</div>
															<div class="col-md-2">
																<button class="btn btn-success btn-sm" id="consultar_apertura">Consultar</button>
															</div>
														</div>
													</div>
													<div class="col-md-12 pt-5">
														<table id="info_vp" class="table table-border display" style="width:100%">
															<thead>
																<tr>
																	<th>Usuario de Apertura</th>
																	<th>Fecha/Hora Apertura</th>
																	<th>Usuario de Cierre</th>
																	<th>Fecha/Hora Cierre</th>
																	<th>Monto Final</th>
																	<th>Pago a Proveedores</th>
																</tr>
															</thead>
															<tbody id="fila_aperturas">
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</section>
		</div>

	</div>

	<?php include 'layout/footer.php'; ?>

	<!--<script src="<?php echo base_url(); ?>assets/clocks/js/clock.js">
	</script>-->
	<script src="<?php echo base_url(); ?>assets/develop/js/dashboard.js">
	</script>
	<script src="<?php echo base_url(); ?>assets/develop/js/indicadores_vp.js">
	</script>
	<script src="<?php echo base_url(); ?>assets/develop/js/indicadores_report_caja.js">
	</script>

	<script>
		$(document).ready(function() {

			/*Inicializar los select*/
			$("#mes").val(<?php echo $mes_actual; ?>);
			$("#anio").val(<?php echo $anio_actual; ?>);
			/*-----------------------*/


			/* Grafica de pie de ventas vs pedidos */
			let gp_ventas_vs_pedidos = $("#pie2").dxPieChart({
				palette: "bright",
				dataSource: <?php echo $venta_vs_pedido_mensual; ?>,
				title: "Ventas vs Pedidos",
				legend: {
					orientation: "horizontal",
					itemTextPosition: "right",
					horizontalAlignment: "center",
					verticalAlignment: "bottom",
					columnCount: 4
				},
				"export": {
					enabled: true
				},
				series: [{
					argumentField: "tipo",
					valueField: "monto",
					label: {
						visible: true,
						font: {
							size: 16
						},
						connector: {
							visible: true,
							width: 0.5
						},
						position: "columns",
						customizeText: function(arg) {
							return arg.valueText + " (" + arg.percentText + ")";
						}
					}
				}]
			});

			/****************************************************************** */


			/* Grafica de barra de ventas vs pedidos */
			$("#chart").dxChart({
				dataSource: <?php echo $venta_vs_pedido_mensual_barra; ?>,
				commonSeriesSettings: {
					argumentField: "day",
					type: "bar",
					hoverMode: "allArgumentPoints",
					selectionMode: "allArgumentPoints",
					label: {
						visible: true,
						format: {
							type: "fixedPoint",
							precision: 0
						}
					}
				},
				series: [{
						valueField: "ventas",
						name: "ventas"
					},
					{
						valueField: "pedidos",
						name: "pedidos"
					}
				],
				title: "Ventas vs Pedidos diarios",
				legend: {
					verticalAlignment: "bottom",
					horizontalAlignment: "center"
				},
				"export": {
					enabled: true
				},
				onPointClick: function(e) {
					e.target.select();
				}
			});
			/************************************************************ */

			/* Grafica de pie de ventas vs pedidos al dia*/
			var venta_pedidos = <?php echo $venta_vs_pedido; ?>;
			let venta_vs_pedidos = $("#venta_pedidos").dxPieChart({
				palette: "bright",
				dataSource: venta_pedidos,
				title: "Ventas vs Pedidos",
				legend: {
					orientation: "horizontal",
					itemTextPosition: "right",
					horizontalAlignment: "center",
					verticalAlignment: "bottom",
					columnCount: 4
				},
				"export": {
					enabled: true
				},
				series: [{
					argumentField: "tipo",
					valueField: "monto",
					label: {
						visible: true,
						font: {
							size: 16
						},
						connector: {
							visible: true,
							width: 0.5
						},
						position: "columns",
						customizeText: function(arg) {
							return arg.valueText + " (" + arg.percentText + ")";
						}
					}
				}]
			});
			/***************************************************************** */



		});
	</script>





	</html>