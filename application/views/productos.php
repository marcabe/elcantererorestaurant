<?php include 'layout/head.php';?>


<!--<link
	href="<?php echo base_url();?>assets/css/pages/lista_clientes.css"
rel="stylesheet">-->

<body>
	<div class="loader"></div>
	<div class="loader1"></div>

	<?php include 'layout/encabezado.php';?>
	<!--<?php include 'layout/menu.php';?>-->

	<div class="container-fluid">
		<div class="row">
			<section class="col-md-12">
				<div class="row">
					<div class="col-md-12" id="sup_top">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
									href="#productos" role="tab" aria-controls="nav-home"
									aria-selected="true">Productos</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#categorias"
									role="tab" aria-controls="nav-profile" aria-selected="false">Categoría de
									Productos</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#proveedores"
									role="tab" aria-controls="nav-profile" aria-selected="false">Proveedores</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#faltantes"
									role="tab" aria-controls="nav-profile" aria-selected="false">Faltantes</a>
							</div>
						</nav>
					</div>
					<div class="col-md-12">
						<div class="tab-content" id="nav-tabContent">

							<div class="row tab-pane fade show active" id="productos" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														PRODCUTOS
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black" id="nuevo_producto"><i
																class="fas fa-plus"></i>
															Nuevo Producto</button>
														<button class="btn btn-sm btn-black"><i
																class="fas fa-cloud-download-alt"></i></button>

													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover"
															id="tbl_productos">
															<thead>
																<tr>
																	<td>Producto</td>
																	<td>Tipo</td>
																	<td>Categoria</td>
																	<td>Precio Unitario</td>
																	<td class="text-center">Favorito</td>
																</tr>
															</thead>
															<tbody id="lista_productos">
																<?php foreach ($productos as $p) { ?>
																<tr class="producto_fila"
																	data-content="<?php echo $p->idproducto;?>">
																	<td><?php echo $p->producto;?>
																	</td>
																	<td><?php echo $p->categoria->categoria;?>
																	</td>
																	<td><?php echo $p->categoria->lugar;?>
																	</td>
																	</td>
																	<td>$ <?php echo $p->precio;?>
																	</td>
																	<td class="text-center"><?php echo ($p->favorito==0)?'<i class="far fa-star" data-content='.$p->idproducto.'></i>':'<i class="fas fa-star" data-content='.$p->idproducto.'></i>';?>
																	</td>
																</tr>
																<?php } ?>
															</tbody>
															<tfoot id="final">
																<tr>
																	<td>Producto</td>
																	<td>Tipo</td>
																	<td>Categoria</td>
																	<td>Precio Unitario</td>
																	<td>Favorito</td>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_productos">
										<div class="card" id="itme_listado_productos">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>
										<div class="card" id="formulario_nuevo_producto" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NUEVO PRODUCTO
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert" id="alert_danger"
															style="display:none;">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Producto
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="producto">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Precio
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="precio">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Descripcion</label>
															<div class="col-sm-8">
																<textarea class="form-control" id="descripcion">
																</textarea>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Categoría
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<select class="form-control" id="idcategoria">
																	<option value=0>--Seleccione una categoría--
																	</option>
																	<?php foreach ($categorias as $c) { ?>
																	<option
																		value="<?php echo $c->idcategoria;?>">
																		<?php echo $c->categoria;?>
																	</option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Favorito</label>
															<div class="col-sm-8">
																<select class="form-control" id="favorito">
																	<option value=0>No</option>
																	<option value=1>Si</option>
																</select>
															</div>
														</div>
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm"
																	id="cancelar_guardad_producto">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm"
																	id="guardar_producto">
																	<i class="fas fa-chevron-circle-right"></i> Guardar
																</button>

																<button type="button" class="btn btn-success btn-sm"
																	id="editar_producto">
																	<i class="fas fa-chevron-circle-right"></i> Editar
																</button>


															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="row tab-pane fade" id="categorias" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														CATEGORIAS
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black" id="nueva_categoria"><i
																class="fas fa-plus"></i>
															Nueva Categoría</button>
														<button class="btn btn-sm btn-black"><i
																class="fas fa-cloud-download-alt"></i></button>

													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover"
															id="tbl_categorias">
															<thead>
																<tr>
																	<td>Categoria</td>
																	<td>Tipo</td>
																</tr>
															</thead>
															<tbody id="lista_categorias">
																<?php foreach ($categorias as $c) { ?>
																<tr class="categoria_fila"
																	data-content="<?php echo $c->idcategoria;?>">
																	<td><?php echo $c->categoria;?>
																	</td>
																	<td><?php echo ($c->lugar=='b')?"Barra":"Cocina";?>
																	</td>
																</tr>
																<?php } ?>
															</tbody>
															<tfoot id="final">
																<tr>
																	<td>Producto</td>
																	<td>Tipo</td>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_categorias">
										<div class="card" id="itme_listado_categorias">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>

										<div class="card" id="formulario_nueva_categoria" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NUEVA CATEGORÍA
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert"
															id="alert_danger_categoria" style="display:none;">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Categoría
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="categoria">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Tipo</label>
															<div class="col-sm-8">
																<select class="form-control" id="lugar">
																	<option value=0>--Seleccione el tipo--</option>
																	<option value="c">Cocina</option>
																	<option value="b">Barra</option>
																</select>
															</div>
														</div>
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm"
																	id="cancelar_categoria">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm"
																	id="guardar_categoria">
																	<i class="fas fa-chevron-circle-right"></i> Guardar
																</button>

																<button type="button" class="btn btn-success btn-sm"
																	id="editar_categoria">
																	<i class="fas fa-chevron-circle-right"></i> Editar
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="row tab-pane fade" id="proveedores" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														PROVEEDORES
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black" id="nuevo_proveedor"><i
																class="fas fa-plus"></i>
															Nuevo Proveedor</button>
														<button class="btn btn-sm btn-black"><i
																class="fas fa-cloud-download-alt"></i></button>

													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover"
															id="tbl_proveedores">
															<thead>
																<tr>
																	<td>UUID</td>
																	<td>Proveedor</td>
																	<td>Status</td>
																</tr>
															</thead>
															<tbody id="lista_proveedores">
																<?php foreach ($proveedores as $p) { ?>
																<tr class="proveedor_fila"
																	data-content="<?php echo $p->idpro;?>">
																	<td><?php echo $p->idpro; ?>
																	</td>
																	<td><?php echo $p->nombre;?>
																	</td>
																	<td><?php echo ($p->status==1)?"Activo":"Inactivo";?>
																	</td>
																</tr>
																<?php } ?>
															</tbody>
															<tfoot id="final_proveedor">
																<tr>
																	<td>UUID</td>
																	<td>Proveedor</td>
																	<td>Status</td>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_proveedores">
										<div class="card" id="itme_listado_proveedores">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>

										<div class="card" id="formulario_nuevo_proveedor" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NUEVO PROVEEDOR
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert"
															id="alert_danger_proveedor" style="display:none;">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Proveedor
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="nombre_pro">
															</div>
														</div>
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm"
																	id="cancelar_proveedor">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm"
																	id="guardar_proveedor">
																	<i class="fas fa-chevron-circle-right"></i> Guardar
																</button>

																<button type="button" class="btn btn-success btn-sm"
																	id="editar_proveedor">
																	<i class="fas fa-chevron-circle-right"></i> Editar
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="row tab-pane fade" id="faltantes" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														FALTANTES
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black" id="nuevo_faltante"><i
																class="fas fa-plus"></i>
															Nuevo Registro</button>
														<button class="btn btn-sm btn-black"><i
																class="fas fa-cloud-download-alt"></i></button>
													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover"
															id="tbl_faltantes">
															<thead>
																<tr>
																	<td>ID</td>
																	<td>FECHA</td>
																</tr>
															</thead>
															<tbody id="lista_faltantes">
																<?php foreach ($faltantes as $f) { ?>
																<tr class="faltantes_fila"
																	data-content="<?php echo $f->idfaltante;?>">
																	<td><?php echo $f->idfaltante; ?>
																	</td>
																	<td><?php echo $f->fecha;?>
																</tr>
																<?php } ?>
															</tbody>
															<tfoot id="final_faltantes">
																<tr>
																	<td>ID</td>
																	<td>FECHA</td>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_faltante">
										<div class="card" id="itme_listado_faltantes">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>

										<div class="card" id="elementos_faltantes" style="display:none;">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">

													<div class="col-md-12">
														<table class="table">
															<thead>
																<tr>
																	<th scope="col">PRODUCTO</th>
																	<th scope="col">CANTIDAD</th>
																</tr>
															</thead>
															<tbody id="lista_faltates_get">
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>


										<div class="card" id="formulario_nuevo_faltantes" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NUEVO REGISTRO DE FALTANTES <?php echo date("Y-m-d");?>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert"
															id="alert_danger_faltantes" style="display:none;">
														</div>
													</div>
													<div class="col-md-12 text-right">
														<button class="btn btn-sm btn-success"
															id="agregar_input_faltante"><i class="fas fa-plus"></i>
															Agregar Producto</button>
													</div>
													<div class="col-md-12" id="seccion_faltantes_input">
														<div class="form-group row">
															<label class="col-sm-6 col-form-label">Producto
															</label>
															<label class="col-sm-6 col-form-label">Cantidad
															</label>
														</div>
														<div class="form-group row">
															<div class="col-sm-5">
																<input type="text" class="form-control">
															</div>
															<div class="col-sm-5">
																<input type="text" class="form-control">
															</div>
															<div class="col-sm-2">
																<i class="fas fa-times-circle quitar_faltante"></i>
															</div>
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm"
																	id="cancelar_faltante">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm"
																	id="guardar_faltantes">
																	<i class="fas fa-chevron-circle-right"></i> Guardar
																</button>

																<!--<button type="button" class="btn btn-success btn-sm"
																	id="editar_faltantes">
																	<i class="fas fa-chevron-circle-right"></i> Editar
																</button>-->
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
				</div>
			</section>
		</div>

	</div>

	<?php include 'layout/footer.php';?>

	<!--<script src="<?php echo base_url();?>assets/clocks/js/clock.js">
	</script>-->
	<script src="<?php echo base_url();?>assets/develop/js/productos.js">
	</script>
	<script src="<?php echo base_url();?>assets/develop/js/categorias.js">
	</script>
	<script src="<?php echo base_url();?>assets/develop/js/proveedores.js">
	</script>

	<script src="<?php echo base_url();?>assets/develop/js/faltantes.js">
	</script>


	</html>