<?php include 'layout/head.php';?>


<!--<link
	href="<?php echo base_url();?>assets/css/pages/lista_clientes.css"
rel="stylesheet">-->

<body>
	<div class="loader"></div>
	<div class="loader1"></div>

	<?php include 'layout/encabezado.php';?>
	<!--<?php include 'layout/menu.php';?>-->

	<div class="container-fluid">
		<div class="row">
			<section class="col-md-12">
				<div class="row">
					<div class="col-md-12" id="sup_top">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#usuarios"
									role="tab" aria-controls="nav-home" aria-selected="true">Usuarios</a>
								<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#meseros"
									role="tab" aria-controls="nav-profile" aria-selected="false">
									Meseros</a>
							</div>
						</nav>
					</div>
					<div class="col-md-12">
						<div class="tab-content" id="nav-tabContent">
							<div class="row tab-pane fade show active" id="usuarios" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														USUARIOS
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black" id="nuevo_usuario"><i
																class="fas fa-plus"></i>
															Nuevo Usuario</button>
														<button class="btn btn-sm btn-black"><i
																class="fas fa-cloud-download-alt"></i></button>

													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover"
															id="tbl_usuarios">
															<thead>
																<tr>
																	<td>Usuario</td>
																	<td>Tipo</td>
																	<td>Nombre</td>
																	<td>Status</td>
																</tr>
															</thead>
															<tbody id="lista_usuarios">
																<?php foreach ($usuarios as $u) { ?>
																<tr class="usuario_fila"
																	data-content="<?php echo $u->usuario;?>">
																	<td><?php echo $u->usuario;?>
																	</td>
																	<td><?php echo $u->type;?>
																	</td>
																	<td><?php echo $u->nombre;?>
																	</td>
																	<td><?php echo ($u->status==0)?'<i class="fas fa-user-times stat" data-content='.$u->usuario.'></i>':'<i class="fas fa-user-check stat" data-content='.$u->usuario.'></i>';?>
																	</td>
																</tr>
																<?php } ?>
															</tbody>
															<tfoot id="final">
																<tr>
																	<td>Usuario</td>
																	<td>Tipo</td>
																	<td>Nombre</td>
																	<td>Status</td>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_usuarios">
										<div class="card" id="itme_listado_usuarios">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>
										<div class="card" id="formulario_nuevo_usuario" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NUEVO PRODUCTO
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert"
															id="alert_danger_usuario" style="display:none;">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Usuario
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="usuario">
															</div>
														</div>

														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Tipo</label>
															<div class="col-sm-8">
																<select class="form-control" id="tipo">
																	<option value=0>--Seleccione un tipo de usuario--
																	</option>
																	<option value=1>Administrador</option>
																	<option value=2>Encargado</option>
																</select>
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Nombre completo
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="nombre">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Contraseña
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="clave">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Confirmas contraseña
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control"
																	id="clave_confirm">
															</div>
														</div>
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm"
																	id="cancelar_usuario">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm"
																	id="guardar_usuario">
																	<i class="fas fa-chevron-circle-right"></i> Guardar
																</button>

																<button type="button" class="btn btn-success btn-sm"
																	id="editar_usuario">
																	<i class="fas fa-chevron-circle-right"></i> Editar
																</button>


															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


							<div class="row tab-pane fade" id="meseros" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														MESEROS
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black" id="nuevo_mesero"><i
																class="fas fa-plus"></i>
															Nuevo Mesero</button>
														<button class="btn btn-sm btn-black"><i
																class="fas fa-cloud-download-alt"></i></button>

													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover" id="tbl_meseros">
															<thead>
																<tr>
																	<td>UUID</td>
																	<td>Nombre</td>
																	<td>Teléfono</td>
																	<td>Status</td>
																</tr>
															</thead>
															<tbody id="lista_meseros">
																<?php foreach ($meseros as $m) { ?>
																<tr class="mesero_fila"
																	data-content="<?php echo $m->idmesero;?>">
																	<td><?php echo $m->idmesero;?>
																	</td>
																	<td><?php echo $m->nombre." ".$m->apellidos;?>
																	</td>
																	<td><?php echo $m->celular;?>
																	</td>
																	<td><?php echo ($m->status==0)?'<i class="fas fa-user-times stat_m" data-content='.$m->idmesero.'></i>':'<i class="fas fa-user-check stat_m" data-content='.$m->idmesero.'></i>';?>
																	</td>
																</tr>
																<?php } ?>
															</tbody>
															<tfoot id="final">
																<tr>
																	<td>UUID</td>
																	<td>Nombre</td>
																	<td>Teléfono</td>
																	<td>Status</td>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_meseros">
										<div class="card" id="itme_listado_meseros">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>

										<div class="card" id="formulario_nuevo_mesero" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NUEVA MESERO
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert"
															id="alert_danger_mesero" style="display:none;">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Mesero UID
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="idmesero">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Nombre(s)
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="nombre_mesero">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Apellido(s)
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="apellidos_mesero">
															</div>
														</div>
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Celular
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="celular_mesero">
															</div>
														</div>
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm"
																	id="cancelar_mesero">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm"
																	id="guardar_mesero">
																	<i class="fas fa-chevron-circle-right"></i> Guardar
																</button>

																<button type="button" class="btn btn-success btn-sm"
																	id="editar_mesero">
																	<i class="fas fa-chevron-circle-right"></i> Editar
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

	</div>

	<?php include 'layout/footer.php';?>

	<script src="<?php echo base_url();?>assets/develop/js/usuarios.js">
	</script>
	<script src="<?php echo base_url();?>assets/develop/js/meseros.js"></script>







	</html>