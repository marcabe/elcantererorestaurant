<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Include the above in your HEAD tag -->

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">


<link href="<?php echo base_url() ?>assets/develop/css/login.css"
	rel="stylesheet">

<div class="main">


	<div class="container">
		<center>
			<div class="middle">
				<div id="login">

					<div id="formulario_login" action="javascript:void(0);" method="get">

						<fieldset class="clearfix">

							<p><span class="fa fa-user"></span><input type="text" Placeholder="Ingrese su usuario"
									id="usuario">
							</p>
							<!-- JS because of IE support; better: placeholder="Username" -->
							<p><span class="fa fa-lock"></span><input type="password"
									Placeholder="Ingrese su contraseña" id="clave">
							</p> <!-- JS because of IE support; better: placeholder="Password" -->

							<div>
								<span style="width:50%; text-align:right;  display: inline-block;"><input type="submit"
										value="Ingresar" id="ingresar"></span>
							</div>

						</fieldset>
						<div class="clearfix"></div>
					</div>

					<div class="clearfix"></div>

				</div> <!-- end login -->
				<div class="logo pl-5">
					<img src="<?php echo base_url();?>/assets/develop/images/logo.png"
						width="100%">

					<div class="clearfix"></div>
				</div>

			</div>
		</center>
	</div>

</div>
<script>
	$(document).ready(function() {



		$(document).keypress(function(e) {
			if (e.which == 13) {
				var usuario = $("#usuario").val();
				var clave = $("#clave").val();
				if (usuario == '' || clave == '') {
					alert("Debe ingresar todos los datos");
					return 0;
				} else {
					$.ajax({
						url: 'Usuarios/validacion',
						data: {
							usuario: usuario,
							clave: clave
						},
						type: 'POST',
						success: function(response) {
							if (response == 0) {
								alert("Usuario no valido");
							} else if (response == 1) {
								alert("Contraseña no valida");
							} else if (response == 2) {
								window.location = "admin/indicadores";
							}
						}
					});
				}
			}
		});



		$("#ingresar").on("click", function() {
			var usuario = $("#usuario").val();
			var clave = $("#clave").val();
			if (usuario == '' || clave == '') {
				alert("Debe ingresar todos los datos");
				return 0;
			} else {
				$.ajax({
					url: 'Usuarios/validacion',
					data: {
						usuario: usuario,
						clave: clave
					},
					type: 'POST',
					success: function(response) {
						if (response == 0) {
							alert("Usuario no valido");
						} else if (response == 1) {
							alert("Contraseña no valida");
						} else if (response == 2) {
							window.location = "admin/indicadores";
						}
					}
				});
			}
		});
	});
</script>