<?php include 'layout/head.php';?>

<body>

	<?php include 'layout/encabezado.php';?>

<style>

footer {
  position:fixed !important;
}


</style>
	<div class="main mt-5">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4 offset-md-4">
					<div class="card" id="form_login">
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<h4>INICIO DE SESIÓN</h4>
									<small>Por favor, proporcione sus datos</small>
								</div>
								<div class="col-md-12 pt-3">
									<div class="form-group">
										<div class="input-group mb-2 mr-sm-2">
											<div class="input-group-prepend">
												<div class="input-group-text">
													<i class="fas fa-user"></i>
												</div>
											</div>
											<input type="text" class="form-control" id="usuario" placeholder="Usuario">
										</div>
										<small id="error_usuario" class="form-text text-error"></small>
									</div>
									<div class="form-group">
										<div class="input-group mb-2 mr-sm-2">
											<div class="input-group-prepend">
												<div class="input-group-text">
													<i class="fas fa-key"></i>
												</div>
											</div>
											<input type="password" class="form-control" id="clave"
												placeholder="Usuario">
										</div>
										<small id="error_clave" class="form-text text-error"></small>

									</div>
									<div class="form-group">
										<button class="btn btn-success" id="ingresar"><i class="fas fa-paper-plane"></i>
											Ingresar</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>

	</div>



	<footer class="footer">

		<div class="footer-inner">

			<div class="container">

				<div class="row">

					<div class="span12">
						&copy; 2021 Desarrollado por: <a href="https://www.creandopixeles.com.mx">Creando Pixeles</a>.
					</div> <!-- /span12 -->

				</div> <!-- /row -->

			</div> <!-- /container -->

		</div> <!-- /footer-inner -->

	</footer> <!-- /footer -->


	<script src="<?php echo base_url();?>assets/JQuery/jquery.min.js"></script>
	<script
		src="<?php echo base_url();?>assets/bootstrap-4.3.1/dist/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
	</script>
	<script type="text/javascript"
		src="<?php echo base_url();?>assets/datatables/datatables.min.js">
	</script>
	<script type="text/javascript"
		src="<?php echo base_url();?>assets/accounting/accounting.js">
	</script>

	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>

	<script>
		$(window).on("load", function() {
			$(".loader").fadeOut("slow");
			$(".loader1").fadeOut("slow");
		});
	</script>
</body>

<script src="<?php echo base_url();?>assets/develop/js/login.js">
</script>


</html>