<?php include 'layout/head.php'; ?>


<!--<link
	href="<?php echo base_url(); ?>assets/css/pages/lista_clientes.css"
rel="stylesheet">-->

<body>
	<div class="loader"></div>
	<div class="loader1"></div>

	<?php include 'layout/encabezado.php'; ?>
	<!--<?php include 'layout/menu.php'; ?>-->

	<div class="container-fluid">
		<div class="row">
			<section class="col-md-12">
				<div class="row">
					<div class="col-md-12" id="sup_top">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#impresoras" role="tab" aria-controls="nav-home" aria-selected="true">Impresoras</a>
							</div>
						</nav>
					</div>
					<div class="col-md-12">
						<div class="tab-content" id="nav-tabContent">

							<div class="row tab-pane fade show active" id="impresoras" role="tabpanel" aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														Impresoras
													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover" id="tbl_productos">
															<thead>
																<tr>
																	<td>IMPRESORA</td>
																	<td>UBICACION</td>
																	<td></td>
																</tr>
															</thead>
															<tbody id="lista_productos">
																<tr>
																	<td><?php echo $impresora_barra; ?></td>
																	<td>Barra</td>
																	<td>
																		<button class="btn btn-success btn-sm asignar_impresora" data-imp="impresora_barra">Asginar impresora</button class="btn btn-secondary">
																		<button class="btn btn-secondary btn-sm"><i class=" fas fa-print"></i></button>
																	</td>
																</tr>
																<tr>
																	<td><?php echo $impresora_cocina; ?></td>
																	<td>Cocina</td>
																	<td>
																		<button class="btn btn-success btn-sm asignar_impresora" data-imp="impresora_cocina">Asginar impresora</button>
																		<button class="btn btn-secondary btn-sm"><i class="fas fa-print"></i></button>
																	</td>
																</tr>
																<tr>
																	<td><?php echo $impresora_corte; ?></td>
																	<td>Corte de caja</td>
																	<td>
																		<button class="btn btn-success btn-sm asignar_impresora" data-imp="impresora_corte">Asginar impresora</button>
																		<button class="btn btn-secondary btn-sm"><i class="fas fa-print"></i></button>
																	</td>
																</tr>
															</tbody>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_impresoras">
										<div class="card" id="itme_listado_impresoras">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>
										<div class="card" id="formulario_asignar_impresora" style="display:none;">
											<div class="card-header encabezado_carta_info">
												Asignacion de impresora
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert" id="alert_danger" style="display:none;">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Impresora
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<select type="text" class="form-control form-control-sm" id="listaDeImpresoras">
																</select>
															</div>
														</div>
														<div class="col-md-12">
															<h2>Shell
																<button class="btn btn-warning btn-sm" id="btnLimpiarLog">Limpiar shell</button>
															</h2>

															<pre id="estado"></pre>
														</div>
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm" id="cancelar_asignacion">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm" data-impresora="" id="asignar_impresora">
																	<i class="fas fa-chevron-circle-right"></i> Asignar
																</button>
																<button type="button" class="btn btn-success btn-sm" id="probar_impresora">
																	<i class="fas fa-chevron-circle-right"></i> Probar
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

	</div>

	<?php include 'layout/footer.php'; ?>

	<!--<script src="<?php echo base_url(); ?>assets/clocks/js/clock.js">
	</script>-->
	<script src="<?php echo base_url(); ?>assets/develop/js/impresoras.js">
	</script>




	</html>