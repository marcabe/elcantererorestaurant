<?php include 'layout/head.php';?>


<!--<link
	href="<?php echo base_url();?>assets/css/pages/lista_clientes.css"
rel="stylesheet">-->

<body>
	<div class="loader"></div>
	<div class="loader1"></div>

	<?php include 'layout/encabezado.php';?>
	<!--<?php include 'layout/menu.php';?>-->

	<div class="container-fluid">
		<div class="row">
			<section class="col-md-12">
				<div class="row">
					<div class="col-md-12" id="sup_top">
						<nav>
							<div class="nav nav-tabs" id="nav-tab" role="tablist">
								<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#clientes"
									role="tab" aria-controls="nav-home" aria-selected="true">Clientes</a>
							</div>
						</nav>
					</div>
					<div class="col-md-12">
						<div class="tab-content" id="nav-tabContent">

							<div class="row tab-pane fade show active" id="clientes" role="tabpanel"
								aria-labelledby="nav-home-tab">
								<div class="row" style="margin-right:0px;">
									<div class="col-md-8 fondo_card">
										<div class="card">
											<div class="card-header">
												<div class="row">
													<div class="col-md-6">
														Clientes
													</div>
													<div class="col-md-6 text-right">
														<button class="btn btn-sm btn-black" id="nuevo_cliente"><i
																class="fas fa-plus"></i>
															Nuevo Cliente</button>
														<button class="btn btn-sm btn-black"><i
																class="fas fa-cloud-download-alt"></i></button>

													</div>
												</div>
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<table class="table table-striped table-hover"
															id="tbl_clientes">
															<thead>
																<tr>
																	<td>Nobre del cliete</td>
																	<td>Status</td>
																</tr>
															</thead>
															<tbody id="lista_clientes">
																<?php foreach ($clientes as $c) { ?>
																<tr class="clientes_fila"
																	data-content="<?php echo $c->idcliente;?>">
																	<td><?php echo $c->nombre;?>
																	</td>
																	<td><?php echo ($c->status==0)?'<i class="far fa-id-card status_disable" data-content='.$c->idcliente.'></i>':'<i class="far fa-id-card status_enable" data-content='.$c->idcliente.'></i>';?>
																	</td>
																</tr>
																<?php } ?>
															</tbody>
															<tfoot id="final">
																<tr>
																	<td>Nobre del cliete</td>
																	<td>Status</td>
																</tr>
															</tfoot>
														</table>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4 fondo_card" id="columna_formulario_clientes">
										<div class="card" id="itme_listado_clientes">
											<div class="card-header encabezado_carta_info">
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12 mt-5 mb-5">
														<i class="fas fa-chevron-left"></i> Seleccione un item del
														listado
													</div>
												</div>
											</div>
										</div>
										<div class="card" id="formulario_nuevo_cliente" style="display:none;">
											<div class="card-header encabezado_carta_info">
												NUEVO CLIENTE
											</div>
											<div class="card-body">
												<div class="row">
													<div class="col-md-12">
														<div class="alert alert-danger" role="alert"
															id="alert_danger_cliente" style="display:none;">
														</div>
													</div>
													<div class="col-md-12">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Nombre:
																<sup><i class="fas fa-asterisk"></i></sup>
															</label>
															<div class="col-sm-8">
																<input type="text" class="form-control" id="nombre">
															</div>
														</div>
														<div class="form-group row pt-4 text-rigth">
															<div class="col-md-6 text-right">
																<button type="button" class="btn btn-light btn-sm"
																	id="cancelar_cliente">
																	<i class="fas fa-times"></i> Cancelar
																</button>
															</div>
															<div class="col-md-6 text-left">
																<button type="button" class="btn btn-success btn-sm"
																	id="guardar_cliente">
																	<i class="fas fa-chevron-circle-right"></i> Guardar
																</button>

																<button type="button" class="btn btn-success btn-sm"
																	id="editar_cliente">
																	<i class="fas fa-chevron-circle-right"></i> Editar
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
		</div>

	</div>

	<?php include 'layout/footer.php';?>

	<!--<script src="<?php echo base_url();?>assets/clocks/js/clock.js">
	</script>-->
	<script src="<?php echo base_url();?>assets/develop/js/clientes.js">
	</script>





	</html>