<div class="main" id="menu">
    <div class="container-fluid">
        <div class="row">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item dropdown text-center">
                            <div class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-users"></i><br>
                                <span>
                                    Clientes
                                </span>
                            </div>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo base_url();?>lista_clientes">Lista de Clientes</a>
                                <a class="dropdown-item" href="<?php echo base_url();?>nuevo_cliente">Nuevo Cliente</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown text-center">
                            <div class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="far fa-id-card"></i><br>
                                <span>
                                    Personal
                                </span>
                            </div>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo base_url();?>lista_usuarios">Lista de Usuarios</a>
                                <a class="dropdown-item" href="<?php echo base_url();?>nuevo_usuario">Nuevo Usuario</a>
                            </div>
                        </li>

                        <li class="nav-item dropdown text-center">
                            <div class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-envelope-open-text"></i><br>
                                <span>
                                    Textos Aut.
                                </span>
                            </div>

                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo base_url();?>lista_textos">Lista de textos</a>
                                <a class="dropdown-item" href="<?php echo base_url();?>nuevo_texto">Nuevo Texto</a>
                            </div>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    </div>
</div>