<footer class="footer">
    <div class="footer-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    &copy; 2021 Desarrollado por: <a href="https://www.creandopixeles.com.mx">Creando Pixeles</a>.
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="<?php echo base_url(); ?>assets/develop/js/ConectorPlugin.js">
</script>
<script src="<?php echo base_url(); ?>assets/JQuery/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap-4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/datatables/datatables.min.js">
</script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/accounting/accounting.js">
</script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<link rel="stylesheet" href="https://cdn3.devexpress.com/jslib/20.1.6/css/dx.common.css">
<link rel="stylesheet" href="https://cdn3.devexpress.com/jslib/20.1.6/css/dx.light.css">

<!-- DevExtreme library -->
<script type="text/javascript" src="https://cdn3.devexpress.com/jslib/20.1.6/js/dx.all.js"></script>
<script>
    $(window).on("load", function() {
        $(".loader").fadeOut("slow");
        $(".loader1").fadeOut("slow");
    });
</script>
</body>