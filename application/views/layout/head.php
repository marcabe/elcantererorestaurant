<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Canterero</title>
    <link rel="icon" type="image/png" sizes="96x96"
        href="<?php echo base_url() ?>assets/develop/images/favicon-96x96.png">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">


    <link
        href="<?php echo base_url() ?>assets/fontawesome-f/css/all.css"
        rel="stylesheet">

    <link
        href="<?php echo base_url() ?>assets/develop/css/general.css"
        rel="stylesheet">


    <link rel="stylesheet" type="text/css"
        href="<?php echo base_url();?>assets/datatables/datatables.min.css" />


    <link rel="stylesheet"
        href="<?php echo base_url();?>assets/bootstrap-4.3.1/dist/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    <link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css">



</head>