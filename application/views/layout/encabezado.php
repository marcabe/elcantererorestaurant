<header class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#">
    <img src="<?php echo base_url(); ?>/assets/develop/images/logo.png" width="100%">
  </a>

  <?php $datossesion = $this->session->userdata("canterero");
  if (is_null($datossesion) != 1) { ?>

    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link <?php echo $active[0]; ?>" href="
          productos"><i class="fas fa-hamburger fa-2x" title="Productos"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link  <?php echo $active[1]; ?>" href="personal"><i class="far fa-id-card fa-2x" title="Personal"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $active[2]; ?>" href="clientes"><i class="fas fa-users fa-2x" title="Clientes"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $active[3]; ?>" href="indicadores"> <i class="fas fa-chart-line fa-2x" title="Indicadores"></i></a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo $active[4]; ?>" href="configuracion"> <i class="fas fa-cogs fa-2x" title="Configuraciónn"></i></a>
        </li>
      </ul>
      <span class="navbar-text">
        <a href="<?php echo base_url(); ?>">
          <?php echo $datossesion['nombre']; ?></a><input id="rol_user" value="<?php echo $datossesion['rol']; ?>" hidden>
      </span>
    </div>
  <?php } ?>
</header>