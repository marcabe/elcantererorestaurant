<?php

class ConfigModel extends CI_Model
{
    public $tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla = "config";
    }

    public function get_ibarra()
    {
        $this->db->select('impresora_barra');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }


    public function get_icocina()
    {
        $this->db->select('impresora_cocina');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_icorte()
    {
        $this->db->select('impresora_corte');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function update($dataUpdate)
    {
        $this->db->set($dataUpdate);
        $this->db->update($this->tabla);
        return 1;
    }
}
