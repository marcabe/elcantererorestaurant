<?php

class ClientesModel extends CI_Model
{
    public $tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla = "clientes";
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_where($where)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where($where);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idcliente", $id);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function insert($data)
    {
        if ($this->db->insert($this->tabla, $data)) {
            return $this->db->insert_id();
        } else {
            return null;
        }
    }
    public function delete_where($data)
    {
        $this->db->delete($this->tabla, $data); // Produces: // DELETE FROM mytable  // WHERE id = $id
        return true;
    }

    public function update($id, $dataUpdate)
    {
        $this->db->set($dataUpdate);
        $this->db->where('idcliente', $id);
        $this->db->update($this->tabla);
        return 1;
    }

    public function get_like($like){
		$this->db->select('idcliente as id, nombre');
		$this->db->from($this->tabla);
		$this->db->where("status", 1);
		$this->db->like('nombre', $like, 'both');
		$consulta = $this->db->get();
		$prod = $consulta->result_array();

		$data = array();
		foreach($prod as $user){
			$data[] = array("id"=>$user['id'], "text"=>$user['nombre']);
		}
		return $data;

	}



}
