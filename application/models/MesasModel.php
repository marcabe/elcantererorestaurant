<?php

class MesasModel extends CI_Model
{
    public $tabla;

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->tabla = "mesas";
    }

    public function get()
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_where($where)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where($where);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from($this->tabla);
        $this->db->where("idmesa", $id);
        $consulta = $this->db->get();
        return $consulta->result();
    }

    public function insert($data)
    {
        if ($this->db->insert($this->tabla, $data)) {
            return $this->db->insert_id();
        } else {
            return null;
        }
    }
    public function delete_where($data)
    {
        $this->db->delete($this->tabla, $data); // Produces: // DELETE FROM mytable  // WHERE id = $id
        return true;
    }

    public function update($id, $dataUpdate)
    {
        $this->db->set($dataUpdate);
        $this->db->where('idmesa', $id);
        $this->db->update($this->tabla);
        return 1;
    }
}
