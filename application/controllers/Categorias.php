<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Categorias extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('CategoriasModel');

    }


    public function insert()
    {
        $dataInsert = $this->input->post();
        $this->CategoriasModel->insert($dataInsert);
        echo 1;
    }

    public function update($idcategoria){
        $dataUpdate = $this->input->post();
        $this->CategoriasModel->update($idcategoria, $dataUpdate);
    }


    public function get_id($idcategoria)
    {
        $dataProducto = $this->CategoriasModel->get_by_id($idcategoria);
        echo json_encode($dataProducto[0]);
    }
}
