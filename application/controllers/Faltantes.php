<?php

#Controlador apertura principal
#Elaborado por Marco Antonio Caronda
#igng.marco.cardona@gmail.com
#2021

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class Faltantes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('FaltantesModel');
        $this->load->Model('ListafaltantesModel');

    }

    public function insert(){
        $fecha = date("Y-m-d");
        $idFaltante = $this->FaltantesModel->insert(array("fecha"=>$fecha));
        $dataElementos = json_decode($this->input->post("faltantes"));
        foreach($dataElementos as $e){
            $insert = array(
                "idfaltante"=>$idFaltante,
                "producto"=>$e->productos,
                "cantidad"=>$e->cantidad
            );
            $this->ListafaltantesModel->insert($insert);
        }
        echo 1;
    }

    public function get_id($idFaltante){
        $dataFaltantes = $this->ListafaltantesModel->get_where(array("idfaltante"=>$idFaltante));
        echo json_encode($dataFaltantes);
    }


}
