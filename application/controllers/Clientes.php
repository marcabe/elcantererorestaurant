<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Clientes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('ClientesModel');
    }


   
    public function insert()
    {
        $dataInsert = $this->input->post();
        $this->ClientesModel->insert($dataInsert);
        echo 1;
    }

    public function get_like()
    {
        $json = [];
        $data = $this->input->get("q");
        $dataProductos  = $this->ClientesModel->get_like($data);
        echo json_encode($dataProductos);
    }

    public function get_id($idCliente)
    {
        $dataProducto = $this->ClientesModel->get_by_id($idCliente);
        echo json_encode($dataProducto[0]);
    }

    public function update($idcliente)
    {
        $dataUpdate = $this->input->post();
        $this->ClientesModel->update($idcliente, $dataUpdate);
        echo 1;
    }
}
