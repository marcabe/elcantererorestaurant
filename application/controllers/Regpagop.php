<?php

#Controlador Registro de Pago a proveedores
#Elaborado por Marco Antonio Caronda
#igng.marco.cardona@gmail.com
#2021

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class Regpagop extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('AperturaModel');
        $this->load->Model('SubaperturaModel');
        $this->load->Model('VentasModel');
        $this->load->Model('PedidosModel');
        $this->load->Model('RegpagopModel');

    }


    public function insert()
    {
        #Guardamos en una varianle la sesion
        $sesion = $this->session->userdata("canterero");

        $dataInsert = $this->input->post();
        $dataInsert['usuario'] = $sesion['usuario'];
        $this->RegpagopModel->insert($dataInsert);
        $dataSubap = $this->SubaperturaModel->get_where(array("idsub_apertura"=>$dataInsert['idsuba']));
        $total_prove= $dataSubap[0]->pagos_provedor + $dataInsert['monto'];
        $this->SubaperturaModel->update($dataSubap[0]->idsub_apertura, array("pagos_provedor"=>$total_prove));
    }
   
}
