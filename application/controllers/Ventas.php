<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");
require('fpdf/fpdf.php');

class Ventas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('VentasModel');
        $this->load->Model('MesasModel');
        $this->load->Model('MeserosModel');
        $this->load->Model('ComandasModel');
        $this->load->Model('ClientesModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('SubaperturaModel');
        $this->load->Model('ConfigModel');
    }


    public function filtro()
    {
        $info_table = array();
        $fi = $this->input->post("fi");
        $ff = $this->input->post("ff");
        $opcion = $this->input->post('opcion');

        if ($opcion == 'c') {
            $dataVentas = $this->VentasModel->get_where(array("status" => 2, "hora_inicio>=" => $fi, "fecha_final<=" => $ff));
        } elseif ($opcion == 'a') {
            $dataVentas = $this->VentasModel->get_where(array("status" => 1, "hora_inicio>=" => $fi));
        }

        foreach ($dataVentas as $v) {
            $venta = new \stdClass;
            $dataMesero = $this->MeserosModel->get_by_id($v->idmesero);
            $mesero = $dataMesero[0]->nombre . " " . $dataMesero[0]->apellidos;
            $venta->id = $v->idventa;
            $venta->ticket = $v->idventa;
            $venta->mesero = $mesero;
            $venta->status = ($v->status == 1) ? "Abierta" : "Cerrada";
            $venta->total = $v->total;
            $info_table[] = $venta;
        }
        $response = new \stdClass;
        $response->data = $info_table;

        echo json_encode($info_table);
    }



    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataSub = $this->SubaperturaModel->get_where(array("status" => 1));

        #Verificacamos si existe el mesero en la base de datos
        $dataMesero = $this->MeserosModel->get_by_id($dataInsert['idmesero']);
        if (count($dataMesero) > 0) {
            $dataInsert['idsubapertura'] = $dataSub[0]->idsub_apertura;
            $idVenta = $this->VentasModel->insert($dataInsert);
            $this->MesasModel->update($dataInsert['idmesa'], array("status" => 2));
            echo $idVenta;
        } else {
            echo 0;
        }
    }

    public function get_info($idventa)
    {
        $dataVenta = $this->VentasModel->get_by_id($idventa);
        foreach ($dataVenta as $v) {
            $dataComanda = $this->ComandasModel->get_where(array("idventa" => $v->idventa));
            $v->comanda = $dataComanda[0];

            $dataMesero  = $this->MeserosModel->get_where(array("idmesero" => $v->idmesero));
            $v->mesero = $dataMesero[0];

            $datCliente = $this->ClientesModel->get_where(array("idcliente" => $v->idcliente));

            $v->cliente = $datCliente[0];

            $dataComanda = $this->ComandasModel->get_where(array("idventa" => $v->idventa));
            $v->comanda = $dataComanda;
            foreach ($v->comanda as $c) {
                $dataPorducto = $this->ProductosModel->get_by_id($c->idproducto);
                $c->producto = $dataPorducto[0];
            }
        }
        echo json_encode($dataVenta[0]);
    }


    public function get_where()
    {
        $dataWhere = $this->input->post();
        $dataVenta = $this->VentasModel->get_where($dataWhere);
        foreach ($dataVenta as $v) {
            /*$dataComanda = $this->ComandasModel->get_where(array("idventa"=>$v->idventa));
            $v->comanda = $dataComanda[0];*/

            $dataMesero  = $this->MeserosModel->get_where(array("idmesero" => $v->idmesero));
            $v->mesero = $dataMesero[0];

            $datCliente = $this->ClientesModel->get_where(array("idcliente" => $v->idcliente));

            $v->cliente = $datCliente[0];

            $dataComanda = $this->ComandasModel->get_where(array("idventa" => $v->idventa, "status" => 1));
            $v->comanda = $dataComanda;
            foreach ($v->comanda as $c) {
                $dataPorducto = $this->ProductosModel->get_by_id($c->idproducto);
                $c->producto = $dataPorducto[0];
            }

            $dataMesa = $this->MesasModel->get_by_id($v->idmesa);
            $v->mesas = $dataMesa[0];
        }
        echo json_encode($dataVenta[0]);
    }

    public function termina_mesa($idventa)
    {
        $dataTermina  = $this->input->post();
        $dataTermina['fecha_final'] = date("Y-m-d");
        $dataTermina['hora_final'] = date("H:i");
        $idmesa = $dataTermina['idmesa'];
        unset($dataTermina['idmesa']);

        #Ingresamos los parametros finales de la venta
        $this->VentasModel->update($idventa, $dataTermina);
        #Cambiamos la se a un estatus para poder usarla de nuevo
        $this->MesasModel->update($idmesa, array("status" => 1));

        $info_venta = array();
        $dataVenta = $this->VentasModel->get_by_id($idventa);
        if ($dataVenta[0]->metodo_pago == 1) {
            $dataVenta[0]->metodo_pago = "Efectivo";
        } elseif ($dataVenta[0]->metodo_pago == 2) {
            $dataVenta[0]->metodo_pago = "T. de Crédito";
        } elseif ($dataVenta[0]->metodo_pago == 3) {
            $dataVenta[0]->metodo_pago = "T. de Débito";
        }
        $dataMesa = $this->MesasModel->get_where(array("idmesa" => $dataVenta[0]->idmesa));

        $dataMesero = $this->MeserosModel->get_by_id($dataVenta[0]->idmesero);
        $dataComanda = $this->ComandasModel->get_where(array("idventa" => $idventa, "status!=" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $c->producto = $dataProducto[0];
        }

        $dataImpresiorCorte = $this->ConfigModel->get_icorte();


        $info_venta['venta'] = $dataVenta[0];
        $info_venta['mesero'] = $dataMesero[0];
        $info_venta['comanda'] = $dataComanda;
        $info_venta['mesa'] = $dataMesa[0];
        $info_venta["info"] = array(
            "fecha" => date("Y-m-d H:i"),
            "impresora_corte" => $dataImpresiorCorte[0]->impresora_corte
        );

        echo json_encode($info_venta);
    }

    /*public function ticket($idVenta)
    {
        $dataVenta = $this->VentasModel->get_by_id($idVenta);
        $dataMesero = $this->MeserosModel->get_by_id($dataVenta[0]->idmesero);

        $dataComanda = $this->ComandasModel->get_where(array("idventa" => $idVenta));
        $dataPdf  = array();
        $subtotal = 0;
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $dataPdf[] = array(
                0 => $dataProducto[0]->producto,
                1 => $c->cantidad,
                2 => $dataProducto[0]->precio,
                3 => ($dataProducto[0]->precio * $c->cantidad)
            );
            $subtotal += ($dataProducto[0]->precio * $c->cantidad);
        }

        switch ($dataVenta[0]->metodo_pago) {
            case 1:
                $mp = "Efectivo";
                break;
            case 2:
                $mp = "Credito";
                break;
            case 3:
                $mp = "Débito";
                break;
        }



        $pdf = new FPDF($orientation = 'P', $unit = 'mm', array(45, 350));
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 8);    //Letra Arial, negrita (Bold), tam. 20
        $textypos = 3;
        $pdf->setY(2);

        $pdf->Image('assets/develop/images/logo.png', 10, 0, -300, 0);
        $pdf->Ln();
        $pdf->setXY(2, 10);
        $pdf->SetFont('Arial', '', 7);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->setXY(2, 15);
        $pdf->Cell(42, $textypos, "Expedido el: " . date("Y/m/d H:i") . " en", 0, 0, "L");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Guerrero 25  Centro 47980", 0, 0, "L");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Degollado Jalisco", 0, 0, "C");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Mesero: " . $dataMesero[0]->nombre, 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Nota de venta : NV " . $idVenta, 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(0);
        $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->Cell(1, $textypos, '-------------------------------------------------------------------------');
        $pdf->Ln();

        $pdf->setX(2);
        $pdf->Cell(17, $textypos, utf8_decode("Descripción"), 0, 0, "C");
        $pdf->setX(19);
        $pdf->Cell(5, $textypos, "Cant. ", 0, 0, "C");
        $pdf->setX(24);
        $pdf->Cell(10, $textypos, "Pr.Un. ", 0, 0, "C");
        $pdf->setX(34);
        $pdf->Cell(10, $textypos, "Importe", 0, 0, "C");
        $pdf->Ln();
        $pdf->setX(0);
        $pdf->Cell(1, $textypos, '-------------------------------------------------------------------------');
        $pdf->setX(2);
        $pdf->SetFont('Arial', 'B', 5);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->Ln();

        foreach ($dataPdf as $row) {
            for ($i = 0; $i <= 3; $i++) {
                if ($i == 0) {
                    $pdf->SetX(2);
                    $pdf->Cell(15, $textypos, substr(utf8_decode($row[$i]), 0, 15) . "...", 0, 0, "C");
                } elseif ($i == 1) {
                    $pdf->setX(19);
                    $pdf->Cell(5, $textypos, $row[$i], 0, 0, "C");
                } elseif ($i == 2) {
                    $pdf->setX(24);
                    $pdf->Cell(10, $textypos, "$" . number_format($row[$i], 2), 0, 0, "C");
                } elseif ($i == 3) {
                    $pdf->setX(34);
                    $pdf->Cell(10, $textypos, "$" . number_format($row[$i], 2), 0, 0, "C");
                }
            }
            $pdf->Ln();
        }

        $pdf->setX(0);
        $pdf->SetFont('Arial', 'B', 5);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->Cell(45, $textypos, '=======================', 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "SUB TOTAL:", 0, 0, "L");
        $pdf->Cell(20, 5, "$" . number_format($subtotal, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "Impuestos", 0, 0, "L");
        $pdf->Cell(20, 5, "$" . number_format(0.0, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, $mp, 0, 0, "L");
        $pdf->Cell(20, 5, "$" . number_format($dataVenta[0]->cuanto_pago - $dataVenta[0]->monto_propina, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(0);
        $pdf->Cell(45, $textypos, '=======================', 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "Total pago:", 0, 0, "L");
        $pdf->Cell(20, 5, "$" . number_format($dataVenta[0]->cuanto_pago - $dataVenta[0]->monto_propina, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "Cambio: ", 0, 0, "L");
        $pdf->Cell(20, 5, "$" . number_format($dataVenta[0]->cambio, 2), 0, 0, "R");
        $pdf->Ln();

        $pdf->setX(2);
        $pdf->Cell(20, 5, "PROPINA SUGERIDA %" . $dataVenta[0]->porcentaje_propina, 0, 0, "L");
        $pdf->Cell(20, 5, "$" . number_format($dataVenta[0]->monto_propina, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(1, $textypos, '----------------------------------------------------------------------------------');
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(40, $textypos, 'GRACIAS POR SU COMPRA', 0, 0, "C");

        $pdf->Output();
    }*/
}
