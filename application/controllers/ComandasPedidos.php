<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('fpdf/fpdf.php');

class ComandasPedidos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('VentasModel');
        $this->load->Model('ComandaspedidosModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('UsuariosModel');
        $this->load->Model('CancelacionespvModel');
        $this->load->Model('PedidosModel');
        $this->load->Model('CancelacionesppModel');
        $this->load->Model('CategoriasModel');
        $this->load->Model('MeserosModel');
        $this->load->Model('ConfigModel');
    }



    public function comanda($idPedido)
    {
        $dataComandas = $this->ComandaspedidosModel->get_where(array("idpedido" => $idPedido));
        foreach ($dataComandas as $c) {
            $this->ComandaspedidosModel->update($c->comanda_pedido, array("status_comanda" => 1));
        }

        $ventaPedido  = $this->PedidosModel->get_by_id($idPedido);
        $dataComandaP = $this->ComandaspedidosModel->get_where(array("idpedido" => $idPedido));
        foreach ($dataComandaP as $c) {
            $dataPorducto = $this->ProductosModel->get_by_id($c->idproducto);
            $c->producto = $dataPorducto[0];
        }
        $data = array(
            "pedido" => $ventaPedido[0],
            "comanda" => $dataComandaP
        );
        $this->load->view("comanda_p", $data);
    }

    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataInsert = json_decode($dataInsert["comanda"]);
        $idpedido = $dataInsert[0]->idpedido;
        foreach ($dataInsert as $insert) {
            $this->ComandaspedidosModel->insert($insert);
        }

        $comandas = array();

        $dataBarra = array();
        $dataComanda = $this->ComandaspedidosModel->get_where(array("idpedido" => $idpedido, "status_comanda" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria" => $dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar == 'b') {
                $barra = new \stdClass;
                $barra->cantidad = $c->cantidad;
                $barra->producto = strtoupper($dataProducto[0]->producto);
                $barra->comentario = strtoupper($c->comentario);
                $dataBarra[] = $barra;
            }
        }

        $dataCocina = array();
        $dataComanda = $this->ComandaspedidosModel->get_where(array("idpedido" => $idpedido, "status_comanda" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria" => $dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar == 'c') {
                $cocina = new \stdClass;
                $cocina->cantidad = $c->cantidad;
                $cocina->producto = strtoupper($dataProducto[0]->producto);
                $cocina->comentario = strtoupper($c->comentario);
                $dataCocina[] = $cocina;
            }
        }


        $dataPedido = $this->PedidosModel->get_where(array("idpedido" => $idpedido));

        $dataImpresioraBarra = $this->ConfigModel->get_ibarra();
        $dataImpresioraCocina = $this->ConfigModel->get_icocina();

        $comandas["barra"] = $dataBarra;
        $comandas["cocina"] = $dataCocina;
        $comandas["pedido"] = $dataPedido[0];
        $comandas["info"] = array(
            "fecha" => date("Y-m-d H:i"),
            "impresora_barra" => $dataImpresioraBarra[0]->impresora_barra,
            "impresora_cocina" => $dataImpresioraCocina[0]->impresora_cocina
        );
        echo json_encode($comandas);
    }

    public function delete($comanda)
    {
        $dataCondicion = $this->input->post();
        $dataCondicion['clave'] = md5($dataCondicion['clave']);

        $where = array(
            "usuario" => $dataCondicion['usuario'],
            "clave" => $dataCondicion['clave']
        );
        $user = $this->UsuariosModel->get_where($where);
        if (count($user) > 0) {
            #Existe el usuario y su clave
            $this->ComandaspedidosModel->update($comanda, array("status" => 0));
            $this->CancelacionesppModel->insert(array("motivo" => $dataCondicion['motivo'], "usuario" => $dataCondicion['usuario'], "idcomanda" => $comanda));
            echo 1;
        } else {
            #No existe el usuario
            echo 0;
        }
    }


    /*
    public function comanda_print_barra($idPedido)
    {
        $dataPdf = array();

        $dataComanda = $this->ComandaspedidosModel->get_where(array("idpedido"=>$idPedido, "status_comanda"=>0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto"=>$c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria"=>$dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar=='b') {
                $dataPdf[] = array(
                0 => $c->cantidad,
                1 => $dataProducto[0]->producto,
                2 => $c->comentario
            );
            }
        }

        $dataPedido = $this->PedidosModel->get_by_id($idPedido);
        $dataMesero = $this->MeserosModel->get_by_id($dataPedido[0]->idmesero);
        $mesero = $dataMesero[0]->nombre." ".$dataMesero[0]->apellidos;


        $header = array('CANT.', 'PRODUCTO', 'COMENTARIO');

        $pdf = new FPDF($orientation='P', $unit='mm', array(45,350));
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 8);    //Letra Arial, negrita (Bold), tam. 20
        $textypos = 5;
        $pdf->SetXY(2, 2);
        $pdf->Cell(42, $textypos, "COMANDA PARA BARRA", 0, 0, "C");
        $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20
        

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(40, $textypos, "Mesero: ".$mesero , 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(5, $textypos, '====================================');


        $pdf->Ln();
        $pdf->setX(2);
        $pdf->SetFont('Arial', 'B', 5);

        for ($i = 0; $i<=2; $i++) {
            if ($i==0) {
                $pdf->Cell(5, 3, $header[$i], 0, 0, "C");
            } elseif ($i==1) {
                $pdf->Cell(20, 3, $header[$i], 0, 0, "L");
            } elseif ($i==2) {
                $pdf->Cell(20, 3, $header[$i], 0, 0, "L");
            }
        }
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->SetFont('Arial', 'B', 5);

        foreach ($dataPdf as $row) {
            for ($i = 0;$i<=3;$i++) {
                if ($i==0) {
                    $pdf->SetX(2);
                    $pdf->Cell(5, $textypos, $row[$i], 0, 0, "C");
                } elseif ($i==1) {
                    $pdf->setX(7);
                    $pdf->Cell(20, $textypos, substr(utf8_decode($row[$i]), 0, 15)."...", 0, 0, "L");
                } elseif ($i==2) {
                    $pdf->setX(27);
                    $pdf->Cell(20, $textypos, $row[$i], 0, 0, "L");
                }
            }
            $pdf->Ln();
        }
        $pdf->Output();
    }
    public function comanda_print_cocina($idPedido)
    {
        $dataPdf = array();

        $dataComanda = $this->ComandaspedidosModel->get_where(array("idpedido"=>$idPedido, "status_comanda"=>0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto"=>$c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria"=>$dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar=='c') {
                $dataPdf[] = array(
                0 => $c->cantidad,
                1 => $dataProducto[0]->producto,
                2 => $c->comentario
            );
            }
        }
        $dataPedido = $this->PedidosModel->get_by_id($idPedido);
        $dataMesero = $this->MeserosModel->get_by_id($dataPedido[0]->idmesero);
        $mesero = $dataMesero[0]->nombre." ".$dataMesero[0]->apellidos;


        $header = array('CANT.', 'PRODUCTO', 'COMENTARIO');

        $pdf = new FPDF($orientation='P', $unit='mm', array(45,350));
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 8);    //Letra Arial, negrita (Bold), tam. 20
        $textypos = 5;
        $pdf->SetXY(2, 2);
        $pdf->Cell(42, $textypos, "COMANDA PARA COCINA", 0, 0, "C");
        $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20
        

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(40, $textypos, "Mesero: ".$mesero , 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(5, $textypos, '====================================');


        $pdf->Ln();
        $pdf->setX(2);
        $pdf->SetFont('Arial', 'B', 5);

        for ($i = 0; $i<=2; $i++) {
            if ($i==0) {
                $pdf->Cell(5, 3, $header[$i], 0, 0, "C");
            } elseif ($i==1) {
                $pdf->Cell(20, 3, $header[$i], 0, 0, "L");
            } elseif ($i==2) {
                $pdf->Cell(20, 3, $header[$i], 0, 0, "L");
            }
        }
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->SetFont('Arial', 'B', 5);

        foreach ($dataPdf as $row) {
            for ($i = 0;$i<=3;$i++) {
                if ($i==0) {
                    $pdf->SetX(2);
                    $pdf->Cell(5, $textypos, $row[$i], 0, 0, "C");
                } elseif ($i==1) {
                    $pdf->setX(7);
                    $pdf->Cell(20, $textypos, substr(utf8_decode($row[$i]), 0, 15)."...", 0, 0, "L");
                } elseif ($i==2) {
                    $pdf->setX(27);
                    $pdf->Cell(20, $textypos, $row[$i], 0, 0, "L");
                }
            }
            $pdf->Ln();
        }
        $pdf->Output();
    }
    */
}
