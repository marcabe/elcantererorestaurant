<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");
require('fpdf/fpdf.php');

class Pedidos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('PedidosModel');
        $this->load->Model('MesasModel');
        $this->load->Model('MeserosModel');
        $this->load->Model('ComandasModel');
        $this->load->Model('ClientesModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('ComandaspedidosModel');
        $this->load->Model('SubaperturaModel');
        $this->load->Model('ConfigModel');
    }



    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataSub = $this->SubaperturaModel->get_where(array("status" => 1));

        #Verificacamos si existe el mesero en la base de datos
        $dataMesero = $this->MeserosModel->get_by_id($dataInsert['idmesero']);
        if (count($dataMesero) > 0) {
            $dataInsert['idsubapertura'] = $dataSub[0]->idsub_apertura;
            $idPedido = $this->PedidosModel->insert($dataInsert);
            echo $idPedido;
        } else {
            echo 0;
        }
    }

    public function get_by_id($idPedido)
    {
        $dataPedido = $this->PedidosModel->get_by_id($idPedido);
        foreach ($dataPedido as $p) {
            $dataMesero  = $this->MeserosModel->get_where(array("idmesero" => $p->idmesero));
            $p->mesero = $dataMesero[0];

            $datCliente = $this->ClientesModel->get_where(array("idcliente" => $p->idcliente));

            $p->cliente = $datCliente[0];

            $dataComanda = $this->ComandaspedidosModel->get_where(array("idpedido" => $idPedido, "status" => 1));
            $p->comanda = $dataComanda;
            foreach ($p->comanda as $c) {
                $dataPorducto = $this->ProductosModel->get_by_id($c->idproducto);
                $c->producto = $dataPorducto[0];
            }
        }
        echo json_encode($dataPedido[0]);
    }

    public function termina_pedido($idpedido)
    {
        $dataTermina  = $this->input->post();
        $dataTermina['fecha_final'] = date("Y-m-d");
        $dataTermina['hora_final'] = date("H:i");
        $this->PedidosModel->update($idpedido, $dataTermina);



        $info_pedido = array();
        $dataPedidos = $this->PedidosModel->get_by_id($idpedido);
        if ($dataPedidos[0]->metodo_pago == 1) {
            $dataPedidos[0]->metodo_pago = "Efectivo";
        } elseif ($dataPedidos[0]->metodo_pago == 2) {
            $dataPedidos[0]->metodo_pago = "T. de Crédito";
        } elseif ($dataPedidos[0]->metodo_pago == 3) {
            $dataPedidos[0]->metodo_pago = "T. de Débito";
        }
        $dataMesero = $this->MeserosModel->get_by_id($dataPedidos[0]->idmesero);
        $dataComanda = $this->ComandaspedidosModel->get_where(array("idpedido" => $idpedido, "status!=" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $c->producto = $dataProducto[0];
        }

        $dataImpresiorCorte = $this->ConfigModel->get_icorte();


        $info_pedido['pedido'] = $dataPedidos[0];
        $info_pedido['mesero'] = $dataMesero[0];
        $info_pedido['comanda'] = $dataComanda;
        $info_pedido["info"] = array(
            "fecha" => date("Y-m-d H:i"),
            "impresora_corte" => $dataImpresiorCorte[0]->impresora_corte

        );
        echo json_encode($info_pedido);
    }

    /*public function ticket($idPedido)
    {


        $dataPedido = $this->PedidosModel->get_by_id($idPedido);
        $dataMesero = $this->MeserosModel->get_by_id($dataPedido[0]->idmesero);
        $dataCliente = $this->ClientesModel->get_by_id($dataPedido[0]->idcliente);

        $dataComanda = $this->ComandaspedidosModel->get_where(array("idpedido"=>$idPedido));
        $dataPdf  = array();
        $subtotal = 0;
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto"=>$c->idproducto));
            $dataPdf[] = array(
                0 => $dataProducto[0]->producto,
                1 => $c->cantidad,
                2 => $dataProducto[0]->precio,
                3 => ($dataProducto[0]->precio * $c->cantidad)
            );
            $subtotal += ($dataProducto[0]->precio * $c->cantidad);
        }

        switch ($dataPedido[0]->metodo_pago) {
            case 1:
                $mp = "Efectivo";
                break;
            case 2:
                $mp = "Credito";
                break;
            case 3:
                $mp = "Débito";
                break;
        }


        $pdf = new FPDF($orientation='P', $unit='mm', array(45,350));
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 8);    //Letra Arial, negrita (Bold), tam. 20
        $textypos = 3;
        $pdf->setY(2);

        $pdf->Image('assets/develop/images/logo.png', 10, 0, -300, 0);
        $pdf->Ln();
        $pdf->setXY(2, 10);
        //$pdf->Cell(5, $textypos, '________________________________________');
        $pdf->SetFont('Arial', '', 7);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->setXY(2, 15);
        $pdf->Cell(42, $textypos, "Expedido el: ".date("Y/m/d H:i")." en", 0, 0, "L");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Guerrero 25  Centro 47980", 0, 0, "L");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Degollado Jalisco", 0, 0, "C");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Mesero: ".$dataMesero[0]->nombre, 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, $textypos, "Nota de venta : NV ".$idPedido, 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(0);
        $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->Cell(1, $textypos, '-------------------------------------------------------------------------');
        $pdf->Ln();

        $pdf->setX(2);
        $pdf->Cell(17, $textypos, utf8_decode("Descripción"), 0, 0, "C");
        $pdf->setX(19);
        $pdf->Cell(5, $textypos, "Cant. ", 0, 0, "C");
        $pdf->setX(24);
        $pdf->Cell(10, $textypos, "Pr.Un. ", 0, 0, "C");
        $pdf->setX(34);
        $pdf->Cell(10, $textypos, "Importe", 0, 0, "C");
        $pdf->Ln();
        $pdf->setX(0);
        $pdf->Cell(1, $textypos, '-------------------------------------------------------------------------');
        $pdf->setX(2);
        $pdf->SetFont('Arial', '', 4);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->Ln();

        foreach ($dataPdf as $row) {
            for ($i = 0;$i<=3;$i++) {
                if ($i==0) {
                    $pdf->SetX(2);
                    $pdf->Cell(15, $textypos, substr(utf8_decode($row[$i]), 0, 15)."...", 0, 0, "C");
                } elseif ($i==1) {
                    $pdf->setX(19);
                    $pdf->Cell(5, $textypos, $row[$i], 0, 0, "C");
                } elseif ($i==2) {
                    $pdf->setX(24);
                    $pdf->Cell(10, $textypos, "$". number_format($row[$i], 2), 0, 0, "C");
                } elseif ($i==3) {
                    $pdf->setX(34);
                    $pdf->Cell(10, $textypos, "$". number_format($row[$i], 2), 0, 0, "C");
                }
            }
            $pdf->Ln();
        }

        $pdf->setX(0);
        $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->Cell(45, $textypos, '=======================', 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "SUB TOTAL:", 0, 0, "L");
        $pdf->Cell(20, 5, "$".number_format($subtotal, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "Impuestos", 0, 0, "L");
        $pdf->Cell(20, 5, "$".number_format(0.0, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, $mp, 0, 0, "L");
        $pdf->Cell(20, 5, "$".number_format($dataPedido[0]->cuanto_pago, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(0);
        $pdf->Cell(45, $textypos, '=======================', 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "Total pago:", 0, 0, "L");
        $pdf->Cell(20, 5, "$".number_format($dataPedido[0]->cuanto_pago, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(20, 5, "Cambio: ", 0, 0, "L");
        $pdf->Cell(20, 5, "$".number_format($dataPedido[0]->cambio, 2), 0, 0, "R");
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(1, $textypos, '----------------------------------------------------------------------------------');
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(40, $textypos, 'GRACIAS POR SU COMPRA', 0, 0, "C");

        $pdf->Output();
    }*/
}
