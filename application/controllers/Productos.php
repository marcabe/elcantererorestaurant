<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Productos extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('ProductosModel');
        $this->load->Model('CategoriasModel');

    }

    

    public function insert()
    {
        $dataInsert = $this->input->post();
        $this->ProductosModel->insert($dataInsert);
        echo 1;
    }

    public function update($idproducto){
        $dataUpdate = $this->input->post();
        $this->ProductosModel->update($idproducto, $dataUpdate);
    }


    public function get_like()
    {
        $json = [];
        $data = $this->input->get("q");
        $dataProductos  = $this->ProductosModel->get_like($data);
        echo json_encode($dataProductos);
    }

    public function get_id($idProducto)
    {
        $dataProducto = $this->ProductosModel->get_by_id($idProducto);
        echo json_encode($dataProducto[0]);
    }
}
