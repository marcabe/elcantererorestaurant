<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class Rutes extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('VentasModel');
        $this->load->Model('ClientesModel');
        $this->load->Model('UsuariosModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('MeserosModel');
        $this->load->Model('CategoriasModel');
        $this->load->Model('MesasModel');
        $this->load->Model('PedidosModel');
        $this->load->Model('AperturaModel');
        $this->load->Model('SubaperturaModel');
        $this->load->Model('ProveedoresModel');
        $this->load->Model('AperturaModel');
        $this->load->Model('FaltantesModel');
        $this->load->Model('ListafaltantesModel');
        $this->load->Model('ConfigModel');


        $this->hoy = date("Y-m-d");
        $this->mes = date("m");
        $this->anio = date("Y");
        $this->finicio = date("Y") . "-" . date("m") . "-" . "01";
        $this->ffinal = date("Y") . "-" . date("m") . "-" . date("t", strtotime("01-" . date("m") . "-" . date("Y")));
    }


    public function filtro($mes, $anio)
    {
        $array_respuesta = array();
        $finicio = $anio . "-" . $mes . "-" . "01";
        $ffinal = $anio . "-" . $mes . "-" . date("t", strtotime("01-" . $mes . "-" . $anio));


        switch ($mes) {
            case 1:
                $titulo = "Informacion del mes de Enero del año " . $anio;
                break;
            case 2:
                $titulo = "Informacion del mes de Febrero del año " . $anio;
                break;
            case 3:
                $titulo = "Informacion del mes de Marzo del año " . $anio;
                break;
            case 4:
                $titulo = "Informacion del mes de Abril del año " . $anio;
                break;
            case 5:
                $titulo = "Informacion del mes de Mayo del año " . $anio;
                break;
            case 6:
                $titulo = "Informacion del mes de Junio del año " . $anio;
                break;
            case 7:
                $titulo = "Informacion del mes de Julio del año " . $anio;
                break;
            case 8:
                $titulo = "Informacion del mes de Agosto del año " . $anio;
                break;
            case 9:
                $titulo = "Informacion del mes de Septiembre del año " . $anio;
                break;
            case 10:
                $titulo = "Informacion del mes de Octubre del año " . $anio;
                break;
            case 11:
                $titulo = "Informacion del mes de Noviembre del año " . $anio;
                break;
            case 12:
                $titulo = "DInformacion del mes de iciembre del año " . $anio;
                break;
        }
        $array_respuesta['titulo'] = $titulo;

        $ventas_del_mes = 0;
        $pedidos_del_mes = 0;
        $total_ventas_mensual = 0;
        $total_pedidos_mensual = 0;
        $dataVentas = $this->VentasModel->get_where(array("status" => 2, "hora_inicio>=" => $finicio, "fecha_final<=" => $ffinal));
        $total_ventas_mensual += count($dataVentas);
        foreach ($dataVentas as $v) {
            $ventas_del_mes += $v->total;
        }
        $dataPedidos = $this->PedidosModel->get_where(array("status" => 2, "hora_inicio>=" => $finicio, "fecha_final<=" => $ffinal));
        $total_pedidos_mensual += count($dataPedidos);
        foreach ($dataPedidos as $p) {
            $pedidos_del_mes += $p->total;
        }
        #Seccion para la grafica 1
        #Array para la grafica 1 (ventass vs pedidos)
        $venta_vs_pedido_mensual = array();
        $obj_v_p_mensual = new \stdClass;
        $obj_v_p_mensual->tipo = "Ventas";
        $obj_v_p_mensual->monto = $ventas_del_mes;
        $venta_vs_pedido_mensual[0] = $obj_v_p_mensual;


        $obj_v_p_mensual = new \stdClass;
        $obj_v_p_mensual->tipo = "Pedidos";
        $obj_v_p_mensual->monto = $pedidos_del_mes;
        $venta_vs_pedido_mensual[1] = $obj_v_p_mensual;
        $array_respuesta['venta_vs_pedidos_pie'] = $venta_vs_pedido_mensual;



        $array_respuesta['total_ventas_mensual'] = $total_ventas_mensual;
        $array_respuesta['total_pedidos_mensual'] = $total_pedidos_mensual;
        $array_respuesta['ventas_del_mes'] = $ventas_del_mes;
        $array_respuesta['pedidos_del_mes'] = $pedidos_del_mes;
        #Array para la grafica 2 (ventass vs pedidos diarios)
        $venta_vs_pedido_mensual_barra = array();
        $diafinal = date("t", strtotime("01-" . $mes . "-" . $anio));

        for ($i = 1; $i <= $diafinal; $i++) {
            $dataVentas = $this->VentasModel->get_where(array("status" => 2, "DATE(fecha_final) " => $anio . "-" . $mes . "-" . $i));
            $monto_venta = 0;
            if (count($dataVentas) > 0) {
                foreach ($dataVentas as $v) {
                    $monto_venta += $v->total;
                }
            }

            $dataPedidos = $this->PedidosModel->get_where(array("status" => 2, "DATE(fecha_final) " => $anio . "-" . $mes . "-" . $i));
            $monto_pedidos = 0;
            if (count($dataPedidos) > 0) {
                foreach ($dataPedidos as $p) {
                    $monto_pedidos += $p->total;
                }
            }

            $graf = new \stdClass;
            $graf->day = date("Y") . "-" . date("m") . "-" . $i;
            $graf->ventas =  $monto_venta;
            $graf->pedidos =  $monto_pedidos;

            $venta_vs_pedido_mensual_barra[] = $graf;
        }

        $array_respuesta['venta_vs_pedidos_barra'] = $venta_vs_pedido_mensual_barra;
        echo json_encode($array_respuesta);
    }

    public function ventas()
    {
        $this->session->sess_destroy();
        $dataMesasCocina = $this->MesasModel->get_where(array("croquis" => "Cocina"));
        $dataMesasPatio = $this->MesasModel->get_where(array("croquis" => "Patio"));
        $dataMesasPrecidencial = $this->MesasModel->get_where(array("croquis" => "Presidencial"));
        $dataMesasRedonda = $this->MesasModel->get_where(array("croquis" => "Redonda"));
        $dataMesasCuartito = $this->MesasModel->get_where(array("croquis" => "Cuartito"));

        $dataPedidos = $this->PedidosModel->get();

        foreach ($dataPedidos as $p) {
            $dataMesero = $this->MeserosModel->get_by_id($p->idmesero);
            $p->mesero = $dataMesero[0]->nombre . " " . $dataMesero[0]->apellidos;
            $p->status = ($p->status == 1) ? "Abierto" : "Cerrado";
        }


        $data = array(
            "mesascocina" => $dataMesasCocina,
            "mesaspatio" => $dataMesasPatio,
            "mesasprecidencial" => $dataMesasPrecidencial,
            "mesasredonda" => $dataMesasRedonda,
            "mesascocuartito" => $dataMesasCuartito,
            "pedidos" => $dataPedidos
        );


        $this->load->view("lista_mesas", $data);
    }


    public function productos()
    {
        if (is_null($this->session->userdata("canterero"))) {
            redirect(base_url());
        } else {
            $dataProductos = $this->ProductosModel->get();
            foreach ($dataProductos as $p) {
                $dataCategoria = $this->CategoriasModel->get_by_id($p->idcategoria);
                if ($dataCategoria[0]->lugar == 'b') {
                    $dataCategoria[0]->lugar = 'Barra';
                } elseif ($dataCategoria[0]->lugar == 'c') {
                    $dataCategoria[0]->lugar = 'Cocina';
                }
                $p->categoria = $dataCategoria[0];
            }
            $dataCategorias = $this->CategoriasModel->get_where(array("status" => 1));

            $dataProveedores = $this->ProveedoresModel->get_where(array("status" => 1));

            $dataFaltantes = $this->FaltantesModel->get();

            $data = array(
                "productos" => $dataProductos,
                "categorias" => $dataCategorias,
                "proveedores" => $dataProveedores,
                "faltantes" => $dataFaltantes,
                "active" => array(
                    0 => "active",
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => ''
                )
            );
            $this->load->view("productos", $data);
        }
    }

    public function personal()
    {
        if (is_null($this->session->userdata("canterero"))) {
            redirect(base_url());
        } else {
            $dataUsuarios = $this->UsuariosModel->get();
            foreach ($dataUsuarios as $u) {
                switch ($u->tipo) {
                    case 1:
                        $u->type = "Administrador";
                        break;
                    case 2:
                        $u->type = "Encargado";
                        break;
                }
            }
            $dataMeseros = $this->MeserosModel->get();
            $data = array(
                "usuarios" => $dataUsuarios,
                "meseros" => $dataMeseros,
                "active" => array(
                    0 => "",
                    1 => 'active',
                    2 => '',
                    3 => '',
                    4 => ''
                )
            );

            $this->load->view("usuarios", $data);
        }
    }

    public function clientes()
    {
        if (is_null($this->session->userdata("canterero"))) {
            redirect(base_url());
        } else {
            $dataClientes = $this->ClientesModel->get();
            $data = array(
                "clientes" => $dataClientes,
                "active" => array(
                    0 => "",
                    1 => '',
                    2 => 'active',
                    3 => '',
                    4 => ''
                )
            );
            $this->load->view("clientes", $data);
        }
    }




    public function indicadores()
    {
        if (is_null($this->session->userdata("canterero"))) {
            redirect(base_url());
        } else {

            #Pediente el dash

            /*$dataApertura = $this->AperturaModel->get_where(array("status"=>2, "hora_inicio>="=>$this->finicio, "hora_inicio<="=>$this->ffinal));
            foreach ($dataApertura as $a) {
                $dataSub = $this->SubaperturaModel->get_where(array("idapertura"=>$a->idapertura_principal));
                foreach ($dataSub as $sa) {
                    $dataVentas = $this->VentasModel->get_where(array("status"=>2, "idsubapertura"=>$sa->idsub_apertura));
                    $total_ventas_mensual += count($dataVentas);
                    foreach ($dataVentas as $v) {
                        $ventas_del_mes+= $v->total;
                    }
                    $dataPedidos = $this->PedidosModel->get_where(array("status"=>2, "idsubapertura"=>$sa->idsub_apertura));
                    $total_pedidos_mensual += count($dataPedidos);
                    foreach ($dataPedidos as $p) {
                        $pedidos_del_mes+= $p->total;
                    }
                }
            }*/


            #Datos para la gráfica
            $ventas_del_mes = 0;
            $pedidos_del_mes = 0;

            $total_ventas_mensual = 0;
            $total_pedidos_mensual = 0;
            $dataVentas = $this->VentasModel->get_where(array("status" => 2, "hora_inicio>=" => $this->finicio, "fecha_final<=" => $this->ffinal));
            $total_ventas_mensual += count($dataVentas);
            foreach ($dataVentas as $v) {
                $ventas_del_mes += $v->total;
            }

            $dataPedidos = $this->PedidosModel->get_where(array("status" => 2, "hora_inicio>=" => $this->finicio, "fecha_final<=" => $this->ffinal));
            $total_pedidos_mensual += count($dataPedidos);
            foreach ($dataPedidos as $p) {
                $pedidos_del_mes += $p->total;
            }

            #Seccion para la grafica 1
            #Array para la grafica 1 (ventass vs pedidos)
            $venta_vs_pedido_mensual = array();

            $obj_v_p_mensual = new \stdClass;
            $obj_v_p_mensual->tipo = "Ventas";
            $obj_v_p_mensual->monto = $ventas_del_mes;
            $venta_vs_pedido_mensual[0] = $obj_v_p_mensual;


            $obj_v_p_mensual = new \stdClass;
            $obj_v_p_mensual->tipo = "Pedidos";
            $obj_v_p_mensual->monto = $pedidos_del_mes;
            $venta_vs_pedido_mensual[1] = $obj_v_p_mensual;
            #----------------------------------------------------------#


            #Array para la grafica 2 (ventass vs pedidos diarios)
            $venta_vs_pedido_mensual_barra = array();
            $diafinal = date("d");
            for ($i = 1; $i <= $diafinal; $i++) {
                $dataVentas = $this->VentasModel->get_where(array("status" => 2, "DATE(fecha_final) " => $this->anio . "-" . $this->mes . "-" . $i));
                $monto_venta = 0;
                if (count($dataVentas) > 0) {
                    foreach ($dataVentas as $v) {
                        $monto_venta += $v->total;
                    }
                }

                $dataPedidos = $this->PedidosModel->get_where(array("status" => 2, "DATE(fecha_final) " => $this->anio . "-" . $this->mes . "-" . $i));
                $monto_pedidos = 0;
                if (count($dataPedidos) > 0) {
                    foreach ($dataPedidos as $p) {
                        $monto_pedidos += $p->total;
                    }
                }

                $graf = new \stdClass;
                $graf->day = date("Y") . "-" . date("m") . "-" . $i;
                $graf->ventas =  $monto_venta;
                $graf->pedidos =  $monto_pedidos;

                $venta_vs_pedido_mensual_barra[] = $graf;
            }





            $total_al_dia = 0;
            $mesas_atendidas = 0;
            $pedidos_atendidos = 0;
            $ventas_del_dia = 0;
            $pedidos_del_dia = 0;
            $venta_vs_pedido = array();
            $dataApertura = $this->AperturaModel->get_where(array("status" => 1));
            if (count($dataApertura) > 0) {
                #Ya hay caja abierta
                #Obtenemos los datos de la subapertura
                $dataSub = $this->SubaperturaModel->get_where(array("idapertura" => $dataApertura[0]->idapertura_principal));
                #Obtenemos el total de cuantos turnos hay
                $tam_sub = count($dataSub);
                #Si existe mas de 1 obtenemos los datos de toda la sub_apertura
                if ($tam_sub > 0) {
                    foreach ($dataSub as $sa) {
                        if ($sa->status == 2) {
                            $sa->total_apertura = $sa->monto_apertura + $sa->monto_pedido + $sa->monto_venta - $sa->pagos_provedor;
                            $total_al_dia += $sa->total_apertura;
                        } elseif ($sa->status == 1) {
                            #Buscamos los pedidos y ventas que estan en esa subapertura
                            $total_v = 0;
                            $total_p = 0;
                            $dataVentas = $this->VentasModel->get_where(array("status" => 2, "idsubapertura" => $sa->idsub_apertura));
                            foreach ($dataVentas as $v) {
                                $total_v += $v->total;
                            }
                            $dataPedidos = $this->PedidosModel->get_where(array("status" => 2, "idsubapertura" => $sa->idsub_apertura));
                            foreach ($dataPedidos as $p) {
                                $total_p += $p->total;
                            }
                            $sa->monto_venta = $total_v;
                            $sa->monto_pedido = $total_p;
                            $sa->total_apertura = $sa->monto_apertura  + $total_v + $total_p - $sa->pagos_provedor;
                        }
                    }
                }
                #Guardamos los datos de todas las subapaerturas en la apertura principal que le correcomento
                $dataApertura[0]->subapertura = $dataSub;

                $dataApertura[0]->tam_sub = $tam_sub;

                $ultimo = count($dataApertura) - 1;

                $dataSubas = $this->SubaperturaModel->get_where(array("idapertura" => $dataApertura[$ultimo]->idapertura_principal));
                foreach ($dataSubas as $sa) {
                    #Obtenemos las ventas que corresponde al la apertura principal
                    $dataVentas = $this->VentasModel->get_where(array("status" => 2, "idsubapertura" => $sa->idsub_apertura));
                    $mesas_atendidas += count($dataVentas);
                    #Hacemos un for para obtener el total de las ventas del día
                    foreach ($dataVentas as $v) {
                        $ventas_del_dia += $v->total;
                    }
                    #Obtenemos los que corresponde al la apertura principal
                    $dataPedidos = $this->PedidosModel->get_where(array("status" => 2, "idsubapertura" => $sa->idsub_apertura));
                    $pedidos_atendidos += count($dataPedidos);
                    #Hacemos un for para obtener el total de los pedidos del día
                    foreach ($dataPedidos as $p) {
                        $pedidos_del_dia += $p->total;
                    }
                }
                #Obtenemos datos para la gráfica
                $obj_v_p = new \stdClass;
                $obj_v_p->tipo = "Ventas";
                $obj_v_p->monto = $ventas_del_dia;
                $venta_vs_pedido[0] = $obj_v_p;
                $obj_v_p = new \stdClass;
                $obj_v_p->tipo = "Pedidos";
                $obj_v_p->monto = $pedidos_del_dia;
                $venta_vs_pedido[1] = $obj_v_p;
            }

            $mes_actual = date("m");

            switch ($mes_actual) {
                case 1:
                    $n_mes = "Enero";
                    break;
                case 2:
                    $n_mes = "Febrero";
                    break;
                case 3:
                    $n_mes = "Marzo";
                    break;
                case 4:
                    $n_mes = "Abril";
                    break;
                case 5:
                    $n_mes = "Mayo";
                    break;
                case 6:
                    $n_mes = "Junio";
                    break;
                case 7:
                    $n_mes = "Julio";
                    break;
                case 8:
                    $n_mes = "Agosto";
                    break;
                case 9:
                    $n_mes = "Septiembre";
                    break;
                case 10:
                    $n_mes = "Octubre";
                    break;
                case 11:
                    $n_mes = "Noviembre";
                    break;
                case 12:
                    $n_mes = "Diciembre";
                    break;
            }

            $anio_actual = date("Y");
            $data = array(
                "mes_actual" => $mes_actual,
                "anio_actual" => $anio_actual,
                "n_mes" => $n_mes,
                "mesas_atendidas" => $mesas_atendidas,
                "ventas_del_dia" => number_format($ventas_del_dia, 2, '.', ','),
                "pedidos_atendidos" => $pedidos_atendidos,
                "pedidos_del_dia" => number_format($pedidos_del_dia, 2, '.', ','),
                "active" => array(
                    0 => "",
                    1 => '',
                    2 => '',
                    3 => 'active',
                    4 => ''
                ),
                "apertura" => $dataApertura,
                "venta_vs_pedido" => json_encode($venta_vs_pedido),
                "total_al_dia" => $total_al_dia,
                "venta_vs_pedido_mensual" => json_encode($venta_vs_pedido_mensual),
                "venta_vs_pedido_mensual_barra" => json_encode($venta_vs_pedido_mensual_barra),
                "ventas_del_mes" => number_format($ventas_del_mes, 2, '.', ','),
                "pedidos_del_mes" => number_format($pedidos_del_mes, 2, '.', ','),
                "total_ventas_mensual" => $total_ventas_mensual,
                "total_pedidos_mensual" => $total_pedidos_mensual

            );
            $this->load->view("indicadores", $data);
        }
    }


    public function configuracion()
    {
        $dataImpresora_barra = $this->ConfigModel->get_ibarra();
        $dataImpresora_cocina = $this->ConfigModel->get_icocina();
        $dataImpresora_corte = $this->ConfigModel->get_icorte();

        $data = array(
            "active" => array(
                0 => "",
                1 => '',
                2 => '',
                3 => 'active',
                4 => ''
            ),
            "impresora_barra" => $dataImpresora_barra[0]->impresora_barra,
            "impresora_cocina" => $dataImpresora_cocina[0]->impresora_cocina,
            "impresora_corte" => $dataImpresora_corte[0]->impresora_corte,
        );
        $this->load->view("config", $data);
    }

    public function insert_impresora()
    {
        $dataInput = $this->input->post();
        var_dump($dataInput);
        $impresora_asignar = $dataInput['impresora_asignar'];
        $this->ConfigModel->update(array($impresora_asignar => $dataInput['impresora']));
        echo 1;
    }


    public function faltantes()
    {
        $dataFaltantes = $this->FaltantesModel->get();
        foreach ($dataFaltantes as $f) {
            $dataLista  = $this->ListafaltantesModel->get_where(array("idfaltante" => $f->idfaltante));
            $f->lista = $dataLista;
        }
        $data = array(
            "faltantes" => $dataFaltantes
        );
        $this->load->view("faltantes", $data);
    }
}
