<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('UsuariosModel');
        $this->load->Model('MeserosModel');
    }

    public function login()
    {
        $this->session->sess_destroy();
        $this->load->view('login');
    }

    public function validacion()
    {
        $data = $this->input->post();
        $where = array(
            "usuario" => $data['usuario']
        );
        $datosUsua = $this->UsuariosModel->get_where($where);
        if (count($datosUsua) > 0 && $datosUsua[0]->status == 1) {
            $data['clave'] = md5($data['clave']);
            $response = $this->UsuariosModel->get_where($data);
            if (count($response) > 0) {
                $arraydata = array(
                    'nombre' => $response[0]->nombre,
                    'usuario' => $response[0]->usuario,
                    'rol' => $response[0]->tipo,
                    'log_in' => true
                );
                $this->session->set_userdata("canterero", $arraydata);
                echo 2;
            } else {
                #La clave no esta bien
                echo 1;
            }
        } else {
            #El usuario no existe
            echo 0;
        }
    }

   


    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataClientes = $this->UsuariosModel->get_by_id($dataInsert['usuario']);
        if (count($dataClientes)>0) {
            echo 0;
        } else {
            $dataInsert['clave'] = md5($dataInsert['clave']);
            $this->UsuariosModel->insert($dataInsert);
            echo 1;
        }
    }
    public function update($usuario)
    {
        $dataUpdate = $this->input->post();
        $this->UsuariosModel->update($usuario, $dataUpdate);
        echo 1;
    }

    public function actualizar($usuario)
    {
        $dataUpdate = $this->input->post();
        $dataUpdate['clave'] = md5($dataUpdate['clave']);
        if ($usuario == $dataUpdate['usuario']) {
            #Es el mismo y solo se van a cambiar atributos
            $this->UsuariosModel->update($usuario, $dataUpdate);
        } else {
            $dataUsuario = $this->UsuariosModel->get_by_id($dataUpdate['usuario']);
            if (count($dataUsuario)>0) {
                #Si existe un igual
                echo 0;
            } else {
                $this->UsuariosModel->update($usuario, $dataUpdate);
                echo 1;
            }
        }
    }

    public function get_id($usuario)
    {
        $dataUsuario = $this->UsuariosModel->get_by_id($usuario);
        echo json_encode($dataUsuario[0]);
    }
}
