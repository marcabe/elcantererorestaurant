<?php

#Controlador apertura principal
#Elaborado por Marco Antonio Caronda
#igng.marco.cardona@gmail.com
#2021

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");
require('fpdf/fpdf.php');

class Apertura extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('AperturaModel');
        $this->load->Model('SubaperturaModel');
        $this->load->Model('VentasModel');
        $this->load->Model('PedidosModel');
        $this->load->Model('UsuariosModel');
        $this->load->Model('RegpagopModel');
        $this->load->Model('ProveedoresModel');
        $this->load->Model('ConfigModel');
        $this->load->Model('MesasModel');
        $this->load->Model('MeserosModel');
    }

    public function search($fecha)
    {
        $dataApertura = $this->AperturaModel->get_where(array("DATE(hora_inicio)" => $fecha,));
        foreach ($dataApertura as $a) {
            $dataUsuarioa = $this->UsuariosModel->get_by_id($a->usuario_apertura);
            if (count($dataUsuarioa) > 0) {
                $a->usuario_c = $dataUsuarioa[0]->nombre;
            } else {
                $a->usuario_c = "----------";
            }
            $dataUsuarioc = $this->UsuariosModel->get_by_id($a->usuario_cierre);
            if (count($dataUsuarioc) > 0) {
                $a->usuario_c = $dataUsuarioc[0]->nombre;
            } else {
                $a->usuario_c = "----------";
            }
        }
        echo json_encode($dataApertura);
    }

    public function ticket_corte($id_apertura)
    {
        $info_corte = array();

        $dataApertura = $this->AperturaModel->get_by_id($id_apertura);
        $dataSuba = $this->SubaperturaModel->get_where(array("idapertura" => $dataApertura[0]->idapertura_principal));
        foreach ($dataSuba as $s) {
            $total_mesas_caja = 0;
            $total_pedido_caja = 0;
            $total_proveedor_caja = 0;

            $dataVentas = $this->VentasModel->get_where(array("idsubapertura" => $s->idsub_apertura));
            foreach ($dataVentas as $v) {
                $total_mesas_caja += $v->total;
                $dataMesa = $this->MesasModel->get_where(array("idmesa" => $v->idmesa));
                $v->mesas = $dataMesa[0];
                $dataMeseros = $this->MeserosModel->get_where(array("idmesero" => $v->idmesero));
                $v->meseros = $dataMeseros[0];
            }

            $s->ventas = $dataVentas;

            $dataPedidos = $this->PedidosModel->get_where(array("idsubapertura" => $s->idsub_apertura));
            foreach ($dataPedidos as $p) {
                $total_pedido_caja += $p->total;
                $dataMeseros = $this->MeserosModel->get_where(array("idmesero" => $p->idmesero));
                $p->meseros = $dataMeseros[0];
            }

            $s->pedidos = $dataPedidos;

            $dataPagosP = $this->RegpagopModel->get_where(array("idsuba" => $s->idsub_apertura));
            foreach ($dataPagosP as $p) {
                $total_proveedor_caja += $p->monto;
                $dataProveedor = $this->ProveedoresModel->get_where(array("idpro" => $p->idproveedor));
                $p->proveedor = $dataProveedor[0];
            }

            $s->pagos = $dataPagosP;

            $s->total_mesas_caja =  $total_mesas_caja;
            $s->total_pedido_caja =  $total_pedido_caja;
            $s->total_proveedor_caja =  $total_proveedor_caja;
            $s->en_caja =  $total_mesas_caja + $total_pedido_caja - $total_proveedor_caja + $s->monto_apertura;
        }
        $dataImpresiorCorte = $this->ConfigModel->get_icorte();


        $info_corte['suba'] = $dataSuba;
        $info_corte["info"] = array(
            "fecha" => date("Y-m-d H:i"),
            "impresora_corte" => $dataImpresiorCorte[0]->impresora_corte
        );
        echo json_encode($info_corte);
    }


    public function imprimir($id_apertura)
    {
        $dataApertura = $this->AperturaModel->get_by_id($id_apertura);
        foreach ($dataApertura as $a) {
            $dataUsuario = $this->UsuariosModel->get_by_id($a->usuario_apertura);
            $a->usuario = $dataUsuario[0];
            $dataUsuarioc = $this->UsuariosModel->get_by_id($a->usuario_cierre);
            if (count($dataUsuarioc) > 0) {
                $usuario_cierre = $dataUsuarioc[0]->nombre;
            } else {
                $usuario_cierre = "----------";
            }
        }
        $pdf = new FPDF('L', 'mm', "Letter");
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 12);


        $pdf->SetX(10);
        $pdf->SetY(10);
        $pdf->Cell(260, 5, "APERTURA PRINCIPAL", 0, 0, "C");

        $pdf->SetFont('Arial', 'B', 8);

        $pdf->SetY(20);
        $pdf->SetX(20);
        $pdf->Cell(10, 5, "ID AP.", 1, 0, "C");
        $pdf->SetX(30);
        $pdf->Cell(40, 5, "HORA INICIO.", 1, 0, "C");
        $pdf->SetX(70);
        $pdf->Cell(40, 5, "USUA. AP.", 1, 0, "C");
        $pdf->SetX(110);
        $pdf->Cell(40, 5, "HORA FINAL", 1, 0, "C");
        $pdf->SetX(150);
        $pdf->Cell(40, 5, "USUA. CIERR.", 1, 0, "C");
        $pdf->SetX(190);
        $pdf->Cell(30, 5, "TOTAL AP.", 1, 0, "C");
        $pdf->SetX(220);
        $pdf->Cell(30, 5, "TOTAL PAG. PROV.", 1, 0, "C");


        $pdf->SetY(25);
        $pdf->SetX(20);
        $pdf->Cell(10, 5, $dataApertura[0]->idapertura_principal, 1, 0, "C");
        $pdf->SetX(30);
        $pdf->Cell(40, 5, $dataApertura[0]->hora_inicio, 1, 0, "C");
        $pdf->SetX(70);
        $pdf->Cell(40, 5, $dataApertura[0]->usuario->nombre, 1, 0, "C");
        $pdf->SetX(110);
        $pdf->Cell(40, 5, $dataApertura[0]->hora_final, 1, 0, "C");
        $pdf->SetX(150);
        $pdf->Cell(40, 5, $usuario_cierre, 1, 0, "C");
        $pdf->SetX(190);
        $pdf->Cell(30, 5, $dataApertura[0]->monto_cierre, 1, 0, "C");
        $pdf->SetX(220);
        $pdf->Cell(30, 5, $dataApertura[0]->monto_cierre_p, 1, 0, "C");

        $pdf->SetX(10);
        $pdf->SetY(35);
        $pdf->SetFont('Arial', 'B', 12);

        $pdf->Cell(260, 5, "APERTURA DE CAJAS EN EL TURNO", 0, 0, "C");

        $dataSuba = $this->SubaperturaModel->get_where(array("idapertura" => $dataApertura[0]->idapertura_principal));
        $pdf->SetX(10);
        $pdf->SetY(45);
        $y = 45;
        $i = 1;
        foreach ($dataSuba as $s) {
            $total_mesas_caja = 0;
            $total_pedido_caja = 0;
            $total_proveedor_caja = 0;

            $pdf->SetY($y);
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(260, 5, "CAJA NO. " . $i, 0, 0, "C");
            $y += 10;
            $pdf->SetFont('Arial', 'B', 6);
            $pdf->SetY($y);
            $pdf->SetX(10);
            $pdf->Cell(25, 5, "HORA INICIO.", 1, 0, "C");
            $pdf->SetX(35);
            $pdf->Cell(40, 5, "USUA. AP.", 1, 0, "C");
            $pdf->SetX(75);
            $pdf->Cell(25, 5, "HORA FINAL", 1, 0, "C");
            $pdf->SetX(100);
            $pdf->Cell(40, 5, "USUA. CIERR.", 1, 0, "C");
            $pdf->SetX(140);
            $pdf->Cell(20, 5, "MONTO VENTA", 1, 0, "C");
            $pdf->SetX(160);
            $pdf->Cell(20, 5, "MONTO PEDIDO", 1, 0, "C");
            $pdf->SetX(180);
            $pdf->Cell(25, 5, "MONTO APERTURA", 1, 0, "C");
            $pdf->SetX(205);
            $pdf->Cell(25, 5, "PAGO A PROVEEDOR", 1, 0, "C");
            $pdf->SetX(230);
            $pdf->Cell(25, 5, "EN CAJA ", 1, 0, "C");
            $y = $y + 5;
            $dataVentas = $this->VentasModel->get_where(array("idsubapertura" => $s->idsub_apertura));
            foreach ($dataVentas as $v) {
                $total_mesas_caja += $v->total;
            }
            $dataPedidos = $this->PedidosModel->get_where(array("idsubapertura" => $s->idsub_apertura));

            foreach ($dataPedidos as $p) {
                $total_pedido_caja += $v->total;
            }
            $dataPagosP = $this->RegpagopModel->get_where(array("idsuba" => $s->idsub_apertura));

            foreach ($dataPagosP as $p) {
                $total_proveedor_caja += $p->monto;
            }

            $pdf->SetY($y);
            $pdf->SetX(10);
            $pdf->Cell(25, 5, $s->hora_inicio, 1, 0, "C");
            $pdf->SetX(35);
            $pdf->Cell(40, 5, $s->usuario_apertura, 1, 0, "C");
            $pdf->SetX(75);
            $pdf->Cell(25, 5, $s->fecha_cierre, 1, 0, "C");
            $pdf->SetX(100);
            $pdf->Cell(40, 5, $s->usuario_cierre, 1, 0, "C");
            $pdf->SetX(140);
            $pdf->SetTextColor(41, 154, 11);
            $pdf->Cell(20, 5, (isset($s->monto_venta)) ? $s->monto_venta : "$ " . number_format($total_mesas_caja, 2, '.', ','), 1, 0, "C");
            $pdf->SetX(160);
            $pdf->Cell(20, 5, (isset($s->monto_pedido)) ? $s->monto_pedido : "$ " . number_format($total_pedido_caja, 2, '.', ','), 1, 0, "C");
            $pdf->SetX(180);
            $pdf->Cell(25, 5, "$" . number_format($s->monto_apertura, 2, '.', ','), 1, 0, "C");
            $pdf->SetX(205);
            $pdf->SetTextColor(207, 4, 4);
            $pdf->Cell(25, 5, (isset($s->pagos_provedor)) ? "$ " . number_format($s->pagos_provedor, 2, '.', ',') : "$ " . number_format($total_proveedor_caja, 2, '.', ','), 1, 0, "C");
            $pdf->SetTextColor(0, 0, 0);
            $pdf->SetX(230);

            $en_caja = $total_mesas_caja + $total_pedido_caja - $total_proveedor_caja + $s->monto_apertura;
            $pdf->Cell(25, 5, "$" . number_format($en_caja, 2, '.', ','), 1, 0, "C");
            $y = $y + 10;

            //$dataVentas = $this->VentasModel->get_where(array("idsubapertura"=>$s->idsub_apertura));
            //$dataPedidos = $this->PedidosModel->get_where(array("idsubapertura"=>$s->idsub_apertura));
            //$dataPagosP = $this->RegpagopModel->get_where(array("idsuba"=>$s->idsub_apertura));
            foreach ($dataPagosP as $p) {
                $dataProve = $this->ProveedoresModel->get_by_id($p->idproveedor);
                $p->n_prove = $dataProve[0]->nombre;
            }

            $pdf->SetY($y);

            $pdf->Cell(90, 5, "VENTAS EN TURNO", 0, 0, "C");
            $pdf->Cell(90, 5, "PEDIDOS EN TURNO", 0, 0, "C");
            $pdf->Cell(90, 5, "PAGO A PROVEEDORES", 0, 0, "C");
            $y = $y + 10;
            $yp = $y;
            $yv = $y;
            $ypro = $y;

            #VENTAS
            $pdf->SetY($yv);
            $pdf->SetX(10);
            $pdf->Cell(25, 5, "NO. MESA", 1, 0, "C");
            $pdf->SetX(35);
            $pdf->Cell(20, 5, "MESERO", 1, 0, "C");
            $pdf->SetX(55);
            $pdf->Cell(25, 5, "PROPINA", 1, 0, "C");
            $pdf->SetX(80);
            $pdf->Cell(20, 5, "TOTAL", 1, 0, "C");
            $total_mesas = 0;
            $pdf->SetX(10);
            $yv += 5;
            $pdf->SetY($yv);
            foreach ($dataVentas as $v) {
                $pdf->SetY($yv);
                $pdf->SetX(10);
                $pdf->MultiCell(25, 10, $v->idmesa, 1, "L", false);
                $pdf->SetY($yv);
                $pdf->SetX(35);
                $pdf->MultiCell(20, 10, $v->idmesero, 1, "L", false);
                $pdf->SetY($yv);
                $pdf->SetX(55);
                $pdf->MultiCell(25, 10, "$" . number_format($v->monto_propina, 2, '.', ','), 1, "L", false);
                $pdf->SetY($yv);
                $pdf->SetX(80);
                $pdf->MultiCell(20, 10, "$" . number_format($v->total, 2, '.', ','), 1, "L", false);
                $total_mesas += $v->total;
                $pdf->Ln();
                $yv += 10;
            }
            $pdf->SetY($yv);
            $pdf->SetX(10);
            $pdf->MultiCell(90, 5, "TOTAL= $" . number_format($total_mesas, 2, '.', ','), 1, "L", false);
            #fIN vENTAS


            #PEDIDOS
            $pdf->SetY($yp);
            $pdf->SetX(105);
            $pdf->Cell(25, 5, "FECHA", 1, 0, "C");
            $pdf->SetX(130);
            $pdf->Cell(20, 5, "MESERO", 1, 0, "C");
            $pdf->SetX(150);
            $pdf->Cell(25, 5, "TOTAL", 1, 0, "C");
            $total_pedidos = 0;
            $pdf->SetX(105);
            $yp += 5;
            $pdf->SetY($yp);
            foreach ($dataPedidos as $p) {
                $pdf->SetY($yp);
                $pdf->SetX(105);
                $pdf->MultiCell(25, 10, $p->hora_inicio, 1, "L", false);
                $pdf->SetY($yp);
                $pdf->SetX(130);
                $pdf->MultiCell(20, 10, $p->idmesero, 1, "L", false);
                $pdf->SetY($yp);
                $pdf->SetX(150);
                $pdf->MultiCell(25, 10, $p->total, 1, "L", false);
                $total_pedidos += $v->total;
                $pdf->Ln();
                $yp += 10;
            }
            $pdf->SetY($yp);
            $pdf->SetX(105);
            $pdf->MultiCell(70, 5, "TOTAL= $" . number_format($total_pedidos), 1, "R", false);
            #FIN PEDIDOS

            #ROVEEDORES
            $pdf->SetY($ypro);
            $pdf->SetX(180);
            $pdf->Cell(25, 5, "FECHA", 1, 0, "C");
            $pdf->SetX(205);
            $pdf->Cell(25, 5, "PROVEEDOR", 1, 0, "C");
            $pdf->SetX(230);
            $pdf->Cell(30, 5, "COMENTARIOS", 1, 0, "C");
            $pdf->SetX(260);
            $pdf->Cell(20, 5, "TOTAL", 1, 0, "C");
            $total_pagos_p = 0;
            $pdf->SetX(180);
            $ypro += 5;
            $pdf->SetY($ypro);
            foreach ($dataPagosP as $p) {
                $pdf->SetY($ypro);
                $pdf->SetX(180);
                $pdf->MultiCell(25, 10, $p->fecha, 1, "L", false);
                $pdf->SetY($ypro);
                $pdf->SetX(205);
                $pdf->MultiCell(25, 10, $p->n_prove, 1, "L", false);
                $pdf->SetY($ypro);
                $pdf->SetX(230);
                $pdf->MultiCell(30, 10, $pdf->MultiCell(30, 2, utf8_decode($p->descripcion), 1, "L", false), 0, "L", false);
                $pdf->SetY($ypro);
                $pdf->SetX(260);
                $pdf->MultiCell(20, 10, "$" . number_format($p->monto), 1, "L", false);
                $total_pagos_p += $p->monto;
                $pdf->Ln();
                $ypro += 10;
            }

            $pdf->SetY($ypro);
            $pdf->SetX(180);
            $pdf->MultiCell(100, 5, "TOTAL= $" . number_format($total_pagos_p), 1, "R", false);


            $coord = array($yp, $yv, $ypro);

            if ($y > 120) {
                $pdf->AddPage();
                $pdf->SetX(10);
                $pdf->SetY(10);
                $y = 10;
            } else {
                $y = max($coord) + 10;
            }
            $i++;
        }



        $pdf->Output('paginaEnBlanco.pdf', 'I');
    }


    public function verificar_abierta()
    {
        $dataApertura = $this->AperturaModel->get_where(array("status" => 1));
        if (count($dataApertura) > 0) {
            #Si esta aperturada la caja
            echo 1;
        } else {
            #No esta aperturada la caja
            echo 0;
        }
    }

    public function nueva_apertura()
    {
        #Recibimos los datos a ingresar
        $dataInsert = $this->input->post();

        #Guardamos en una varianle la sesion
        $sesion = $this->session->userdata("canterero");

        #Creamos un arreglo para ingresar la apertura
        $dataNueva = array(
            "usuario_apertura" => $sesion['usuario'],
            "status" => 1
        );

        #Guardamos la apertura de la caja principal y recibimos el id
        $idapertura = $this->AperturaModel->insert($dataNueva);

        #Creamos un arreglo para
        $datasubnueva = array(
            "idapertura" => $idapertura,
            "usuario_apertura" => $sesion['usuario'],
            "status" => 1,
            "monto_apertura" => $dataInsert['monto_apertura']
        );

        #Guardamos la subapertura
        $idSub = $this->SubaperturaModel->insert($datasubnueva);

        echo 1;
    }


    public function cerrar_apertura()
    {
        $dataVentas = $this->VentasModel->get_where(array("status" => 1));
        $dataPedidos = $this->PedidosModel->get_where(array("status" => 1));
        if (count($dataVentas) > 0 || count($dataPedidos) > 0) {
            echo 0;
        } else {
            $sesion = $this->session->userdata("canterero");
            $idapertura = $this->input->post("idapertura");
            $dataCerrar = array(
                "usuario_cierre" => $sesion['usuario'],
                "status" => 2
            );
            $this->AperturaModel->update($idapertura, $dataCerrar);

            #Obtenemos el ultimo turno abierto
            $dataSuba = $this->SubaperturaModel->get_where(array("idapertura" => $idapertura, "status" => 1));

            #Sumamos el total de la suma de la subapertura
            $dataVentas = $this->VentasModel->get_where(array("status" => 2, "idsubapertura" => $dataSuba[0]->idsub_apertura));
            $final_apertura_ventas = 0;
            foreach ($dataVentas as $v) {
                $final_apertura_ventas += $v->total;
            }

            #Summamos todos los pedidos de la apertura anterios
            $dataPedido = $this->PedidosModel->get_where(array("status" => 2, "idsubapertura" => $dataSuba[0]->idsub_apertura));
            $total_apertura_p = 0;
            foreach ($dataPedido as $p) {
                $total_apertura_p += $p->total;
            }

            $this->SubaperturaModel->update($dataSuba[0]->idsub_apertura, array("status" => 2, "usuario_cierre" => $sesion['usuario'], "monto_venta" => $final_apertura_ventas, "monto_pedido" => $total_apertura_p));


            $dataSuba = $this->SubaperturaModel->get_where(array("idapertura" => $idapertura, "status" => 2));
            $total_apertura_venta = 0;
            $total_apertura_pedido = 0;

            foreach ($dataSuba as $s) {
                $total_apertura_venta += $s->monto_venta;
                $total_apertura_pedido += $s->monto_pedido;
            }
            $this->AperturaModel->update($idapertura, array("monto_cierre" => $total_apertura_venta, "monto_cierre_p" => $total_apertura_pedido));
            echo 1;
        }
    }

    public function cron()
    {
        $dataApertura = $this->AperturaModel->get();

        foreach ($dataApertura as $a) {
            $total_cierre = 0;
            $total_cierrep = 0;
            echo "--------------------------------------" . $a->idapertura_principal . "---------------------------------<br>";

            $dataSuba = $this->SubaperturaModel->get_where(array("idapertura" => $a->idapertura_principal));
            foreach ($dataSuba as $s) {
                $total_venta = 0;
                $total_ventap = 0;
                $total_pagop = 0;
                $dataVentas = $this->VentasModel->get_where(array("idsubapertura" => $s->idsub_apertura));
                foreach ($dataVentas as $v) {
                    $total_venta += $v->total;
                }
                $dataPedidos = $this->PedidosModel->get_where(array("idsubapertura" => $s->idsub_apertura));
                foreach ($dataPedidos as $p) {
                    $total_ventap += $p->total;
                }

                $dataPagosP = $this->RegpagopModel->get_where(array("idsuba" => $s->idsub_apertura));
                foreach ($dataPagosP as $p) {
                    $total_pagop += $p->monto;
                }
                echo $s->idsub_apertura . "Total Venta = " . $total_venta . " Total Pedidos = " . $total_ventap . " y pago a proveedor= " . $total_pagop . "<br>";
                $total_cierre += $total_venta;
                $total_cierrep += $total_ventap;
                $this->SubaperturaModel->update($s->idsub_apertura, array("monto_venta" => $total_venta, "monto_pedido" => $total_ventap, "pagos_provedor" => $total_pagop));
            }

            echo "-------Monto Cierre: " . $total_cierre . "----Monto Cierre Pedido: " . $total_cierrep . "--------------<br><br>";
            $this->AperturaModel->update($a->idapertura_principal, array("monto_cierre" => $total_cierre, "monto_cierre_p" => $total_cierrep));
        }
    }
}
