<?php

#Controlador apertura principal
#Elaborado por Marco Antonio Caronda
#igng.marco.cardona@gmail.com
#2021

defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");

class Proveedores extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('ProveedoresModel');
    }

    public function insert(){
        $dataInsert = $this->input->post();
        $this->ProveedoresModel->insert($dataInsert);
        echo 1;
    }

    public function get_id($idpro)
    {
        $dataProveedor = $this->ProveedoresModel->get_by_id($idpro);
        echo json_encode($dataProveedor[0]);
    }

    public function update($idpro){
        $dataUpdate = $this->input->post();
        $this->ProveedoresModel->update($idpro, $dataUpdate);
        echo 1;
    }

    public function get(){
        $dataPost = $this->input->post();
        $dataProveedores = $this->ProveedoresModel->get_where($dataPost);
        echo json_encode($dataProveedores);
    }


}
