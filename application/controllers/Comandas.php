<?php
defined('BASEPATH') or exit('No direct script access allowed');
require('fpdf/fpdf.php');

class Comandas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('VentasModel');
        $this->load->Model('ComandasModel');
        $this->load->Model('ProductosModel');
        $this->load->Model('UsuariosModel');
        $this->load->Model('CancelacionespvModel');
        $this->load->Model('AperturaModel');
        $this->load->Model('SubaperturaModel');
        $this->load->Model('CategoriasModel');
        $this->load->Model('MeserosModel');
        $this->load->Model('MesasModel');
        $this->load->Model('ConfigModel');
    }


    public function comanda($idVenta)
    {

        #Primero pasamos las comandas a status 1 para solo imprimir las nuevas
        $dataComandas = $this->ComandasModel->get_where(array("idventa" => $idVenta));
        foreach ($dataComandas as $c) {
            $this->ComandasModel->update($c->comanda, array("status_comanda" => 1));
        }


        $ventaData  = $this->VentasModel->get_by_id($idVenta);
        $dataComanda = $this->ComandasModel->get_where(array("idventa" => $idVenta, "status!=" => 0));
        foreach ($dataComanda as $c) {
            $dataPorducto = $this->ProductosModel->get_by_id($c->idproducto);
            $c->producto = $dataPorducto[0];
        }
        $data = array(
            "venta" => $ventaData[0],
            "comanda" => $dataComanda
        );
        $this->load->view("comanda", $data);
    }

    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataInsert = json_decode($dataInsert["comanda"]);
        $idVenta = $dataInsert[0]->idventa;
        foreach ($dataInsert as $insert) {
            $this->ComandasModel->insert($insert);
        }

        $comandas = array();

        $dataBarra = array();
        $dataComanda = $this->ComandasModel->get_where(array("idventa" => $idVenta, "status_comanda" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria" => $dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar == 'b') {
                $barra = new \stdClass;
                $barra->cantidad = $c->cantidad;
                $barra->producto = strtoupper($dataProducto[0]->producto);
                $barra->comentario = strtoupper($c->comentario);
                $dataBarra[] = $barra;
            }
        }

        $dataCocina = array();
        $dataComanda = $this->ComandasModel->get_where(array("idventa" => $idVenta, "status_comanda" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria" => $dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar == 'c') {
                $cocina = new \stdClass;
                $cocina->cantidad = $c->cantidad;
                $cocina->producto = strtoupper($dataProducto[0]->producto);
                $cocina->comentario = strtoupper($c->comentario);
                $dataCocina[] = $cocina;
            }
        }

        $dataVenta = $this->VentasModel->get_where(array("idventa" => $idVenta));
        $dataMesa = $this->MesasModel->get_where(array("idmesa" => $dataVenta[0]->idmesa));

        $dataImpresioraBarra = $this->ConfigModel->get_ibarra();
        $dataImpresioraCocina = $this->ConfigModel->get_icocina();

        $comandas["barra"] = $dataBarra;
        $comandas["cocina"] = $dataCocina;
        $comandas["info"] = array(
            "fecha" => date("Y-m-d H:i"),
            "no_mesa" => $dataMesa[0]->nomesa,
            "ubicacion" => $dataMesa[0]->croquis,
            "impresora_barra" => $dataImpresioraBarra[0]->impresora_barra,
            "impresora_cocina" => $dataImpresioraCocina[0]->impresora_cocina
        );
        echo json_encode($comandas);
    }


    public function delete($comanda)
    {
        $dataCondicion = $this->input->post();
        $dataCondicion['clave'] = md5($dataCondicion['clave']);

        $where = array(
            "usuario" => $dataCondicion['usuario'],
            "clave" => $dataCondicion['clave']
        );
        $user = $this->UsuariosModel->get_where($where);
        if (count($user) > 0) {
            #Existe el usuario y su clave
            $this->ComandasModel->update($comanda, array("status" => 0));
            $this->CancelacionespvModel->insert(array("motivo" => $dataCondicion['motivo'], "usuario" => $dataCondicion['usuario'], "idcomanda" => $comanda));
            echo 1;
        } else {
            #No existe el usuario
            echo 0;
        }
    }


    /*
    public function comanda_print_barra($idVenta)
    {
        $dataPdf = array();

        $dataComanda = $this->ComandasModel->get_where(array("idventa" => $idVenta, "status_comanda" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria" => $dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar == 'b') {
                $dataPdf[] = array(
                    0 => $c->cantidad,
                    1 => $dataProducto[0]->producto,
                    2 => $c->comentario
                );
            }
        }
        if (count($dataPdf) > 0) {
            $dataVenta = $this->VentasModel->get_by_id($idVenta);
            $dataMesero = $this->MeserosModel->get_by_id($dataVenta[0]->idmesero);
            $mesero = $dataMesero[0]->nombre . " " . $dataMesero[0]->apellidos;

            $dataMesa = $this->MesasModel->get_by_id($dataVenta[0]->idmesa);


            $header = array('CANT.', 'PRODUCTO', 'COMENTARIO');

            $pdf = new FPDF($orientation = 'P', $unit = 'mm', array(45, 350));
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 8);    //Letra Arial, negrita (Bold), tam. 20
            $textypos = 5;
            $pdf->SetXY(2, 2);
            $pdf->Cell(42, $textypos, "COMANDA PARA BARRA", 0, 0, "C");
            $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20


            $pdf->Ln();
            $pdf->setX(2);
            $pdf->Cell(42, $textypos, "Mesa: " . $dataMesa[0]->nomesa, 0, 0, "L");

            $pdf->Ln();
            $pdf->setX(2);
            $pdf->Cell(40, $textypos, "Mesero: " . $mesero, 0, 0, "L");

            $pdf->Ln();
            $pdf->setX(2);
            $pdf->Cell(5, $textypos, '====================================');


            $pdf->Ln();
            $pdf->setX(2);
            $pdf->SetFont('Arial', 'B', 4);

            for ($i = 0; $i <= 2; $i++) {
                if ($i == 0) {
                    $pdf->Cell(5, 3, $header[$i], 0, 0, "C");
                } elseif ($i == 1) {
                    $pdf->Cell(15, 3, $header[$i], 0, 0, "L");
                } elseif ($i == 2) {
                    $pdf->Cell(25, 3, $header[$i], 0, 0, "L");
                }
            }
            $pdf->Ln();
            $pdf->Ln();
            $pdf->setX(2);
            $pdf->SetFont('Arial', 'B', 4);

            $y = 0;
            foreach ($dataPdf as $row) {
                for ($i = 0; $i <= 3; $i++) {
                    if ($i == 0) {
                        $pdf->SetX(2);
                        $pdf->Cell(5, $textypos, $row[$i], 0, 0, "C");
                    } elseif ($i == 1) {
                        $y = $pdf->GetY();
                        $pdf->setX(7);
                        $pdf->MultiCell(15, $textypos, utf8_decode($row[$i]), 0, "L", false);
                    } elseif ($i == 2) {
                        $pdf->setY($y);
                        $pdf->setX(22);
                        $pdf->MultiCell(20, 2, $row[$i], 0, "L", false);
                    }
                }
                $pdf->Ln();
                $pdf->Ln();
            }
            $pdf->Output();
        } else {
            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
        }
    }

    public function comanda_print_cocina($idVenta)
    {
        $dataPdf = array();

        $dataComanda = $this->ComandasModel->get_where(array("idventa" => $idVenta, "status_comanda" => 0));
        foreach ($dataComanda as $c) {
            $dataProducto = $this->ProductosModel->get_where(array("idproducto" => $c->idproducto));
            $dataCategoria = $this->CategoriasModel->get_where(array("idcategoria" => $dataProducto[0]->idcategoria));
            if ($dataCategoria[0]->lugar == 'c') {
                $dataPdf[] = array(
                    0 => $c->cantidad,
                    1 => $dataProducto[0]->producto,
                    2 => $c->comentario
                );
            }
        }
        if (count($dataPdf) > 0) {
            $dataVenta = $this->VentasModel->get_by_id($idVenta);
            $dataMesero = $this->MeserosModel->get_by_id($dataVenta[0]->idmesero);
            $mesero = $dataMesero[0]->nombre . " " . $dataMesero[0]->apellidos;

            $dataMesa = $this->MesasModel->get_by_id($dataVenta[0]->idmesa);

            $header = array('CANT.', 'PRODUCTO', 'COMENTARIO');

            $pdf = new FPDF($orientation = 'P', $unit = 'mm', array(45, 350));
            $pdf->AddPage();
            $pdf->SetFont('Arial', 'B', 8);    //Letra Arial, negrita (Bold), tam. 20
            $textypos = 5;
            $pdf->setY(2);
            $pdf->setX(2);
            $pdf->Cell(42, $textypos, "COMANDA PARA COCINA", 0, 0, "C");
            $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20
            $pdf->Ln();
            $pdf->setX(2);
            $pdf->Cell(42, $textypos, "Mesa: " . $dataMesa[0]->nomesa, 0, 0, "L");

            $pdf->Ln();
            $pdf->setX(2);
            $pdf->Cell(40, $textypos, "Mesero: " . $mesero, 0, 0, "L");

            $pdf->Ln();
            $pdf->setX(2);
            $pdf->Cell(5, $textypos, '====================================');


            $pdf->Ln();
            $pdf->setX(2);
            $pdf->SetFont('Arial', 'B', 4);

            for ($i = 0; $i <= 2; $i++) {
                if ($i == 0) {
                    $pdf->Cell(5, 3, $header[$i], 0, 0, "C");
                } elseif ($i == 1) {
                    $pdf->Cell(15, 3, $header[$i], 0, 0, "L");
                } elseif ($i == 2) {
                    $pdf->Cell(25, 3, $header[$i], 0, 0, "L");
                }
            }
            $pdf->Ln();
            $pdf->Ln();
            $pdf->setX(2);
            $pdf->SetFont('Arial', 'B', 4);

            $y = 0;
            foreach ($dataPdf as $row) {
                for ($i = 0; $i <= 3; $i++) {
                    if ($i == 0) {
                        $pdf->SetX(2);
                        $pdf->Cell(5, $textypos, $row[$i], 0, 0, "C");
                    } elseif ($i == 1) {
                        $y = $pdf->GetY();
                        $pdf->setX(7);
                        $pdf->MultiCell(15, $textypos, utf8_decode($row[$i]), 0, "L", false);
                    } elseif ($i == 2) {
                        $pdf->setY($y);
                        $pdf->setX(22);
                        $pdf->MultiCell(20, 2, $row[$i], 0, "L", false);
                    }
                }
                $pdf->Ln();
                $pdf->Ln();
            }
            $pdf->Output();
        } else {
            echo "<script languaje='javascript' type='text/javascript'>window.close();</script>";
        }
    }
    */
}
