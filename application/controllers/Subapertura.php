<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Subapertura extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('AperturaModel');
        $this->load->Model('SubaperturaModel');
        $this->load->Model('VentasModel');
        $this->load->Model('PedidosModel');
    }


   
    public function nueva_apertura()
    {
        $sesion = $this->session->userdata("canterero");

        $idsub = $this->input->post("idsub");
        $datasa = $this->SubaperturaModel->get_by_id($idsub);
        $idapertura = $datasa[0]->idapertura;

        $dataNueva = array(
            "usuario_cierre"=> $sesion['usuario'],
            "status"=>2
        );

        $this->SubaperturaModel->update($idsub, $dataNueva);

        $datasubnueva = array(
            "idapertura"=>$idapertura,
            "usuario_apertura"=> $sesion['usuario'],
            "status"=>1,
            "monto_apertura"=> $this->input->post("monto_apertura")
        );

        $idSubnew = $this->SubaperturaModel->insert($datasubnueva);
        #Pasamos todos las ventas en status 1 no terminadas al nuevo turno
        $dataVentas = $this->VentasModel->get_where(array("status"=>1));
        foreach ($dataVentas as $v) {
            $this->VentasModel->Update($v->idventa, array("idsubapertura"=>$idSubnew));
        }

        #Pasamos todos los pedidos en status 1 no terminadas al nuevo turno
        $dataPedidos = $this->PedidosModel->get_where(array("status"=>1));
        foreach ($dataPedidos as $p) {
            $this->PedidosModel->update($p->idpedido, array("idsubapertura"=>$idSubnew));
        }

        #Summamos todas las ventas de la apertura anterios
        $dataVentas = $this->VentasModel->get_where(array("status"=>2, "idsubapertura"=>$idsub));
        $total_apertura = 0;
        foreach ($dataVentas as $v) {
            $total_apertura += $v->total;
        }

        #Summamos todos los pedidos de la apertura anterios
        $dataPedido = $this->PedidosModel->get_where(array("status"=>2, "idsubapertura"=>$idsub));
        $total_apertura_p = 0;
        foreach ($dataPedido as $p) {
            $total_apertura_p += $p->total;
        }


        $this->SubaperturaModel->update($idsub, array("monto_venta"=>$total_apertura, "monto_pedido"=>$total_apertura_p));
        echo 1;
    }
}
