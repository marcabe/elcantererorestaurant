<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set("America/Mexico_City");
require('fpdf/fpdf.php');
class Meseros extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->Model('MeserosModel');
        $this->load->Model('VentasModel');
        $this->load->Model('PedidosModel');
        $this->load->Model('MesasModel');
        $this->load->Model('ConfigModel');
    }


    public function get_where($idmesero)
    {
        $dataMesero  = $this->MeserosModel->get_by_id($idmesero);
        if (count($dataMesero) > 0) {
            echo json_encode($dataMesero[0]);
        } else {
            echo false;
        }
    }

    public function ticket_venta_pedido($mesero, $fecha)
    {
        $ticket = array();
        $dataVentas = $this->VentasModel->get_where(array('idmesero' => $mesero, 'status' => 2, 'DATE(hora_inicio)' => $fecha));
        foreach ($dataVentas as $v) {
            $dataMesa = $this->MesasModel->get_where(array("idmesa" => $v->idmesa));
            $v->mesa_info = $dataMesa[0];
        }
        $dataPedidos = $this->PedidosModel->get_where(array('idmesero' => $mesero, 'status' => 2, 'DATE(hora_inicio)' => $fecha));
        $dataMesero = $this->MeserosModel->get_by_id($mesero);

        $dataImpresioraCocina = $this->ConfigModel->get_icorte();


        $ticket['ventas'] = $dataVentas;
        $ticket['pedidos'] = $dataPedidos;
        $ticket['mesero'] = $dataMesero[0];
        $ticket['info'] = array(
            "fecha" => date("Y-m-d H:i"),
            "impresora_corte" => $dataImpresioraCocina[0]->impresora_corte
        );

        echo json_encode($ticket);
    }


    public function reporte_vp($mesero, $fecha)
    {
        $dataVentas = $this->VentasModel->get_where(array('idmesero' => $mesero, 'status' => 2, 'DATE(hora_inicio)' => $fecha));
        $dataPedidos = $this->PedidosModel->get_where(array('idmesero' => $mesero, 'status' => 2, 'DATE(hora_inicio)' => $fecha));
        $dataMesero = $this->MeserosModel->get_by_id($mesero);


        $header = array('MESA', 'PROPINA', 'TOTAL');
        $header2 = array('HORA', 'TOTAL');

        $pdf = new FPDF($orientation = 'P', $unit = 'mm', array(45, 350));
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 8);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->setY(2);
        $pdf->setX(2);
        $pdf->Cell(42, 5, "VENTAS MESERO", 0, 0, "C");
        $pdf->SetFont('Arial', '', 5);    //Letra Arial, negrita (Bold), tam. 20
        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(42, 5, "Fecha:  " . $fecha, 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(40, 5, "Mesero: " . $dataMesero[0]->nombre, 0, 0, "L");

        $pdf->Ln();
        $pdf->setX(2);
        $pdf->Cell(5, 5, '===========LISTA DE VENTAS=============');


        $pdf->Ln();
        $pdf->setX(2);
        $pdf->SetFont('Arial', 'B', 5);

        for ($i = 0; $i <= 2; $i++) {
            if ($i == 0) {
                $pdf->Cell(10, 3, $header[$i], 0, 0, "C");
            } elseif ($i == 1) {
                $pdf->Cell(15, 3, $header[$i], 0, 0, "C");
            } elseif ($i == 2) {
                $pdf->Cell(15, 3, $header[$i], 0, 0, "C");
            }
        }

        $pdf->Ln();
        $pdf->setX(10);
        $pdf->SetFont('Arial', 'B', 5);
        $y = 25;
        $total_mesas = 0;
        foreach ($dataVentas as $v) {
            $y += 3;
            $pdf->SetY($y);
            $pdf->SetX(2);
            $pdf->Cell(10, 3, $v->idmesa, 1, 0, "C");
            $pdf->SetX(12);
            $pdf->Cell(15, 3, "$" . number_format($v->monto_propina, 2, '.', ','), 1, 0, "C");
            $pdf->SetX(27);
            $pdf->Cell(15, 3, "$" . number_format($v->total, 2, '.', ','), 1, 0, "C");
            $total_mesas += $v->total;
        }
        $y += 3;
        $pdf->SetY($y);
        $pdf->SetX(2);
        $pdf->Cell(40, 3, "TOTAL= $" . number_format($total_mesas, 2, '.', ','), 1, 0, "R");


        $y = $y + 5;
        $pdf->SetY($y);
        $pdf->SetX(2);
        $pdf->Cell(5, 5, '===========LISTA DE PEDIDOS=============');
        $y = $y + 5;
        $pdf->SetY($y);
        $pdf->SetX(2);
        $pdf->SetFont('Arial', 'B', 5);
        for ($i = 0; $i <= 2; $i++) {
            if ($i == 0) {
                $pdf->Cell(20, 3, $header2[$i], 0, 0, "C");
            } elseif ($i == 1) {
                $pdf->Cell(20, 3, $header2[$i], 0, 0, "C");
            }
        }


        $total_pedidos = 0;
        foreach ($dataPedidos as $p) {
            $y += 3;
            $pdf->SetY($y);
            $pdf->SetX(2);
            $pdf->Cell(20, 3, $p->hora_inicio, 1, 0, "C");
            $pdf->SetX(22);
            $pdf->Cell(20, 3, "$" . number_format($p->total, 2, '.', ','), 1, 0, "C");
            $total_pedidos += $p->total;
        }
        $y += 3;
        $pdf->SetY($y);
        $pdf->SetX(2);
        $pdf->Cell(40, 3, "TOTAL= $" . number_format($total_pedidos, 2, '.', ','), 1, 0, "R");

        $pdf->Output('paginaEnBlanco.pdf', 'I');
    }

    public function insert()
    {
        $dataInsert = $this->input->post();
        $dataMesero = $this->MeserosModel->get_by_id($dataInsert['usuario']);
        if (count($dataMesero) > 0) {
            echo 0;
        } else {
            $this->MeserosModel->insert($dataInsert);
            echo 1;
        }
    }

    public function update($mesero)
    {
        $dataUpdate = $this->input->post();
        $this->MeserosModel->update($mesero, $dataUpdate);
        echo 1;
    }

    public function actualizar($mesero)
    {
        $dataUpdate = $this->input->post();
        if ($mesero == $dataUpdate['idmesero']) {
            #Es el mismo y solo se van a cambiar atributos
            $this->MeserosModel->update($mesero, $dataUpdate);
        } else {
            $dataMesero = $this->MeserosModel->get_by_id($dataUpdate['idmesero']);
            if (count($dataMesero) > 0) {
                #Si existe un igual
                echo 0;
            } else {
                $this->MeserosModel->update($mesero, $dataUpdate);
                echo 1;
            }
        }
    }

    public function get_id($mesero)
    {
        $dataMesero = $this->MeserosModel->get_by_id($mesero);
        echo json_encode($dataMesero[0]);
    }


    public function reporte_ventas_pedidos()
    {
        $dataConsulta = $this->input->post();

        $dataVentas = $this->VentasModel->get_where(array('idmesero' => $dataConsulta['mesero_uuid'], 'status' => 2, 'DATE(hora_inicio)' => $dataConsulta['fecha_mesero']));
        $dataPedidos = $this->PedidosModel->get_where(array('idmesero' => $dataConsulta['mesero_uuid'], 'status' => 2, 'DATE(hora_inicio)' => $dataConsulta['fecha_mesero']));
        $respuesta = array(
            "ventas" => $dataVentas,
            "pedidos" => $dataPedidos
        );

        echo json_encode($respuesta);
    }
}
