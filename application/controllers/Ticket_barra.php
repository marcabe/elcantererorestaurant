<?php

require('fpdf/fpdf.php');

class Ticket_barra extends FPDF
{
	public function __construct()
	{
		parent::__construct();
	}

// Tabla simple
	function BasicTable($header, $data)
	{
		// Cabecera
		$this->SetXY(12, 80);
		$this->SetFont('Arial', 'B', 9);

		foreach ($header as $col) {
			$this->Cell(38, 5, $col, 0, 0, "C");
		}
		$this->Ln();
		$this->Ln();
		// Datos

		foreach ($data as $row) {
			$this->SetX(12);

			foreach ($row as $col) {
				$this->Cell(38, 10, $col, 0, 0, "C");

			}
			$this->Ln();
		}
	}

}
