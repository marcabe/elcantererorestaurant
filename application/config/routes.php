<?php
defined('BASEPATH') or exit('No direct script access allowed');

#Sin login
$route['faltantes'] = 'Rutes/faltantes';

#Vista para los meseros
$route['ventas'] = 'Rutes/ventas';
$route['ventas/comanda/(:any)'] = 'Comandas/comanda/$1';
$route['pedidos/comanda/(:any)'] = 'ComandasPedidos/comanda/$1';




#Vista para el administrador
$route['default_controller'] = 'Usuarios/login';

$route['admin/productos'] = 'Rutes/productos';
$route['admin/personal'] = 'Rutes/personal';
$route['admin/clientes'] = 'Rutes/clientes';
$route['admin/indicadores'] = 'Rutes/indicadores';
$route['admin/configuracion'] = 'Rutes/configuracion';


$route['404_override'] = '';
$route['translate_uri_dashes'] = false;
