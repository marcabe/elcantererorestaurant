$(document).ready(function () {


    var data_mas_vendido = [{
        country: "USA",
        medals: 110
    }, {
        country: "China",
        medals: 100
    }, {
        country: "Russia",
        medals: 72
    }, {
        country: "Britain",
        medals: 47
    }, {
        country: "Australia",
        medals: 46
    }, {
        country: "Germany",
        medals: 41
    }, {
        country: "France",
        medals: 40
    }, {
        country: "South Korea",
        medals: 31
    }];

    let mas_vendido = $("#pie").dxPieChart({
        palette: "bright",
        dataSource: data_mas_vendido,
        title: "Producto más vendido",
        legend: {
            orientation: "horizontal",
            itemTextPosition: "right",
            horizontalAlignment: "center",
            verticalAlignment: "bottom",
            columnCount: 4
        },
        "export": {
            enabled: true
        },
        series: [{
            argumentField: "country",
            valueField: "medals",
            label: {
                visible: true,
                font: {
                    size: 16
                },
                connector: {
                    visible: true,
                    width: 0.5
                },
                position: "columns",
                customizeText: function (arg) {
                    return arg.valueText + " (" + arg.percentText + ")";
                }
            }
        }]
    });





});