$(document).ready(function () {


    $("#agregar_input_faltante").on("click", function () {
        let inputs = `
        <div class="form-group row">
            <div class="col-sm-5">
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-5">
                <input type="text" class="form-control">
            </div>
            <div class="col-sm-2">
                <i class="fas fa-times-circle quitar_faltante"></i>
            </div>
        </div>`;
        $("#seccion_faltantes_input").append(inputs);
    });


    $(document).on("click",".quitar_faltante", function(){
        $(this).parent().parent().remove();
    });


    $(".faltantes_fila").on("click", function () {
        let idfaltante = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Faltantes/get_id/" + idfaltante,
            dataType: "json",
            success: function (response) {
                $("#lista_faltates_get").empty();

                let faltantes_ = response;
                let fila = "";
                $.each(faltantes_, function(i,x){
                    console.log(x);
                    fila = `<tr><td>${x.producto}</td><td>${x.cantidad}</td></tr>`;
                    $("#lista_faltates_get").append(fila);
                });

                $("#elementos_faltantes").show();
                $("#itme_listado_faltantes").hide();
                //$("#editar_proveedor").show();
                $("#guardar_proveedor").hide();

                

            }
        });
    })


    $("#tbl_faltantes tr").on("click", function () {
        $("#tbl_categorias tr").removeClass("highlight");
        $(this).addClass("highlight");
    });




    $('#tbl_faltantes').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false,

        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    $("#nuevo_faltante").on("click", function () {
        $("#elementos_faltantes").hide();
        $("#itme_listado_faltantes").hide();
        $("#formulario_nuevo_faltantes").show();
        $("input").val('');
        $("#editar_faltantes").hide();
        $("#guardar_faltantes").show();
    });

    $("#cancelar_faltante").on("click", function () {
        $("#formulario_nuevo_faltantes").hide();
        $("#elementos_faltantes").hide();
        $("#itme_listado_faltantes").show();
    });

    $("#guardar_faltantes").on("click", function () {
        let faltantes = Array();
        let elementos = new Object();
        let bandera = 0;
        $("#seccion_faltantes_input input").each(function(i, x){
            if(bandera==0){
                elementos.productos = $(this).val();
                bandera++;
            }else if(bandera==1){
                elementos.cantidad = $(this).val();
                faltantes.push(elementos);
                elementos = new Object();
                bandera =0;
            }
        });


        if (elementos.length == 0) {
            $("#alert_danger_faltantes").text("Verificar los elementos a enviar");
            $("#alert_danger_faltantes").show();
            $("#alert_danger_faltantes").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Faltantes/insert",
                data: {
                    faltantes: JSON.stringify(faltantes)
                },
                success: function (response) {
                    alert("Faltantes ingresados correctamente");
                    location.reload();
                }
            });
        }
    });


    $("#editar_proveedor").on("click", function () {
        let idpro = $(this).attr("data-content");
        let proveedor = $("#nombre_pro").val();

        if (proveedor == '') {
            $("#alert_danger").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger").show();
            $("#alert_danger").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Proveedores/update/" + idpro,
                data: {
                    nombre: proveedor
                },
                success: function (response) {
                    alert("Proveedor editado correctamente.");
                    location.reload();
                }
            });
        }
    });




});