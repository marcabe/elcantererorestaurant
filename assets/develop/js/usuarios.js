$(document).ready(function () {

    $(".usuario_fila").on("click", function () {
        let usuario = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Usuarios/get_id/" + usuario,
            dataType: "json",
            success: function (response) {
                let usuario_ = response;
                $("#formulario_nuevo_usuario").show();
                $("#itme_listado_usuarios").hide();
                $("#editar_usuario").show();
                $("#guardar_usuario").hide();
                console.log(usuario_);
                $("#usuario").val(usuario_.usuario);
                $("#tipo").val(usuario_.tipo);
                $("#nombre").val(usuario_.nombre);
                $("#editar_usuario").attr("data-content", usuario);
            }
        });
    })

    $("#tbl_usuarios tr").on("click", function () {
        $("#tbl_usuarios tr").removeClass("highlight");
        $(this).addClass("highlight");
    });


    $(".stat").on("click", function() {
        let idusuario = $(this).attr("data-content");
        let favorito = 0;
        if ($(this).hasClass("fa-user-times")) {
            $(this).removeClass("fa-user-timesar");
            $(this).addClass("fa-user-check");
            favorito = 1;
        } else if ($(this).hasClass("fa-user-check")) {
            $(this).removeClass("fa-user-check");
            $(this).addClass("fa-user-times");
            favorito = 0;
        }
        $.ajax({
            type: "POST",
            url: "../Usuarios/update/"+idusuario,
            data: {
                status: parseInt(favorito)
            }
        });
    });


    $('#tbl_usuarios').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false,

        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    $("#nuevo_usuario").on("click", function () {
        $("#itme_listado_usuarios").hide();
        $("#formulario_nuevo_usuario").show();
        $("input").val('');
        $("select").val(0);

        $("#editar_usuario").hide();
        $("#guardar_usuario").show();
    });

    $("#cancelar_usuario").on("click", function () {
        $("#formulario_nuevo_usuario").hide();
        $("#itme_listado_usuarios").show();
    });

    $("#guardar_usuario").on("click", function () {
        let usuario = $("#usuario").val();
        let nombre = $("#nombre").val();
        let tipo = $("#tipo").val();
        let clave = $("#clave").val();
        let clave_confirm = $("#clave_confirm").val();
        if (usuario == '' || nombre == '' || tipo == 0 || clave == '' || clave_confirm == '') {
            $("#alert_danger_usuario").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_usuario").show();
            $("#alert_danger_usuario").fadeOut(3500);
        } else {
            if (clave != clave_confirm) {
                $("#alert_danger_usuario").text("Las claves no coinciden");
                $("#alert_danger_usuario").show();
                $("#alert_danger_usuario").fadeOut(3500);
            } else {
                $.ajax({
                    type: "POST",
                    url: "../Usuarios/insert",
                    data: {
                        usuario: usuario,
                        nombre: nombre,
                        tipo: tipo,
                        clave: clave,
                        status: 1
                    },
                    success: function (response) {
                        if (response == 0) {
                            $("#alert_danger_usuario").text("El nombre de usuario ya existe, ingresa otro ");
                            $("#alert_danger_usuario").show();
                            $("#alert_danger_usuario").fadeOut(3500);
                        } else {
                            alert("Usuario ingresado correctamente.");
                            location.reload();
                        }

                    }
                });

            }


        }
    });


    $("#editar_usuario").on("click", function () {
        let usuario_update = $(this).attr("data-content");
        let usuario = $("#usuario").val();
        let nombre = $("#nombre").val();
        let tipo = $("#tipo").val();
        let clave = $("#clave").val();
        let clave_confirm = $("#clave_confirm").val();
        if (usuario == '' || nombre == '' || tipo == 0 || clave == '' || clave_confirm == '') {
            $("#alert_danger_usuario").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_usuario").show();
            $("#alert_danger_usuario").fadeOut(3500);
        } else {
            if (clave != clave_confirm) {
                $("#alert_danger_usuario").text("Las claves no coinciden");
                $("#alert_danger_usuario").show();
                $("#alert_danger_usuario").fadeOut(3500);
            } else {
                $.ajax({
                    type: "POST",
                    url: "../Usuarios/actualizar/" + usuario_update,
                    data: {
                        usuario: usuario,
                        nombre: nombre,
                        tipo: tipo,
                        clave: clave
                    },
                    success: function (response) {
                        if (response == 0) {
                            $("#alert_danger_usuario").text("El usuario ingresao ");
                            $("#alert_danger_usuario").show();
                            $("#alert_danger_usuario").fadeOut(3500);
                        } else {
                            alert("Usuario ingresado correctamente.");
                            location.reload();
                        }
                    }
                });
            }

        }
    });




});