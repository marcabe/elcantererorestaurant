$(document).ready(function () {


    $('#tbl_pedidos').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false
    });

    $("#tbl_pedidos tr").on("click", function () {
        $("#tbl_pedidos tr").removeClass("highlight");
        $(this).addClass("highlight");
    });


    $("#nuevo_pedido").on("click", function () {
        $.ajax({
            type: "POST",
            url: "Apertura/verificar_abierta",
            success: function (response) {
                let respuesta = response;
                if (respuesta == 1) {
                    $("#seccion_informacion").empty();
                    let newmesa = `
                        <div class="col-md-12 fondo_card" id="columna_info_mesa">
                            <div class="card">
                                <div class="card-header" id="encabezado_carta_info">
                                    <i class="fas fa-utensils"></i> NUEVO PEDIDO
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Cliente</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control form-control-sm" id="cliente"></select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Mesero</label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control" id="mesero" placeholder="Ingrese el no. del mesero">
                                                </div>
                                            </div>
                                            <div class="form-group row pt-4 text-rigth">
                                                <div class="col-md-12 text-right">
                                                    <button type="button" class="btn btn-success btn-sm" id="abrir_pedido">
                                                        <i class="fas fa-chevron-circle-right"></i> Abrir pedido
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        `;
                    $("#seccion_informacion").append(newmesa);

                    $('#cliente').select2({
                        placeholder: '--- Seleccione un cliente ---',
                        ajax: {
                            url: 'Clientes/get_like',
                            dataType: 'json',
                            delay: 250,
                            processResults: function (response) {
                                return {
                                    results: response
                                };
                            }
                        }
                    });

                    $('#cliente').on('select2:select', function (e) {
                        var data = e.params.data;
                        idclienteglobal = data.id;
                    });
                } else if (respuesta == 0) {
                    alert("Tarea imposible, no hay caja abierta");
                }
            }
        })

    });

    $(".fila_pedidos").on("click", function () {
        let idpedido = $(this).attr("data-content");
        $("#seccion_informacion").empty();
        $.ajax({
            type: "POST",
            url: "Pedidos/get_by_id/" + idpedido,
            data: {},
            dataType: "json",
            success: function (response) {
                let pedido = response;
                let status_pedido = pedido.status;
                let btn_ped = ``;
                let btn_mas_pedido = '';
                if (status_pedido == 1) {
                    btn_ped = `
                    <div class="form-group row pt-4 text-rigth">
                        <div class="col-md-12 text-right">
                            <button type="button" class="btn btn-danger btn-sm" data-content=${idpedido}  id="cerrar_pedido">
                                <i class="fas fa-chevron-circle-right"></i> Cerrar pedido
                            </button>
                        </div>
                    </div>`;
                    btn_mas_pedido = `
                    <div class="col-md-6 text-right">
                        <button id="agregar_mas_pedido" data-content=${pedido.idpedido} class="btn btn-sm btn-danger"><i class="fas fa-plus-square"></i></button>
                    </div>`;
                }
                let lista_comandap = '';
                let total = 0;
                let elim_p = '';
                $.each(pedido.comanda, function (i, p) {
                    let subtotal = accounting.formatMoney(p.subtotal);
                    total += parseFloat(p.subtotal);
                    if (status_pedido == 1) {
                        elim_p = `
                        <span class="price ng-binding "><i class="fas fa-times-circle elim_prod_p" data-content="${p.comanda_pedido}"></i></span>
                    `;
                    } else {
                        elim_p = ``;
                    }
                    lista_comandap += `
                     <li class="addition">
                        <div class="item">
                            <span class="count ng-binding ">${p.cantidad}</span>
                            <strong class="item-label">
                                <span class="ng-binding ">${p.producto.producto}</span>
                            </strong>
                            <span class="price ng-binding ">${subtotal}</span>
                            ${elim_p}
                        </div>
                    </li>
                     `;
                });

                total = accounting.formatMoney(total);

                let info_pedido = `
                <div class="col-md-12 fondo_card" id="columna_info_mesa">
                    <div class="card">
                        <div class="card-header" id="encabezado_carta_info_ocupado">
                            <div class="row">
                                <div class="col-md-6">
                                    <i class="fas fa-utensils"></i> PEDIDO NO. ${pedido.idpedido}
                                </div>
                                ${btn_mas_pedido}
                            </div>
                        </div>
                        <div class="card-body" id="comanda_x_mesa">
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Cliente: ${pedido.cliente.nombre}, ${pedido.hora_inicio}</p>
                                </div>
                                <div class="col-md-12">
                                    <ul id="lista_comandap" class="list show items stated additions">
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <div class="total"><span>Total: </span><strong class="ng-binding ">${total}</strong></div>
                                </div>
                                ${btn_ped}
                            </div>
                        </div>
                    </div>
                </div>
                 `;
                $("#seccion_informacion").append(info_pedido);
                $("#lista_comandap").append(lista_comandap);
            }
        });


    });

    $(document).on('click', '.elim_prod_p', function (e) {
        let apuntador_producto = $(this).parent().parent().parent();
        let comanda_pedido = $(this).attr("data-content");
        let body = `
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group pt-3">
                        <label>Usuario administrador</label>
                        <input type="text" class="form-control" id="usuario" placeholder="Ingrese el usuario del administrador">
                    </div>
                    <div class="form-group pt-3">
                        <label>Ingrese contraseña del administrador</label>
                        <input type="password" class="form-control" id="clave" placeholder="Ingresar contraseña para eliminar el producto">
                    </div>
                    <div class="form-group pt-3">
                        <label>Ingrese el motivo</label>
                        <textarea class="form-control" id="motivo"></textarea>
                    </div>

                </div>
            </div>
        </div>`;
        let confirm_pssw = $.confirm({
            columnClass: 'col-md-6 col-md-offset-3',
            title: `Mensaje del sistema`,
            closeIcon: true,
            content: body,
            row: 'row',
            buttons: {
                Cancelar: {
                    btnClass: 'btn-light',
                    action: function () {
                    }
                },
                Confirmar: {
                    btnClass: 'btn-warning',
                    action: function () {
                        if ($("#usuario").val() == '' || $("#clave").val() == '') {
                            alert("Debe ingresar todos los datos");
                            return 0;
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "ComandasPedidos/delete/" + comanda_pedido,
                                data: {
                                    usuario: $("#usuario").val(),
                                    clave: $("#clave").val(),
                                    motivo: $("#motivo").val()
                                },
                                success: function (response) {
                                    if (response == 1) {
                                        alert("Producto eliminado correctaente");
                                        apuntador_producto.remove();
                                    } else {
                                        alert("Usuario y/o contraseña mo validos");

                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    });


    $(document).on('click', '#agregar_mas_pedido', function (e) {
        let idventa = $(this).attr("data-content");
        window.location = 'pedidos/comanda/' + idventa;
    })



    $(document).on("click", "#cerrar_pedido", function (e) {
        let idpedido = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "Pedidos/get_by_id/" + idpedido,
            data: {},
            dataType: "json",
            success: function (response) {
                let pedido = response;
                let idpedido = pedido.idpedido;
                let lista_comanda = '';
                let total_gral = 0.0;
                let sub_total = 0.0;
                $.each(pedido.comanda, function (i, p) {
                    let subtotal_prod = accounting.formatMoney(p.subtotal);
                    sub_total += parseFloat(p.subtotal);
                    lista_comanda += `
                     <li class="addition">
                        <div class="item">
                            <span class="count ng-binding ">${p.cantidad}</span>
                            <strong class="item-label">
                                <span class="ng-binding ">${p.producto.producto}</span>
                            </strong>
                            <span class="price ng-binding ">${subtotal_prod}</span>
                            <span class="price ng-binding ">${subtotal_prod}</span>
                        </div>
                    </li>
                     `;
                });
                total_gral = accounting.formatMoney(sub_total);

                let li_subtotal = `
                <li class="addition" id="subtotal">
                    <div class="item">
                        <span class="count ng-binding ">Subtotal</span>
                        <strong class="item-label">
                            <span class="ng-binding "></span>
                        </strong>
                    </div>
                </li>`;
                lista_comanda += li_subtotal;

                let total_li = `
                <li class="addition" id="subtotal">
                    <div class="item">
                        <span class="count ng-binding ">TOTAL:</span>
                        <strong class="item-label">
                            <span class="ng-binding "></span>
                        </strong>
                        <span class="price ng-binding " id="total_todo">${total_gral}</span>
                    </div>
                </li>`;


                let body = `
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>ADICIONES</label>
                                </div>
                                <div class="col-md-12">
                                    <ul id="lista_comanda" class="list show items stated additions">
                                    ${lista_comanda}
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <ul id="lista_comanda" class="list show items stated additions">
                                    ${total_li}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>PAGO</label>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <select class="form-control form-control-sm" id="metodo_pago">
                                                <option value=1>Efectivo</option>
                                                <option value=2>T. de Credito</option>
                                                <option value=3>T. de Débito</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control form-control-sm" id="t_gral" value="${total_gral}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row pt-5">
                                <div class="col-md-6">
                                    <label>VUELTO</label>
                                </div>
                                <div class="col-md-6 text-right">
                                    <label id="vuelto">$ 00.00</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;


                let modal_cerrar = $.confirm({
                    columnClass: 'col-md-8 col-md-offset-2',
                    title: `Pedido no. ${pedido.idpedido} - Cliente: ${pedido.cliente.nombre}`,
                    closeIcon: true,
                    content: body,
                    row: 'row',
                    buttons: {
                        Cancelar: {
                            btnClass: 'btn-light',
                            action: function () {

                            }
                        },
                        a: {
                            text: `Cerrar pedido no. ${pedido.idpedido}`,
                            btnClass: 'btn-warning',
                            action: function () {
                                $.ajax({
                                    type: "POST",
                                    url: "Pedidos/termina_pedido/" + idpedido,
                                    data: {
                                        status: 2,
                                        total: accounting.unformat($("#total_todo").text()),
                                        metodo_pago: $("#metodo_pago").val(),
                                        cuanto_pago: accounting.unformat($("#t_gral").val()),
                                        cambio: accounting.unformat($("#vuelto").text())
                                    },
                                    dataType: "json",
                                    success: function (response) {
                                        let pedido = response;
                                        console.log(pedido);
                                        console.log("Imprimirmos Ticket")
                                        let n_impresora = pedido['info']['impresora_corte'];
                                        let conector = new ConectorPlugin();
                                        conector.establecerTamanioFuente(1, 1);
                                        conector.establecerEnfatizado(0);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                                        conector.imagenDesdeUrl("https://elcantererorestaurante.com//assets/develop/images/logo.png");
                                        conector.feed(1);
                                        conector.texto(`EXPEDIDO EL ${pedido['info']['fecha']}\n`);
                                        conector.texto(`GUERRERO 25 CENTRO 47980\n`);
                                        conector.texto(`DEGOLLADO JALISCO\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                                        conector.texto(`   Mesero: ${pedido['mesero'].nombre} ${pedido['mesero'].apellidos}\n`);
                                        conector.texto(`   Nota de pedido:   ${pedido['pedido'].idpedido}\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                                        conector.feed(1);
                                        conector.texto(`   ---------------------------------------`);
                                        conector.feed(1);
                                        let subtotal = 0;
                                        $.each(pedido['comanda'], function (i, c) {
                                            conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                                            conector.texto(`   ${c.producto.producto}\n`);
                                            conector.texto(`   Precio($${c.producto.precio})\n`);
                                            conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                                            conector.texto(`CANT. (${c.cantidad}) $ ${c.subtotal}\n`);
                                            conector.feed(1);
                                            subtotal += parseFloat(c.subtotal);
                                        });
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                                        conector.texto(`==============================\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                                        conector.texto(`   Subtotal:                       $ ${subtotal}\n`);
                                        conector.texto(`   Impuestos:                      $ 00.00\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                                        conector.texto(`==============================\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);

                                        conector.texto(`   Total pagado(${pedido['pedido'].metodo_pago}):      $ ${pedido['pedido'].cuanto_pago}\n`);
                                        conector.texto(`   Cambio:                        $ ${pedido['pedido'].cambio}\n`);

                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                                        conector.feed(1);
                                        conector.texto("***_____GRACIAS POR SU COMPRA_____***");
                                        conector.feed(4);
                                        conector.cortar();
                                        conector.imprimirEn(n_impresora)
                                            .then(respuestaAlImprimir => {
                                                if (respuestaAlImprimir === true) {
                                                    console.log("Impreso correctamente");
                                                } else {
                                                    console.log("Error. La respuesta es: " + respuestaAlImprimir);
                                                }
                                            });

                                        location.reload();

                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    });



    $(document).on("blur", "#t_gral", function () {
        $(this).val(accounting.formatMoney($(this).val()));
        let paga_con = accounting.unformat($(this).val());
        let tgral = accounting.unformat($("#total_todo").text());
        $("#vuelto").text(accounting.formatMoney(paga_con - tgral));
    });



    $(document).on("click", "#abrir_pedido", function (e) {
        let mesero = $("#mesero").val();
        let personas = $("#personas").val();
        $.ajax({
            type: "POST",
            url: "Pedidos/insert",
            data: {
                status: 1,
                idmesero: mesero,
                idcliente: idclienteglobal
            },
            success: function (response) {
                if (response != 0) {
                    let idpedido = response;
                    window.location = "pedidos/comanda/" + idpedido
                } else {
                    alert("Error al abrir el pedido");
                }

            }
        });
    });

    $(document).on("click", "#cerrar_ventana", function () {
        $("#seccion_informacion").empty();
        let body = `
        <span id="msj_seccion_informacion">
            < Selecciona una mesa
        </span>`;
        $("#seccion_informacion").append(body);
    })


});