$(document).ready(function () {



    $(".proveedor_fila").on("click", function(){
        let idproveedor = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Proveedores/get_id/"+idproveedor,
            dataType: "json",
            success: function (response) {
                let proveedor_ = response;

                $("#formulario_nuevo_proveedor").show();
                $("#itme_listado_proveedores").hide();
                $("#editar_proveedor").show();
                $("#guardar_proveedor").hide();

                $("#nombre_pro").val(proveedor_.nombre);
                $("#editar_proveedor").attr("data-content", idproveedor);

            }
        });
    })


    $("#tbl_proveedores tr").on("click", function() {
        $("#tbl_categorias tr").removeClass("highlight");
        $(this).addClass("highlight");
    });




    $('#tbl_proveedores').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false,

        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    $("#nuevo_proveedor").on("click", function() {
        $("#itme_listado_proveedores").hide();
        $("#formulario_nuevo_proveedor").show();
        
        $("input").val('');

        $("#editar_proveedor").hide();
        $("#guardar_proveedor").show();
    });

    $("#cancelar_proveedor").on("click", function() {
        $("#formulario_nuevo_proveedor").hide();
        $("#itme_listado_proveedores").show();
    });

    $("#guardar_proveedor").on("click", function() {
        let proveedor = $("#nombre_pro").val();

        if (proveedor == '') {
            $("#alert_danger_proveedor").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_proveedor").show();
            $("#alert_danger_proveedor").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Proveedores/insert",
                data: {
                    nombre:proveedor,
                    status:1
                },
                success: function(response) {
                    alert("Proveedor ingresado correctamente.");
                    location.reload();
                }
            });
        }
    });


    $("#editar_proveedor").on("click", function() {
        let idpro = $(this).attr("data-content");
        let proveedor = $("#nombre_pro").val();

        if (proveedor == '' ) {
            $("#alert_danger").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger").show();
            $("#alert_danger").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Proveedores/update/"+idpro,
                data: {
                    nombre: proveedor
                },
                success: function(response) {
                    alert("Proveedor editado correctamente.");
                    location.reload();
                }
            });
        }
    });




});