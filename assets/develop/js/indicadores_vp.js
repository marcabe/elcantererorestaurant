$(document).ready(function () {


    $("#filtrar").on("click", function () {
        let mes = $("#mes").val();
        let anio = $("#anio").val();

        $.ajax({
            type: "POST",
            url: "../Rutes/filtro/" + mes + "/" + anio,
            dataType: "json",
            success: function (response) {
                let respuesta = response;
                $("#titulo").text(respuesta['titulo']);
                $("#total_ventas").text(respuesta['total_ventas_mensual']);
                $("#monto_ventas").text(respuesta['ventas_del_mes']);
                $("#total_pedidos").text(respuesta['total_pedidos_mensual']);
                $("#monto_pedidos").text(respuesta['pedidos_del_mes']);

                let array_ventas_vs_pedidos = respuesta['venta_vs_pedidos_pie'];
                let venta_vs_pedidos_barra = respuesta['venta_vs_pedidos_barra'];
                $("#pie2").dxPieChart({
                    palette: "bright",
                    dataSource: array_ventas_vs_pedidos,
                    title: "Ventas vs Pedidos",
                    legend: {
                        orientation: "horizontal",
                        itemTextPosition: "right",
                        horizontalAlignment: "center",
                        verticalAlignment: "bottom",
                        columnCount: 4
                    },
                    "export": {
                        enabled: true
                    },
                    series: [{
                        argumentField: "tipo",
                        valueField: "monto",
                        label: {
                            visible: true,
                            font: {
                                size: 16
                            },
                            connector: {
                                visible: true,
                                width: 0.5
                            },
                            position: "columns",
                            customizeText: function (arg) {
                                return arg.valueText + " (" + arg.percentText + ")";
                            }
                        }
                    }]
                });

                $("#chart").dxChart({
                    dataSource: venta_vs_pedidos_barra,
                    commonSeriesSettings: {
                        argumentField: "day",
                        type: "bar",
                        hoverMode: "allArgumentPoints",
                        selectionMode: "allArgumentPoints",
                        label: {
                            visible: true,
                            format: {
                                type: "fixedPoint",
                                precision: 0
                            }
                        }
                    },
                    series: [{
                        valueField: "ventas",
                        name: "ventas"
                    },
                    {
                        valueField: "pedidos",
                        name: "pedidos"
                    }
                    ],
                    title: "Ventas vs Pedidos diarios",
                    legend: {
                        verticalAlignment: "bottom",
                        horizontalAlignment: "center"
                    },
                    "export": {
                        enabled: true
                    },
                    onPointClick: function (e) {
                        e.target.select();
                    }
                });

            }
        });


    });


    $(document).on("click", "#info_vp tr", function () {
        $("#info_vp tr").removeClass("highlight");
        $(this).addClass("highlight");
    });

    $(document).on("click", "#fila_prueba tr", function () {
        let idventa = $(this).children("td:eq(0)").text();
        $.ajax({
            type: "POST",
            url: "../Ventas/get_info/" + idventa,
            dataType: "json",
            success: function (response) {
                $("#itme_info").hide();
                $("#info").show();
                $("#lista_comanda").empty();
                let venta = response;
                let lista_comanda = '';
                let total = 0;
                let metodo_pago = '';
                let propina = (venta.propina == 1) ? "Si" : "No";
                let monto_propina = accounting.formatMoney(venta.monto_propina);

                $.each(venta.comanda, function (i, c) {
                    let subtotal = accounting.formatMoney(c.subtotal);
                    total += parseFloat(c.subtotal);
                    lista_comanda += `
                    <li class="addition">
                        <div class="item">
                            <span class="count ng-binding ">${c.cantidad}</span>
                            <strong class="item-label">
                                <span class="ng-binding ">${c.producto.producto}</span>
                            </strong>
                            <span class="price ng-binding ">${subtotal}</span>
                        </div>
                    </li>`;
                });
                total = accounting.formatMoney(total);
                if (venta.metodo_pago == 1) {
                    metodo_pago = 'Efectivo';
                } else if (venta.metodo_pago == 2) {
                    metodo_pago = 'T. de Credito'
                } else if (venta.metodo_pago == 3) {
                    metodo_pago = 'T. de Debito';
                }
                if (venta.status == 1) {
                    $("#imprimir_info").hide();
                } else {
                    $("#imprimir_info").show();
                    $("#imprimir_info").attr("data-venta", idventa);
                }
                $("#no_ticket").text(venta.idventa);
                $("#metodo_p").text("Metodo de pago: " + metodo_pago);
                $("#propina").text("Propina: " + propina);
                $("#porcentaje_propina").text(venta.porcentaje_propina + "%");
                $("#monto_propina").text(monto_propina);
                $("#personas").text(venta.personas + " persona(s), ");
                $("#ncliente").text(venta.cliente.nombre + ", ");
                $("#fecha_ini").text(venta.hora_inicio);
                $("#total").text(total);
                $("#lista_comanda").append(lista_comanda);
            }
        })
    });

    $(document).on("click", "#imprimir_info", function (e) {
        let vp = $(this).attr("data-venta");
        window.open('../Ventas/ticket/' + vp, '_blank');
    })

    $("#consultar").on("click", function () {
        let fi = $("#fi").val();
        let ff = $("#ff").val();
        let tipo = $("#tipo").val();
        let opcion = $("#opcion").val();
        let url = '';
        if (fi == '' || ff == '' || tipo == 0) {
            alert("Verificar que todos los datos esten llenos")
        } else {
            if (tipo == "v") {
                url = '../Ventas/filtro';
            } else if (tipo == 'p') {
                url = '../Pedidos/filtro';
            }
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    fi: fi,
                    ff: ff,
                    opcion: opcion
                },
                dataType: "json",
                success: function (response) {
                    $('#info_vp').dataTable({
                        destroy: "true",
                        data: response,
                        columns: [{
                            data: 'ticket'
                        },
                        {
                            data: 'mesero'
                        },
                        {
                            data: 'status'
                        },
                        {
                            data: 'total'
                        }
                        ]
                    });
                }
            });

        }
    });


    $("#abrir_caja").on("click", function () {
        let body = `
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pt-3">
                            <label>Monto de apertura:</label>
                            <input type="text" class="form-control" id="monto">
                        </div>
                    </div>
                </div>
            </div>`;
        let confirm_pssw = $.confirm({
            columnClass: 'col-md-6 col-md-offset-3',
            title: `Incio de turno`,
            closeIcon: true,
            content: body,
            row: 'row',
            buttons: {
                Cancelar: {
                    btnClass: 'btn-light',
                    action: function () { }
                },
                Confirmar: {
                    btnClass: 'btn-warning',
                    action: function () {
                        if ($("#monto").val() == '') {
                            alert("Debe ingresar todos los datos");
                            return 0;
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "../Apertura/nueva_apertura",
                                data: {
                                    monto_apertura: accounting.unformat($(
                                        "#monto").val())
                                },
                                success: function (response) {
                                    alert("Caja principal abierta");
                                    location.reload();
                                }
                            });
                        }
                    }
                }
            }
        });
    });

    $(document).on("blur", "#monto", function () {
        $(this).val(accounting.formatMoney($(this).val()));
    });

    $(document).on("blur", "#monto_p", function () {
        $(this).val(accounting.formatMoney($(this).val()));
    });

    $("#pagar_provedor").on("click", function () {
        let idsuba = $(this).attr("data-content");

        $.ajax({
            type: "POST",
            url: "../Proveedores/get",
            data: {
                status: 1
            },
            dataType: "json",
            success: function (response) {
                let prove = response;
                let option = '<option value=0>Seleccione una opción</option>';
                $.each(prove, function (i, p) {
                    option += `<option value=${p.idpro}>${p.nombre}</option>`;
                });
                let body = `
                    <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group pt-3">
                                        <label>Nombre del provedor</label>
                                        <select class="form-control" id="idproveedor">
                                            ${option}
                                        </select>
                                    </div>
                                    <div class="form-group pt-3">
                                        <label>Monto:</label>
                                        <input type="text" class="form-control" id="monto_p">
                                    </div>
                                    <div class="form-group pt-3">
                                        <label>Descripción de pago</label>
                                        <textarea class="form-control" id="descripcion"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>`;
                let confirm_pssw = $.confirm({
                    columnClass: 'col-md-6 col-md-offset-3',
                    title: `Incio de turno`,
                    closeIcon: true,
                    content: body,
                    row: 'row',
                    buttons: {
                        Cancelar: {
                            btnClass: 'btn-light',
                            action: function () { }
                        },
                        Confirmar: {
                            btnClass: 'btn-warning',
                            action: function () {
                                if ($("#monto_p").val() == '' || $(
                                    "#idproveedor").val() == 0) {
                                    alert("Debe ingresar todos los datos");
                                    return 0;
                                } else {
                                    $.ajax({
                                        type: "POST",
                                        url: "../Regpagop/insert",
                                        data: {
                                            idsuba: idsuba,
                                            idproveedor: $(
                                                "#idproveedor")
                                                .val(),
                                            descripcion: $(
                                                "#descripcion")
                                                .val(),
                                            monto: accounting
                                                .unformat($(
                                                    "#monto_p")
                                                    .val()),
                                        },
                                        success: function (response) {
                                            alert(
                                                "Pago guardado exitosamente"
                                            );
                                            location.reload();
                                        }
                                    });
                                }
                            }
                        }
                    }
                });

            }
        });

    });


    $("#abrir_turno").on("click", function () {
        let idsub = $(this).attr("data-content");

        let body = `
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pt-3">
                            <label>Monto de apertura:</label>
                            <input type="text" class="form-control" id="monto_sub">
                        </div>
                    </div>
                </div>
            </div>`;
        let confirm_pssw = $.confirm({
            columnClass: 'col-md-6 col-md-offset-3',
            title: `Incio de turno`,
            closeIcon: true,
            content: body,
            row: 'row',
            buttons: {
                Cancelar: {
                    btnClass: 'btn-light',
                    action: function () { }
                },
                Confirmar: {
                    btnClass: 'btn-warning',
                    action: function () {
                        if ($("#monto_sub").val() == '') {
                            alert("Debe ingresar todos los datos");
                            return 0;
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "../Subapertura/nueva_apertura",
                                data: {
                                    idsub: idsub,
                                    monto_apertura: accounting.unformat($(
                                        "#monto_sub").val())
                                },
                                dataType: "json",
                                success: function (response) {
                                    alert(
                                        "Turno nuevo correctamente abierto"
                                    );
                                    location.reload();
                                }
                            });
                        }
                    }
                }
            }
        });
    });

    $("#cerrar_caja").on("click", function () {
        let idapertura = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Apertura/cerrar_apertura",
            data: {
                idapertura: idapertura
            },
            dataType: "json",
            success: function (response) {
                if (response == 1) {
                    alert("Caja principal cerrada");
                    location.reload();
                } else if (response == 0) {
                    alert(
                        "No es posible cerrar la caja, aun hay mesas o pedidos pendientes"
                    );
                }
            }
        });
    });


});