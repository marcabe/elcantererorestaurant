$(document).ready(function () {


    $(".mesero_fila").on("click", function () {
       let uuidmesero = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Meseros/get_id/" + uuidmesero,
            dataType: "json",
            success: function (response) {
                let mesero_ = response;
                $("#formulario_nuevo_mesero").show();
                $("#itme_listado_meseros").hide();

                $("#itme_listado_mesero").hide();
                $("#editar_mesero").show();
                $("#guardar_mesero").hide();


                $("#idmesero").val(mesero_.idmesero);
                $("#nombre_mesero").val(mesero_.nombre);
                $("#apellidos_mesero").val(mesero_.apellidos);
                $("#celular_mesero").val(mesero_.celular);
                $("#editar_mesero").attr("data-content", uuidmesero);
            }
        })
    })

    $("#tbl_meseros tr").on("click", function () {
        $("#tbl_meseros tr").removeClass("highlight");
        $(this).addClass("highlight");
    });

    $(".stat_m").on("click", function() {
        let uuidmesero = $(this).attr("data-content");
        let favorito = 0;
        if ($(this).hasClass("fa-user-times")) {
            $(this).removeClass("fa-user-timesar");
            $(this).addClass("fa-user-check");
            favorito = 1;
        } else if ($(this).hasClass("fa-user-check")) {
            $(this).removeClass("fa-user-check");
            $(this).addClass("fa-user-times");
            favorito = 0;
        }
        $.ajax({
            type: "POST",
            url: "../Meseros/update/"+uuidmesero,
            data: {
                status: parseInt(favorito)
            }
        });
    });


    $('#tbl_meseros').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false,

        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    $("#nuevo_mesero").on("click", function () {
        $("#itme_listado_meseros").hide();
        $("#formulario_nuevo_mesero").show();
        $("input").val('');
        $("#editar_mesero").hide();
        $("#guardar_mesero").show();
    });



    $("#cancelar_mesero").on("click", function () {
        $("#formulario_nuevo_mesero").hide();
        $("#itme_listado_meseros").show();
    });

    $("#guardar_mesero").on("click", function () {
        let idmesero = $("#idmesero").val();
        let nombre = $("#nombre_mesero").val();
        let apellidos = $("#apellidos_mesero").val();
        let celular = $("#celular_mesero").val();

        if (idmesero == '' || nombre == '' || apellidos == '' || celular == '') {
            $("#alert_danger_mesero").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_mesero").show();
            $("#alert_danger_mesero").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Meseros/insert",
                data: {
                    idmesero: idmesero,
                    nombre: nombre,
                    apellidos: apellidos,
                    celular: celular,
                    status: 1
                },
                success: function (response) {
                    if(response ==0){
                        alert("Error al cargar el mesero.");
                    }else{
                        alert("Mesero ingresado correctamente.");
                        location.reload();
                    }
                    
                }
            });
        }
    });


    $("#editar_mesero").on("click", function () {
        let uuidmesero = $(this).attr("data-content");
        let idmesero = $("#idmesero").val();
        let nombre = $("#nombre_mesero").val();
        let apellidos = $("#apellidos_mesero").val();
        let celular = $("#celular_mesero").val();

        if (idmesero == '' || nombre == '' || apellidos == '' || celular == '') {
            $("#alert_danger_mesero").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_mesero").show();
            $("#alert_danger_mesero").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Meseros/actualizar/" + uuidmesero,
                data: {
                    idmesero: idmesero,
                    nombre: nombre,
                    apellidos: apellidos,
                    celular: celular,
                },
                success: function (response) {
                    if (response == 0) {
                        $("#alert_danger_usuario").text("El uuid del mesero ya existe, ingrese otro ");
                        $("#alert_danger_usuario").show();
                        $("#alert_danger_usuario").fadeOut(3500);
                    } else {
                        alert("Mesero editador correctamente.");
                        location.reload();
                    }
                }
            });
        }
    });




});