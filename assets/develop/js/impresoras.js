const $estado = document.querySelector("#estado"),
    $listaDeImpresoras = document.querySelector("#listaDeImpresoras"),
    $btnLimpiarLog = document.querySelector("#btnLimpiarLog"),
    $btnImprimir = document.querySelector("#btnImprimir");

let obtenerListaDeImpresoras = () => {
    loguear("Cargando lista...");
    ConectorPlugin.obtenerImpresoras()
        .then(listaDeImpresoras => {
            loguear("Lista cargada");
            const option = document.createElement('option');
            option.value = option.text = "----------";
            $listaDeImpresoras.appendChild(option);
            listaDeImpresoras.forEach(nombreImpresora => {
                const option = document.createElement('option');
                option.value = option.text = nombreImpresora;
                $listaDeImpresoras.appendChild(option);
            })
        })
        .catch(() => {
            loguear("Error obteniendo impresoras. Asegúrese de que el plugin se está ejecutando");
        });
}

const loguear = texto => $estado.textContent += texto + "\n";
const limpiarLog = () => $estado.textContent = "";

$btnLimpiarLog.addEventListener("click", limpiarLog);


$(".asignar_impresora").on("click", function () {
    let seccion_seleccionada = $(this).attr("data-imp");
    $("#itme_listado_impresoras").hide();
    $("#formulario_asignar_impresora").show();
    $("#listaDeImpresoras").empty();
    obtenerListaDeImpresoras();
    $("#asignar_impresora").attr("data-impresora", seccion_seleccionada);
});

$("#cancelar_asignacion").on("click", function () {
    $("#formulario_asignar_impresora").hide();
    $("#itme_listado_impresoras").show();
});

$(document).on("click", "#probar_impresora", function () {
    let nombreImpresora = $listaDeImpresoras.value;
    let conector = new ConectorPlugin();
    conector.establecerTamanioFuente(1, 1);
    conector.establecerEnfatizado(0);
    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
    conector.feed(1);
    conector.texto("Conexion con impresora satisfactoria\n");
    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
    conector.texto("***__________________***");
    conector.feed(4);
    conector.cortar();
    conector.imprimirEn(nombreImpresora)
        .then(respuestaAlImprimir => {
            if (respuestaAlImprimir === true) {
                //loguear("Impreso correctamente");
            } else {
                loguear("Error. La respuesta es: " + respuestaAlImprimir);
            }
        });
});


$(document).on("click", "#asignar_impresora", function () {
    let impresora_asignar = $(this).attr("data-impresora");
    let impresora_select = $listaDeImpresoras.value;
    $.ajax({
        type: "POST",
        url: "../Rutes/insert_impresora",
        data: {
            impresora: impresora_select,
            impresora_asignar: impresora_asignar
        },
        success: function (response) {
            let respiesta = response;
            location.reload();
        }
    });

});
