$(document).ready(function () {

    $("#ingresar").on("click", function () {
        var avanza = 1;
        if ($("#usuario").val() == '') {
            $("#error_usuario").text("Debe ingresar su usuario");
            $("#error_usuario").show();
            avanza = 0;
        }
        if ($("#clave").val() == '') {
            $("#error_clave").text("Debe ingresar su clave");
            $("#error_clave").show();
            avanza = 0;
        }
        if (avanza == 1) {
            $.ajax({
                url: 'Usuarios/valida_login',
                data: {
                    usuario: $("#usuario").val(),
                    clave: $("#clave").val()
                },
                type: 'POST',
                success: function (response) {
                    if (response == 0) {
                        /*Usuario no valido*/
                        $("#error_usuario").text("Usuario no válido");
                        $("#error_usuario").show();
                    } else if (response == 1) {
                        /*Contraseña no valida*/
                        $("#error_clave").text("Contraseña no válida");
                        $("#error_clave").show();
                    } else if (response == 2) {
                        window.location = "lista_clientes";
                    }
                }
            });
        }
    });

    $(document).keypress(function (e) {
        if (e.which == 13) {

            var avanza = 1;
            if ($("#usuario").val() == '') {
                $("#error_usuario").text("Debe ingresar su usuario");
                $("#error_usuario").show();
                avanza = 0;
            }
            if ($("#clave").val() == '') {
                $("#error_clave").text("Debe ingresar su clave");
                $("#error_clave").show();
                avanza = 0;
            }
            if (avanza == 1) {
                $.ajax({
                    url: 'Usuarios/valida_login',
                    data: {
                        usuario: $("#usuario").val(),
                        clave: $("#clave").val()
                    },
                    type: 'POST',
                    success: function (response) {
                        if (response == 0) {
                            /*Usuario no valido*/
                            $("#error_usuario").text("Usuario no válido");
                            $("#error_usuario").show();
                        } else if (response == 1) {
                            /*Contraseña no valida*/
                            $("#error_clave").text("Contraseña no válida");
                            $("#error_clave").show();
                        } else if (response == 2) {
                            window.location = "lista_clientes";
                        }
                    }
                });
            }
        }


    });


});