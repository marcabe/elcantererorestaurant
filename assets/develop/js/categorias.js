$(document).ready(function () {



    $(".categoria_fila").on("click", function(){
        let idcategoria = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Categorias/get_id/"+idcategoria,
            dataType: "json",
            success: function (response) {
                let categoria_ = response;

                $("#formulario_nueva_categoria").show();
                $("#itme_listado_categorias").hide();
                $("#editar_categoria").show();
                $("#guardar_categoria").hide();

                $("#categoria").val(categoria_.categoria);
                $("#lugar").val(categoria_.lugar);
                $("#editar_categoria").attr("data-content", idcategoria);

            }
        });
    })

    $("#tbl_categorias tr").on("click", function() {
        $("#tbl_categorias tr").removeClass("highlight");
        $(this).addClass("highlight");
    });




    $('#tbl_categorias').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false,

        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    $("#nueva_categoria").on("click", function() {
        $("#itme_listado_categorias").hide();
        $("#formulario_nueva_categoria").show();
        
        $("input").val('');
        $("select").val(0);
        
        $("#editar_categoria").hide();
        $("#guardar_categoria").show();
    });

    $("#cancelar_categoria").on("click", function() {
        $("#formulario_nueva_categoria").hide();
        $("#itme_listado_categorias").show();
    });

    $("#guardar_categoria").on("click", function() {
        let categoria = $("#categoria").val();
        let lugar = $("#lugar").val();

        if (categoria == '' || lugar ==  0) {
            $("#alert_danger_categoria").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_categoria").show();
            $("#alert_danger_categoria").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Categorias/insert",
                data: {
                    categoria: categoria,
                    lugar: lugar,
                    status:1
                },
                success: function(response) {
                    alert("Categoria ingresado correctamente.");
                    location.reload();
                }
            });
        }
    });


    $("#editar_categoria").on("click", function() {
        let idcategoria = $(this).attr("data-content");
        let categoria = $("#categoria").val();
        let lugar = $("#lugar").val();

        if (producto == '' || precio == '' || idcategoria == 0) {
            $("#alert_danger").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger").show();
            $("#alert_danger").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Categorias/update/"+idcategoria,
                data: {
                    categoria: categoria,
                    lugar: lugar
                },
                success: function(response) {
                    alert("Categoria editada correctamente.");
                    location.reload();
                }
            });
        }
    });




});