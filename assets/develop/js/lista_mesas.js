

$(document).ready(function () {

    $(".mesas_listas").on("click", function () {
        let idmesa = $(this).attr("data-content");
        let nomesa = $(this).attr("alt");
        let status = $(this).attr("name");
        $("#seccion_informacion").empty();

        $.ajax({
            type: "POST",
            url: "Apertura/verificar_abierta",
            success: function (response) {
                let respuesta = response;
                if (respuesta == 1) {
                    if (status == 1) {
                        let newmesa = `
                        <div class="col-md-12 fondo_card" id="columna_info_mesa">
                            <div class="card">
                                <div class="card-header" id="encabezado_carta_info">
                                    <i class="fas fa-utensils"></i> MESA NO. ${nomesa}
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Personas</label>
                                                <div class="col-sm-8">
                                                <input type="number" class="form-control" id="personas" min=1>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Cliente</label>
                                                <div class="col-sm-8">
                                                    <select class="form-control form-control-sm" id="cliente"></select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputPassword" class="col-sm-4 col-form-label">Mesero</label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control" id="mesero" placeholder="Ingrese el no. del mesero">
                                                </div>
                                            </div>
                                            <div class="form-group row pt-4 text-rigth">
                                                <div class="col-md-12 text-right">
                                                    <button type="button" class="btn btn-success btn-sm" data-content=${idmesa} id="abrir_mesa">
                                                        <i class="fas fa-chevron-circle-right"></i> Abrir mesa
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         `;
                        $("#seccion_informacion").append(newmesa);

                        $('#cliente').select2({
                            placeholder: '--- Seleccione un cliente ---',
                            ajax: {
                                url: 'Clientes/get_like',
                                dataType: 'json',
                                delay: 250,
                                processResults: function (response) {
                                    return {
                                        results: response
                                    };
                                }
                            }
                        });

                        $('#cliente').on('select2:select', function (e) {
                            var data = e.params.data;
                            idclienteglobal = data.id;
                        });
                    } else if (status == 2) {
                        $.ajax({
                            type: "POST",
                            url: "Ventas/get_where",
                            data: {
                                idmesa: idmesa,
                                status: 1
                            },
                            dataType: "json",
                            success: function (response) {
                                let venta = response;
                                console.log(venta);
                                let lista_comanda = '';
                                let total = 0;
                                $.each(venta.comanda, function (i, c) {
                                    let subtotal = accounting.formatMoney(c.subtotal);
                                    total += parseFloat(c.subtotal);
                                    lista_comanda += `
                                     <li class="addition">
                                        <div class="item">
                                            <span class="count ng-binding ">${c.cantidad}</span>
                                            <strong class="item-label">
                                                <span class="ng-binding ">${c.producto.producto}</span>
                                            </strong>
                                            <span class="price ng-binding ">${subtotal}</span>
                                            <span class="price ng-binding "><i class="fas fa-times-circle elim_prod" data-content="${c.comanda}"></i></span>
                                        </div>
                                    </li>
                                     `;
                                });

                                total = accounting.formatMoney(total);

                                let newmesa = `
                                <div class="col-md-12 fondo_card" id="columna_info_mesa">
                                    <div class="card">
                                        <div class="card-header" id="encabezado_carta_info_ocupado">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <i class="fas fa-utensils"></i> MESA NO. ${nomesa}
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <button id="agregar_mas" data-content=${venta.idventa} class="btn btn-sm btn-danger"><i class="fas fa-plus-square"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body" id="comanda_x_mesa">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p><b>${venta.personas} persona(s)</b> ${venta.cliente.nombre}, ${venta.hora_inicio}</p>
                                                </div>
                                                <div class="col-md-12">
                                                    <ul id="lista_comanda" class="list show items stated additions">
                                                    </ul>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="total"><span>Total: </span><strong class="ng-binding ">${total}</strong></div>
                                                </div>
                                                <div class="form-group row pt-4 text-rigth">
                                                    <div class="col-md-12 text-right">
                                                        <button type="button" class="btn btn-danger btn-sm" data-content=${venta.idventa}  id="cerrar_mesa">
                                                            <i class="fas fa-chevron-circle-right"></i> Cerrar mesa
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 `;
                                $("#seccion_informacion").append(newmesa);
                                $("#lista_comanda").append(lista_comanda);
                            }
                        });
                    }
                } else if (respuesta == 0) {
                    alert("Tarea imposible, no hay caja abierta");
                }
            }
        });


    });

    $(document).on('click', '.elim_prod', function (e) {
        let apuntador_producto = $(this).parent().parent().parent();
        let comanda = $(this).attr("data-content");
        let body = `
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="form-group pt-3">
                        <label>Usuario administrador</label>
                        <input type="text" class="form-control" id="usuario" placeholder="Ingrese el usuario del administrador">
                    </div>
                    <div class="form-group pt-3">
                        <label>Ingrese contraseña del administrador</label>
                        <input type="password" class="form-control" id="clave" placeholder="Ingresar contraseña para eliminar el producto">
                    </div>
                    <div class="form-group pt-3">
                        <label>Ingrese el motivo</label>
                        <textarea class="form-control" id="motivo"></textarea>
                    </div>

                </div>
            </div>
        </div>`;
        let confirm_pssw = $.confirm({
            columnClass: 'col-md-6 col-md-offset-3',
            title: `Mensaje del sistema`,
            closeIcon: true,
            content: body,
            row: 'row',
            buttons: {
                Cancelar: {
                    btnClass: 'btn-light',
                    action: function () {
                    }
                },
                Confirmar: {
                    btnClass: 'btn-warning',
                    action: function () {
                        if ($("#usuario").val() == '' || $("#clave").val() == '') {
                            alert("Debe ingresar todos los datos");
                            return 0;
                        } else {
                            $.ajax({
                                type: "POST",
                                url: "Comandas/delete/" + comanda,
                                data: {
                                    usuario: $("#usuario").val(),
                                    clave: $("#clave").val(),
                                    motivo: $("#motivo").val()
                                },
                                success: function (response) {
                                    if (response == 1) {
                                        alert("Producto eliminado correctaente");
                                        apuntador_producto.remove();
                                    } else {
                                        alert("Usuario y/o contraseña mo validos");

                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
    });


    $(document).on('click', '#agregar_mas', function (e) {
        let idventa = $(this).attr("data-content");
        window.location = 'ventas/comanda/' + idventa;
    })

    $(document).on("click", "#cerrar_mesa", function (e) {
        let idventa = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "Ventas/get_where",
            data: {
                idventa: idventa
            },
            dataType: "json",
            success: function (response) {
                let venta = response;
                let nomesa = venta.idmesa;
                let idVenta = venta.idventa;
                let lista_comanda = '';
                let sub_total = 0;
                let propina = 0.0;
                let total_gral = 0.0;
                $.each(venta.comanda, function (i, c) {
                    let subtotal_prod = accounting.formatMoney(c.subtotal);
                    sub_total += parseFloat(c.subtotal);
                    lista_comanda += `
                     <li class="addition">
                        <div class="item">
                            <span class="count ng-binding ">${c.cantidad}</span>
                            <strong class="item-label">
                                <span class="ng-binding ">${c.producto.producto}</span>
                            </strong>
                            <span class="price ng-binding ">${subtotal_prod}</span>
                            <span class="price ng-binding ">${subtotal_prod}</span>
                        </div>
                    </li>
                     `;
                });
                propina = (parseFloat(sub_total)) * (0.1);
                total_gral = parseFloat(propina) + parseFloat(sub_total);
                sub_total = accounting.formatMoney(sub_total);
                propina = accounting.formatMoney(propina);
                total_gral = accounting.formatMoney(total_gral);

                let li_subtotal = `
                <li class="addition" id="subtotal">
                    <div class="item">
                        <span class="count ng-binding ">Subtotal</span>
                        <strong class="item-label">
                            <span class="ng-binding "></span>
                        </strong>
                        <span class="price ng-binding " id="subtotal_info">${sub_total}</span>
                    </div>
                </li>
                <li class="addition" id="subtotal">
                    <div class="item">
                        <span class="count ng-binding ">Propina: <input type="number" class="col-md-3" id="propina_add" value=10> %</span>
                        <strong class="item-label">
                            <span class="ng-binding "></span>
                        </strong>
                        <span class="price ng-binding" id="propina_info">${propina}</span>
                    </div>
                </li>`;
                lista_comanda += li_subtotal;

                let total_li = `
                <li class="addition" id="subtotal">
                    <div class="item">
                        <span class="count ng-binding ">TOTAL:</span>
                        <strong class="item-label">
                            <span class="ng-binding "></span>
                        </strong>
                        <span class="price ng-binding " id="total_todo">${total_gral}</span>
                    </div>
                </li>`;


                let body = `
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>ADICIONES</label>
                                </div>
                                <div class="col-md-12">
                                    <ul id="lista_comanda" class="list show items stated additions">
                                    ${lista_comanda}
                                    </ul>
                                </div>
                                <div class="col-md-12">
                                    <ul id="lista_comanda" class="list show items stated additions">
                                    ${total_li}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>PAGO</label>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-sm-6">
                                            <select class="form-control form-control-sm" id="metodo_pago">
                                                <option value=1>Efectivo</option>
                                                <option value=2>T. de Credito</option>
                                                <option value=3>T. de Débito</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control form-control-sm" id="t_gral" value="${total_gral}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row pt-5">
                                <div class="col-md-6">
                                    <label>VUELTO</label>
                                </div>
                                <div class="col-md-6 text-right">
                                    <label id="vuelto">$ 00.00</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `;


                let modal_cerrar = $.confirm({
                    columnClass: 'col-md-8 col-md-offset-2',
                    title: `Cerrar mesa no. ${venta.mesas.nomesa} - Cliente: ${venta.cliente.nombre}`,
                    closeIcon: true,
                    content: body,
                    row: 'row',
                    buttons: {
                        Cancelar: {
                            btnClass: 'btn-light',
                            action: function () {

                            }
                        },
                        a: {
                            text: `Cerrar mesa no. ${venta.mesas.nomesa}`,
                            btnClass: 'btn-warning',
                            action: function () {
                                $.ajax({
                                    type: "POST",
                                    url: "Ventas/termina_mesa/" + idVenta,
                                    data: {
                                        status: 2,
                                        total: accounting.unformat($("#total_todo").text()),
                                        metodo_pago: $("#metodo_pago").val(),
                                        propina: 1,
                                        monto_propina: accounting.unformat($("#propina_info").text()),
                                        porcentaje_propina: $("#propina_add").val(),
                                        cuanto_pago: accounting.unformat($("#t_gral").val()),
                                        cambio: accounting.unformat($("#vuelto").text()),
                                        idmesa: venta.mesas.idmesa
                                    },
                                    dataType: "json",
                                    success: function (response) {
                                        let venta = response;
                                        console.log(venta);
                                        console.log("Imprimirmos Ticket")
                                        let n_impresora = venta['info']['impresora_corte'];
                                        let conector = new ConectorPlugin();
                                        conector.establecerTamanioFuente(1, 1);
                                        conector.establecerEnfatizado(0);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                                        conector.imagenDesdeUrl("https://elcantererorestaurante.com//assets/develop/images/logo.png");
                                        conector.feed(1);
                                        conector.texto(`EXPEDIDO EL ${venta['info']['fecha']}\n`);
                                        conector.texto(`GERRERO 25 CENTRO 47980\n`);
                                        conector.texto(`DEGOLLADO JALISCO\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                                        conector.texto(`   Mesero: ${venta['mesero'].nombre} ${venta['mesero'].apellidos}\n`);
                                        conector.texto(`   Nota de venta: ${venta['venta'].idventa}\n`);
                                        conector.texto(`   No Mesa(${venta['mesa'].nomesa}) ${venta['mesa'].croquis}\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                                        conector.feed(1);
                                        conector.texto(`------------------------------------------`);
                                        conector.feed(1);
                                        let subtotal = 0;
                                        $.each(venta['comanda'], function (i, c) {
                                            conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                                            conector.texto(`   ${c.producto.producto} Precio($${c.producto.precio}) \n`);
                                            conector.texto(`   CANT. (${c.cantidad})                    $ ${c.subtotal}\n`);
                                            conector.feed(1);
                                            subtotal += parseFloat(c.subtotal);
                                        });
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                                        conector.texto(`==============================\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                                        conector.texto(`   Subtotal:    $ ${subtotal}\n`);
                                        conector.texto(`   Impuestos:    $ 00.00\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                                        conector.texto(`==============================\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                                        conector.texto(`   Total pagado(${venta['venta'].metodo_pago}):    $ ${venta['venta'].cuanto_pago}\n`);
                                        conector.texto(`   Cambio:    $ ${venta['venta'].cambio}\n`);
                                        conector.texto(`   Propina sugerida:    ${venta['venta'].porcentaje_propina} % $  ${venta['venta'].monto_propina}\n`);
                                        conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                                        conector.feed(1);
                                        conector.texto("***_____GRACIAS POR SU COMPRA_____***");
                                        conector.feed(4);
                                        conector.cortar();
                                        conector.imprimirEn(n_impresora)
                                            .then(respuestaAlImprimir => {
                                                if (respuestaAlImprimir === true) {
                                                    console.log("Impreso correctamente");
                                                } else {
                                                    console.log("Error. La respuesta es: " + respuestaAlImprimir);
                                                }
                                            });


                                        /*window.open("Ventas/ticket/"+idVenta, '_blank');*/
                                        location.reload();
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    });


    $(document).on("blur", "#propina_add", function () {
        let propina = parseInt($(this).val()) / 100;
        let subtotal = accounting.unformat($("#subtotal_info").text());
        let porpina_total = propina * subtotal;
        $("#propina_info").text(accounting.formatMoney(porpina_total));
        let total_todo = parseFloat(porpina_total) + parseFloat(subtotal);
        $("#total_todo").text(accounting.formatMoney(total_todo));
        $("#t_gral").val(accounting.formatMoney(total_todo));
    });


    $(document).on("blur", "#t_gral", function () {
        $(this).val(accounting.formatMoney($(this).val()));
        let paga_con = accounting.unformat($(this).val());
        let tgral = accounting.unformat($("#total_todo").text());
        $("#vuelto").text(accounting.formatMoney(paga_con - tgral));
    });

    $(document).on("click", "#abrir_mesa", function (e) {
        let idmesa = $(this).attr("data-content");
        let mesero = $("#mesero").val();
        let personas = $("#personas").val();
        $.ajax({
            type: "POST",
            url: "Ventas/insert",
            data: {
                status: 1,
                idmesa: idmesa,
                idmesero: mesero,
                personas: personas,
                idcliente: idclienteglobal
            },
            success: function (response) {
                if (response != 0) {
                    let idventa = response;
                    window.location = "ventas/comanda/" + idventa
                } else {
                    alert("Error al dar de abrir a mesa");
                }

            }
        });
    });


});