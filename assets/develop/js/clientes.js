$(document).ready(function () {


    $(".clientes_fila").on("click", function () {
        let idcliente = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Clientes/get_id/" + idcliente,
            dataType: "json",
            success: function (response) {
                let cliente_ = response;
                $("#formulario_nuevo_cliente").show();
                $("#itme_listado_clientes").hide();

                $("#editar_cliente").show();
                $("#guardar_cliente").hide();


                $("#nombre").val(cliente_.nombre);
                $("#editar_cliente").attr("data-content", idcliente);
            }
        })
    });

    $("#tbl_clientes tr").on("click", function () {
        $("#tbl_clientes tr").removeClass("highlight");
        $(this).addClass("highlight");
    });

    $(".fa-id-card").on("click", function () {
        let idlciente = $(this).attr("data-content");
        let favorito = 0;
        if ($(this).hasClass("status_disable")) {
            $(this).removeClass("status_disable");
            $(this).addClass("status_enable");
            favorito = 1;
        } else if ($(this).hasClass("status_enable")) {
            $(this).removeClass("status_enable");
            $(this).addClass("status_disable");
            favorito = 0;
        }
        $.ajax({
            type: "POST",
            url: "../Usuarios/update/" + idlciente,
            data: {
                status: parseInt(favorito)
            }
        });
    });


    $('#tbl_clientes').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false,

        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    $("#nuevo_cliente").on("click", function () {
        $("#itme_listado_clientes").hide();
        $("#formulario_nuevo_cliente").show();
        $("input").val('');
        $("#editar_cliente").hide();
        $("#guardar_cliente").show();
    });



    $("#cancelar_cliente").on("click", function () {
        $("#formulario_nuevo_cliente").hide();
        $("#itme_listado_clientes").show();
    });

    $("#guardar_cliente").on("click", function () {
        let nombre = $("#nombre").val();

        if (nombre == '') {
            $("#alert_danger_mesero").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_mesero").show();
            $("#alert_danger_mesero").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Clientes/insert",
                data: {
                    nombre: nombre,
                    status: 1
                },
                success: function (response) {
                    alert("Cliente ingresado correctamente.");
                    location.reload();
                }
            });
        }
    });


    $("#editar_cliente").on("click", function () {
        let idcliente = $(this).attr("data-content");
        let nombre = $("#nombre").val();
        if (nombre == '') {
            $("#alert_danger_mesero").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger_mesero").show();
            $("#alert_danger_mesero").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Clientes/update/" + idcliente,
                data: {
                    nombre: nombre
                },
                success: function (response) {
                    alert("Cliente editado correctamente.");
                    location.reload();
                }
            });
        }
    });




});