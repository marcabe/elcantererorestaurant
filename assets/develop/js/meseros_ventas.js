$("#mostrar_meseros_tabla").on("click", function () {
    let fecha_mesero = $("#fecha_mesero").val();
    let mesero_uuid = $("#mesero_uuid").val();

    if (fecha_mesero == '' || mesero_uuid == '') {
        alert("Debe ingresar la fecha y mesero para generar el reporte");
        return 0;
    }

    $.ajax({
        type: "POST",
        url: "Meseros/reporte_ventas_pedidos/",
        data: {
            fecha_mesero: fecha_mesero,
            mesero_uuid: mesero_uuid
        },
        dataType: "json",
        success: function (response) {
            let reporte = response;
            let cuerpo_ventas;
            let cuerpo_pedidos;

            let total_venta_m = 0;
            let total_pedido_m = 0;
            $("#ventas_meseros_lista").empty();
            $("#pedidos_meseros_lista").empty();
            $.each(reporte['ventas'], function (i, ventas) {
                cuerpo_ventas += `<tr><td>${ventas.idventa}</td><td>$ ${ventas.total}</td></tr>`
                total_venta_m = parseFloat(ventas.total) + parseFloat(total_venta_m);
                console.log(total_venta_m, parseFloat(ventas.total), parseFloat(total_pedido_m));
                console.log("La venta va " + total_venta_m);
            });
            $.each(reporte['pedidos'], function (i, pedidos) {
                cuerpo_pedidos += `<tr><td>${pedidos.idpedido}</td><td>$ ${pedidos.total}</td></tr>`
                total_pedido_m = parseFloat(pedidos.total) + parseFloat(total_pedido_m);
            });

            total_venta_m = accounting.formatMoney(total_venta_m);
            total_pedido_m = accounting.formatMoney(total_pedido_m);

            $("#info_meseros").css({ "display": "contents" });

            $("#total_venta_m").text(`TOTAL: ${total_venta_m}`);
            $("#total_pedido_m").text(`TOTAL: ${total_pedido_m}`);

            $("#ventas_meseros_lista").append(cuerpo_ventas);
            $("#pedidos_meseros_lista").append(cuerpo_pedidos);

        }
    });


    $("#download_meseros").on("click", function () {
        let fecha_mesero = $("#fecha_mesero").val();
        let mesero_uuid = $("#mesero_uuid").val();
        $.ajax({
            type: "POST",
            url: `meseros/ticket_venta_pedido/${mesero_uuid}/${fecha_mesero}`,
            dataType: "json",
            success: function (response) {
                let reporte = response
                console.log(reporte);
                console.log("Imprimirmos Ticket")
                let n_impresora = reporte['info']['impresora_corte'];
                let conector = new ConectorPlugin();
                conector.establecerTamanioFuente(1, 1);
                conector.establecerEnfatizado(0);
                conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                conector.feed(1);
                conector.texto("REPORTE DE VENTAS Y PEDIDOS\n");
                conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                conector.feed(1);
                conector.texto(`   Mesero:${reporte['mesero'].nombre} ${reporte['mesero'].apellidos}\n`);
                conector.feed(1);
                conector.texto(`   FECHA/HORA:  ${reporte['info']['fecha']}\n`);

                if (reporte['ventas'].length > 0) {
                    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                    conector.feed(1);
                    conector.texto("=====LISTA DE VENTAS=====\n");
                    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                    conector.feed(1);
                    let suma_ventas = 0;
                    let suma_propina = 0;
                    $.each(reporte['ventas'], function (i, v) {
                        /*conector.texto(`No. Mesa. ${v.mesa_info.nomesa}(${v.mesa_info.croquis})\n`);
                        conector.texto(`Hora entrada. ${v.hora_inicio}\n`);
                        conector.texto(`Hora salida. ${v.fecha_final} ${v.hora_final}\n`);
                        conector.texto(`Propina. ${v.monto_propina} (${v.porcentaje_propina}%)\n`);
                        conector.texto(`Total. $ ${v.total}\n`);
                        conector.feed(2);*/
                        suma_ventas += parseFloat(v.total);
                        suma_propina += parseFloat(v.monto_propina);
                    });
                    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                    conector.texto(`   Total de Ventas. $ ${suma_ventas}\n`);
                    conector.texto(`   Total de Propina. $ ${suma_propina}\n`);

                }

                if (reporte['pedidos'].length > 0) {
                    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                    conector.feed(1);
                    conector.texto("=====LISTA DE PEDIDOS=====\n");
                    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                    conector.feed(1);
                    let suma_pedidos = 0;

                    $.each(reporte['pedidos'], function (i, v) {
                        /*conector.texto(`Hora entrada. ${v.hora_inicio}\n`);
                        conector.texto(`Hora salida. ${v.fecha_final} ${v.hora_final}\n`);
                        conector.texto(`Total. $ ${v.total}\n`);
                        conector.feed(2);*/
                        suma_pedidos += parseFloat(v.total);

                    });
                    conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                    conector.texto(`   Total. $ ${suma_pedidos}\n`);
                }

                conector.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                conector.texto("***__________________***");
                conector.feed(4);
                conector.cortar();

                conector.imprimirEn(n_impresora)
                    .then(respuestaAlImprimir => {
                        if (respuestaAlImprimir === true) {
                            console.log("Impreso correctamente");
                        } else {
                            console.log("Error. La respuesta es: " + respuestaAlImprimir);
                        }
                    });
            }
        });
        //window.open(`meseros/reporte_vp/${mesero_uuid}/${fecha_mesero}`, "Reporte")
    })
});