$(document).ready(function () {

    $('#producto').select2({
        placeholder: '--- Seleccione un producto ---',
        ajax: {
            url: '../../Productos/get_like',
            dataType: 'json',
            delay: 250,
            processResults: function (response) {
                return {
                    results: response
                };
            }
        }
    });


    $('#producto').on('select2:select', function (e) {
        let idProducto = e.params.data.id;
        $.ajax({
            type: "POST",
            url: "../../Productos/get_id/" + idProducto,
            data: {},
            dataType: "json",
            success: function (response) {
                let producto = response;
                let punitario = accounting.formatMoney(producto.precio);
                let subtotal = punitario;
                let fila = `
                <tr class="fila">
                    <td><input type="number" min=1 value=1 style="width:50px;" class="cantidad" data-content=${producto.precio}></td>
                    <td data-content=${producto.idproducto} >${producto.producto}</td>
                    <td>${punitario}</td>
                    <td><textarea></textarea></td>	
                    <td>${subtotal} <i class="fas fa-times-circle quitar" style="color:red";></i></td>
                </tr>
                `;
                $("#lista_comanda").append(fila);

                let total = 0.0;
                $("#lista_comanda tr").each(function (index, element) {
                    let subtotal = $(this).find("td:eq(4)").text();
                    total += parseFloat(accounting.unformat(subtotal));
                    $("#total").text(accounting.formatMoney(total));
                });


            }
        });
    });
    $(document).on("click", ".quitar", function (e) {
        $(this).parent().parent().remove();
        let total = 0.0;
        $("#lista_comanda tr").each(function (index, element) {
            let subtotal = $(this).find("td:eq(4)").text();
            total += parseFloat(accounting.unformat(subtotal));
            $("#total").text(accounting.formatMoney(total));
        });
    });


    $(document).on("click", ".cantidad", function (e) {
        let cantidad = $(this).val();
        let punitario = $(this).attr("data-content");
        let subtotal = accounting.formatMoney(parseFloat(cantidad) * parseFloat(punitario));
        $(this).parent().parent().find("td:eq(4)").html(subtotal +
            ' <i class="fas fa-times-circle quitar" style="color:red";>');
        let total = 0.0;
        $("#lista_comanda tr").each(function (index, element) {
            let subtotal = $(this).find("td:eq(4)").text();
            total += parseFloat(accounting.unformat(subtotal));
            $("#total").text(accounting.formatMoney(total));
        });
    });

    $(document).on("blur", ".cantidad", function (e) {
        let cantidad = $(this).val();
        let punitario = $(this).attr("data-content");
        let subtotal = accounting.formatMoney(parseFloat(cantidad) * parseFloat(punitario));
        $(this).parent().parent().find("td:eq(4)").html(subtotal +
            ' <i class="fas fa-times-circle quitar" style="color:red";>');
        let total = 0.0;
        $("#lista_comanda tr").each(function (index, element) {
            let subtotal = $(this).find("td:eq(4)").text();
            total += parseFloat(accounting.unformat(subtotal));
            $("#total").text(accounting.formatMoney(total));
        });
    });


    $("#mandar_pedido").on("click", function () {
        let lista_comandas = [];
        let number = 1;
        $("#lista_comanda tr").each(function (index, element) {
            $(this).find("td").each(function (i, val) {
                if (i == 0) {
                    cantidad = $(this).children().val();
                } else if (i == 1) {
                    idproducto = $(this).attr("data-content");
                } else if (i == 2) {
                    precio = $(this).text();
                } else if (i == 3) {
                    comentario = $(this).children().val();
                } else if (i == 4) {
                    subtotal = $(this).text();
                }
            });
            let comanda = new Object();
            comanda.cantidad = cantidad;
            comanda.idproducto = idproducto;
            comanda.precio = accounting.unformat(precio);
            comanda.comentario = comentario;
            comanda.subtotal = accounting.unformat(subtotal);
            comanda.status = 1;
            comanda.idventa = $("#idventa").val();
            lista_comandas.push(comanda);
        });
        if (lista_comandas.length == 0) {
            alert("La comanda al menos debe de tener un producto");
        } else {
            lista_comandas = JSON.stringify(lista_comandas);
            $.ajax({
                type: "POST",
                url: "../../Comandas/insert",
                data: {
                    comanda: lista_comandas
                },
                dataType: "json",
                success: function (response) {
                    let comanda = response;
                    console.log(comanda);
                    /*window.location = "../../ventas";
                    window.open("../../Comandas/comanda_print_cocina/" + $("#idventa").val(), '_blank');
                    window.open("../../Comandas/comanda_print_barra/" + $("#idventa").val(), '_blank');*/
                    /*window.open("../../Comandas/comanda_print_cocina/" + $("#idventa").val(), '_blank');
                    window.open("../../Comandas/comanda_print_barra/" + $("#idventa").val(), '_blank');*/
                    console.log("Imprimirmos Ticket Cocina")
                    let n_impresora = comanda['info']['impresora_cocina'];
                    let cocina = new ConectorPlugin();

                    cocina.establecerTamanioFuente(1, 1);
                    cocina.establecerEnfatizado(0);

                    if (comanda['cocina'].length > 0) {
                        cocina.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                        cocina.feed(1);
                        cocina.texto("COMANDA PARA COCINA\n");
                        cocina.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                        cocina.feed(1);
                        cocina.texto(`NO. MESA ${comanda['info']['no_mesa']} (${comanda['info']['ubicacion']})\n`);
                        cocina.feed(1);
                        cocina.texto(`FECHA/HORA  ${comanda['info']['fecha']}\n`);

                        $.each(comanda['cocina'], function (i, cb) {
                            cocina.texto(`CANT. ${cb.cantidad}\n`);
                            cocina.texto(`PEDIDO. ${cb.producto}\n`);
                            cocina.texto(`COMENTARIO. ${cb.comentario}\n`);
                            cocina.feed(2);
                        });
                        cocina.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                        cocina.texto("***__________________***");
                        cocina.feed(4);
                        cocina.cortar();
                        //conector.cortarParcialmente();
                    }

                    cocina.imprimirEn(n_impresora)
                        .then(respuestaAlImprimir => {
                            if (respuestaAlImprimir === true) {
                                console.log("Impreso correctamente");
                            } else {
                                console.log("Error. La respuesta es: " + respuestaAlImprimir);
                            }
                        });

                    n_impresora = comanda['info']['impresora_barra'];
                    let barra = new ConectorPlugin();
                    barra.establecerTamanioFuente(1, 1);
                    barra.establecerEnfatizado(0);
                    if (comanda['barra'].length > 0) {
                        barra.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                        barra.feed(1);
                        barra.texto("COMANDA PARA BARRA\n");
                        barra.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                        barra.feed(1);
                        barra.texto(`NO. MESA ${comanda['info']['no_mesa']} (${comanda['info']['ubicacion']})\n`);
                        barra.feed(1);
                        barra.texto(`FECHA/HORA  ${comanda['info']['fecha']}\n`);
                        $.each(comanda['barra'], function (i, cb) {
                            barra.texto(`CANT. ${cb.cantidad}\n`);
                            barra.texto(`PEDIDO. ${cb.producto}\n`);
                            barra.texto(`COMENTARIO. ${cb.comentario}\n`);
                            barra.feed(2);
                        });

                        barra.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                        barra.texto("***__________________***");
                        barra.feed(4);
                        barra.cortar();
                        //conector.cortarParcialmente();
                    }

                    barra.imprimirEn(n_impresora)
                        .then(respuestaAlImprimir => {
                            if (respuestaAlImprimir === true) {
                                console.log("Impreso correctamente");
                            } else {
                                console.log("Error. La respuesta es: " + respuestaAlImprimir);
                            }
                        });

                    window.location = "../../ventas";
                }
            });
        }



    });


});