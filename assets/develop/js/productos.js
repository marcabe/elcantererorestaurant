$(document).ready(function () {

    $("#precio").on("blur", function () {
        $(this).val(accounting.formatMoney($(this).val()));
    });

    $(".producto_fila").on("click", function(){
        let idproducto = $(this).attr("data-content");
        $.ajax({
            type: "POST",
            url: "../Productos/get_id/"+idproducto,
            dataType: "json",
            success: function (response) {
                let producto_ = response;
                $("#formulario_nuevo_producto").show();
                $("#itme_listado_productos").hide();
                $("#editar_producto").show();
                $("#guardar_producto").hide();

                $("#producto").val(producto_.producto);
                $("#precio").val(accounting.formatMoney(producto_.precio));
                $("#descripcion").val(producto_.descripcion);
                $("#idcategoria").val(producto_.idcategoria);
                $("#favorito").val(producto_.favorito);
                $("#editar_producto").attr("data-content", idproducto);
            }
        });
    })

    $("#tbl_productos tr").on("click", function() {
        $("#tbl_productos tr").removeClass("highlight");
        $(this).addClass("highlight");
    });

    $(".fa-star").on("click", function() {
        let idproducto = $(this).attr("data-content");
        let favorito = 0;
        if ($(this).hasClass("far")) {
            $(this).removeClass("far");
            $(this).addClass("fas");
            favorito = 1;
        } else if ($(this).hasClass("fas")) {
            $(this).removeClass("fas");
            $(this).addClass("far");
            favorito = 0;
        }
        $.ajax({
            type: "POST",
            url: "../Productos/update/"+idproducto,
            data: {
                favorito: parseInt(favorito)
            }
        });
    });


    $('#tbl_productos').DataTable({
        responsive: true,
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "ordering": false,

        initComplete: function() {
            this.api().columns().every(function() {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function() {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });
                column.data().unique().sort().each(function(d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });

    $("#nuevo_producto").on("click", function() {
        $("#itme_listado_productos").hide();
        $("#formulario_nueva_categoria").hide();
        $("#formulario_nuevo_producto").show();
        $("#itme_listado").hide();
        $("input").val('');
        $("textarea").val('');
        $("select").val(0);
        
        $("#editar_producto").hide();
        $("#guardar_producto").show();
    });

    $("#cancelar_guardad_producto").on("click", function() {
        $("#formulario_nuevo_producto").hide();
        $("#itme_listado_productos").show();
    });

    $("#guardar_producto").on("click", function() {
        let producto = $("#producto").val();
        let precio = $("#precio").val();
        let descripcion = $("#descripcion").val();
        let idcategoria = $("#idcategoria").val();
        let favorito = $("#favorito").val();

        if (producto == '' || precio == '' || idcategoria == 0) {
            $("#alert_danger").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger").show();
            $("#alert_danger").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Productos/insert",
                data: {
                    producto: producto,
                    precio: accounting.unformat(precio),
                    descripcion: descripcion,
                    idcategoria: idcategoria,
                    favorito: favorito,
                    status:1
                },
                success: function(response) {
                    alert("Producto ingresado correctamente.");
                    location.reload();
                }
            });
        }
    });


    $("#editar_producto").on("click", function() {
        let idproducto = $(this).attr("data-content");
        let producto = $("#producto").val();
        let precio = $("#precio").val();
        let descripcion = $("#descripcion").val();
        let idcategoria = $("#idcategoria").val();
        let favorito = $("#favorito").val();

        if (producto == '' || precio == '' || idcategoria == 0) {
            $("#alert_danger").text("Verificar que todos los datos esten ingresados");
            $("#alert_danger").show();
            $("#alert_danger").fadeOut(3500);
        } else {
            $.ajax({
                type: "POST",
                url: "../Productos/update/"+idproducto,
                data: {
                    producto: producto,
                    precio: accounting.unformat(precio),
                    descripcion: descripcion,
                    idcategoria: idcategoria,
                    favorito: favorito,
                    status:1
                },
                success: function(response) {
                    alert("Producto editado correctamente.");
                    location.reload();
                }
            });
        }
    });




});