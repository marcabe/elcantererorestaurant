$(document).ready(function () {


    $("#consultar_apertura").on("click", function () {
        let fi = $("#fc_caj").val();
        $.ajax({
            type: "POST",
            url: "../Apertura/search/" + fi,
            dataType: "json",
            success: function (response) {
                let respuesta = response;
                $("#fila_aperturas").empty();
                let fila = ``;
                $.each(respuesta, function (i, e) {
                    e.usuario_cierre = (e.usuario_cierre === null) ? '-------' : e.usuario_cierre;
                    e.monto_cierre = (e.monto_cierre === null) ? '-------' : e.monto_cierre;
                    e.monto_cierre_p = (e.monto_cierre_p === null) ? '-------' : e.monto_cierre_p;
                    fila += `<tr class="imprimir_apertura" data-idapertura=${e.idapertura_principal}>
                                <td>${e.usuario_apertura}</td>
                                <td >${e.hora_inicio}</td>
                                <td>${e.usuario_cierre}</td>
                                <td>${e.hora_final}</td>
                                <td>${e.monto_cierre}</td>
                                <td>${e.monto_cierre_p}</td>
                            </tr>`
                });
                console.log(fila);
                $("#fila_aperturas").append(fila);
            }
        });
    });

    $(document).on("click", ".imprimir_apertura", function (e) {
        //window.open("../Apertura/imprimir/" + $(this).attr("data-idapertura"), '_blank');
        $.ajax({
            type: "POST",
            url: "../Apertura/ticket_corte/" + $(this).attr("data-idapertura"),
            data: "data",
            dataType: "json",
            success: function (response) {
                let cierre = response;
                console.log(cierre);
                console.log("Imprimirmos Ticket Cocina")
                let n_impresora = cierre['info']['impresora_corte'];
                let ticket_c = new ConectorPlugin();

                ticket_c.establecerTamanioFuente(1, 1);
                ticket_c.establecerEnfatizado(0);

                ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                ticket_c.feed(1);
                ticket_c.texto("CORTE DÍA\n");
                ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                ticket_c.feed(1);
                ticket_c.texto(`  FECHA/HORA  ${cierre['info']['fecha']}\n`);
                ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                ticket_c.feed(1);
                ticket_c.texto("============================\n");
                ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                ticket_c.feed(1);
                $.each(cierre['suba'], function (i, c) {
                    ticket_c.texto(`  EN CAJA.                  $ ${c.en_caja}\n`);
                    ticket_c.texto(`  MONTO VENTA.              $ ${c.monto_venta}\n`);
                    ticket_c.texto(`  MONTO PEDIDO.             $ ${c.monto_pedido}\n`);
                    ticket_c.texto(`  MONTO APERTURA.           $ ${c.monto_apertura}\n`);
                    ticket_c.texto(`  PAGO PROVEEDOR.           $ ${c.pagos_provedor}\n`);
                    ticket_c.feed(2);

                    /*
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                    ticket_c.feed(1);
                    ticket_c.texto("=======VENTAS EN TURNO=======\n");
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                    ticket_c.feed(1);

                    $.each(c['ventas'], function (i, ventas) {
                        ticket_c.texto(`  No. Mesa: ${ventas.mesas.nomesa} (${ventas.mesas.croquis})\n`);
                        ticket_c.texto(`  Mesero: ${ventas.meseros.nombre}\n`);
                        ticket_c.texto(`  Propina: $ ${ventas.monto_propina}\n`);
                        ticket_c.texto(`  Total: $ ${ventas.total}\n`);
                        ticket_c.feed(1);
                    });
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                    ticket_c.texto(`Total: $ ${c.monto_venta}\n`);

                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                    ticket_c.feed(1);
                    ticket_c.texto("=======PEDIDOS EN TURNO=======\n");
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                    ticket_c.feed(1);

                    $.each(c['pedidos'], function (i, pedidos) {
                        ticket_c.texto(`  Fecha: ${pedidos.hora_inicio}\n`);
                        ticket_c.texto(`  Mesero: ${pedidos.meseros.nombre}\n`);
                        ticket_c.texto(`  Total: $ ${pedidos.total}\n`);
                        ticket_c.feed(1);
                    });
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                    ticket_c.texto(`Total: $ ${c.monto_pedido}\n`);


                    */
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                    ticket_c.feed(1);
                    ticket_c.texto("=======PAGOS A PROVEEDOR=======\n");
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionIzquierda);
                    ticket_c.feed(1);

                    $.each(c['pagos'], function (i, pagos) {
                        ticket_c.texto(`  Fecha: ${pagos.fecha}\n`);
                        ticket_c.texto(`  Fecha: ${pagos.proveedor.nombre}\n`);
                        ticket_c.texto(`  Comentarios: ${pagos.descripcion}\n`);
                        ticket_c.texto(`  Total: $ ${pagos.monto}\n`);
                        ticket_c.feed(1);
                    });
                    ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionDerecha);
                    ticket_c.texto(`Total: $ ${c.pagos_provedor}\n`);

                });
                ticket_c.establecerJustificacion(ConectorPlugin.Constantes.AlineacionCentro);
                ticket_c.texto("***__________________***");
                ticket_c.feed(4);
                ticket_c.cortar();

                ticket_c.imprimirEn(n_impresora)
                    .then(respuestaAlImprimir => {
                        if (respuestaAlImprimir === true) {
                            console.log("Impreso correctamente");
                        } else {
                            console.log("Error. La respuesta es: " + respuestaAlImprimir);
                        }
                    });



            }
        });

    });




});