/*
Navicat MySQL Data Transfer

Source Server         : Creando Pixeles Local
Source Server Version : 100413
Source Host           : localhost:3306
Source Database       : restaurant

Target Server Type    : MYSQL
Target Server Version : 100413
File Encoding         : 65001

Date: 2022-01-22 00:31:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for apertura
-- ----------------------------
DROP TABLE IF EXISTS `apertura`;
CREATE TABLE `apertura` (
  `idapertura_principal` int(5) NOT NULL AUTO_INCREMENT,
  `hora_inicio` timestamp NOT NULL DEFAULT current_timestamp(),
  `usuario_apertura` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `status` int(1) NOT NULL,
  `hora_final` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `usuario_cierre` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto_cierre` decimal(10,2) DEFAULT NULL,
  `monto_cierre_p` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`idapertura_principal`),
  KEY `fk_aperturapri_ua` (`usuario_apertura`),
  KEY `fk_aperturapri_uc` (`usuario_cierre`),
  CONSTRAINT `fk_aperturapri_ua` FOREIGN KEY (`usuario_apertura`) REFERENCES `usuarios` (`usuario`) ON UPDATE CASCADE,
  CONSTRAINT `fk_aperturapri_uc` FOREIGN KEY (`usuario_cierre`) REFERENCES `usuarios` (`usuario`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for cancelaciones_producto_pedido
-- ----------------------------
DROP TABLE IF EXISTS `cancelaciones_producto_pedido`;
CREATE TABLE `cancelaciones_producto_pedido` (
  `idcancelacion` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `idcomanda` int(2) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idcancelacion`),
  KEY `fk_cancelacion_usuario` (`usuario`),
  KEY `fk_cancelacion_comanda` (`idcomanda`),
  CONSTRAINT `cancelaciones_producto_pedido_ibfk_1` FOREIGN KEY (`idcomanda`) REFERENCES `comandas_pedidos` (`comanda_pedido`) ON UPDATE CASCADE,
  CONSTRAINT `cancelaciones_producto_pedido_ibfk_2` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`usuario`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for cancelaciones_producto_venta
-- ----------------------------
DROP TABLE IF EXISTS `cancelaciones_producto_venta`;
CREATE TABLE `cancelaciones_producto_venta` (
  `idcancelacion` int(11) NOT NULL AUTO_INCREMENT,
  `motivo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `idcomanda` int(2) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idcancelacion`),
  KEY `fk_cancelacion_usuario` (`usuario`),
  KEY `fk_cancelacion_comanda` (`idcomanda`),
  CONSTRAINT `fk_cancelacion_comanda` FOREIGN KEY (`idcomanda`) REFERENCES `comandas` (`comanda`) ON UPDATE CASCADE,
  CONSTRAINT `fk_cancelacion_usuario` FOREIGN KEY (`usuario`) REFERENCES `usuarios` (`usuario`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for categoria_productos
-- ----------------------------
DROP TABLE IF EXISTS `categoria_productos`;
CREATE TABLE `categoria_productos` (
  `idcategoria` int(2) NOT NULL AUTO_INCREMENT,
  `categoria` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `status` int(1) NOT NULL,
  `lugar` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for clientes
-- ----------------------------
DROP TABLE IF EXISTS `clientes`;
CREATE TABLE `clientes` (
  `idcliente` int(2) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`idcliente`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for comandas
-- ----------------------------
DROP TABLE IF EXISTS `comandas`;
CREATE TABLE `comandas` (
  `comanda` int(2) NOT NULL AUTO_INCREMENT,
  `idventa` int(5) NOT NULL,
  `idproducto` int(2) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `cantidad` int(2) NOT NULL,
  `comentario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `status` int(1) NOT NULL,
  `status_comanda` int(11) NOT NULL,
  PRIMARY KEY (`comanda`),
  KEY `fk_ventas_comanda` (`idventa`),
  KEY `fk_ventas_productos` (`idproducto`),
  CONSTRAINT `fk_ventas_comanda` FOREIGN KEY (`idventa`) REFERENCES `ventas` (`idventa`) ON UPDATE CASCADE,
  CONSTRAINT `fk_ventas_productos` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`idproducto`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=562 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for comandas_pedidos
-- ----------------------------
DROP TABLE IF EXISTS `comandas_pedidos`;
CREATE TABLE `comandas_pedidos` (
  `comanda_pedido` int(2) NOT NULL AUTO_INCREMENT,
  `idpedido` int(2) NOT NULL,
  `idproducto` int(2) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `cantidad` int(2) NOT NULL,
  `comentario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `subtotal` decimal(10,2) NOT NULL,
  `status` bit(1) NOT NULL,
  `status_comanda` int(11) NOT NULL,
  PRIMARY KEY (`comanda_pedido`),
  KEY `fk_comanda_pedido_pedido` (`idpedido`),
  KEY `fk_comanda_pedido_producto` (`idproducto`),
  CONSTRAINT `fk_comanda_pedido_pedido` FOREIGN KEY (`idpedido`) REFERENCES `pedidos` (`idpedido`) ON UPDATE CASCADE,
  CONSTRAINT `fk_comanda_pedido_producto` FOREIGN KEY (`idproducto`) REFERENCES `productos` (`idproducto`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `impresora_barra` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `impresora_cocina` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `impresora_corte` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for faltantes
-- ----------------------------
DROP TABLE IF EXISTS `faltantes`;
CREATE TABLE `faltantes` (
  `idfaltante` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`idfaltante`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for listafaltantes
-- ----------------------------
DROP TABLE IF EXISTS `listafaltantes`;
CREATE TABLE `listafaltantes` (
  `idlistaf` int(11) NOT NULL AUTO_INCREMENT,
  `idfaltante` int(11) NOT NULL,
  `producto` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idlistaf`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for mesas
-- ----------------------------
DROP TABLE IF EXISTS `mesas`;
CREATE TABLE `mesas` (
  `idmesa` int(2) NOT NULL AUTO_INCREMENT,
  `nomesa` int(2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `croquis` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `imagen` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `imagen_ocupada` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idmesa`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for meseros
-- ----------------------------
DROP TABLE IF EXISTS `meseros`;
CREATE TABLE `meseros` (
  `idmesero` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(75) COLLATE utf8_spanish_ci NOT NULL,
  `celular` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`idmesero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for pedidos
-- ----------------------------
DROP TABLE IF EXISTS `pedidos`;
CREATE TABLE `pedidos` (
  `idpedido` int(5) NOT NULL AUTO_INCREMENT,
  `hora_inicio` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_final` date DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `status` int(1) NOT NULL,
  `idmesero` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `idcliente` int(2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `metodo_pago` int(2) NOT NULL,
  `idsubapertura` int(5) DEFAULT NULL,
  `cuanto_pago` decimal(10,2) NOT NULL,
  `cambio` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idpedido`),
  KEY `fk_venta_cliente` (`idcliente`),
  KEY `fk_venta_mesero` (`idmesero`),
  KEY `fk_pedidos_subapertura` (`idsubapertura`),
  CONSTRAINT `fk_pedidos_cliente` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`idcliente`) ON UPDATE CASCADE,
  CONSTRAINT `fk_pedidos_mesero` FOREIGN KEY (`idmesero`) REFERENCES `meseros` (`idmesero`) ON UPDATE CASCADE,
  CONSTRAINT `fk_pedidos_subapertura` FOREIGN KEY (`idsubapertura`) REFERENCES `sub_apertura` (`idsub_apertura`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `idproducto` int(2) NOT NULL AUTO_INCREMENT,
  `producto` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `favorito` int(1) NOT NULL,
  `idcategoria` int(2) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`idproducto`),
  KEY `fk_productos_categorias` (`idcategoria`),
  CONSTRAINT `fk_productos_categorias` FOREIGN KEY (`idcategoria`) REFERENCES `categoria_productos` (`idcategoria`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for proveedores
-- ----------------------------
DROP TABLE IF EXISTS `proveedores`;
CREATE TABLE `proveedores` (
  `idpro` int(2) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`idpro`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for reg_pago_provedor
-- ----------------------------
DROP TABLE IF EXISTS `reg_pago_provedor`;
CREATE TABLE `reg_pago_provedor` (
  `idreg` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `idsuba` int(10) NOT NULL,
  `idproveedor` int(2) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto` decimal(10,2) NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idreg`),
  KEY `fk_reg_pp_suba` (`idsuba`),
  KEY `fk_reg_provedor` (`idproveedor`),
  CONSTRAINT `fk_reg_pp_suba` FOREIGN KEY (`idsuba`) REFERENCES `sub_apertura` (`idsub_apertura`) ON UPDATE CASCADE,
  CONSTRAINT `fk_reg_provedor` FOREIGN KEY (`idproveedor`) REFERENCES `proveedores` (`idpro`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for sub_apertura
-- ----------------------------
DROP TABLE IF EXISTS `sub_apertura`;
CREATE TABLE `sub_apertura` (
  `idsub_apertura` int(10) NOT NULL AUTO_INCREMENT,
  `idapertura` int(5) NOT NULL,
  `hora_inicio` timestamp NOT NULL DEFAULT current_timestamp(),
  `usuario_apertura` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_cierre` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `usuario_cierre` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto_venta` decimal(10,2) DEFAULT NULL,
  `monto_pedido` decimal(10,2) DEFAULT NULL,
  `monto_apertura` decimal(10,2) DEFAULT NULL,
  `pagos_provedor` decimal(10,2) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`idsub_apertura`),
  KEY `fk_subapertura_apertura` (`idapertura`),
  KEY `fk_subapertura_ua` (`usuario_apertura`),
  KEY `fk_subapertura_uc` (`usuario_cierre`),
  CONSTRAINT `fk_subapertura_apertura` FOREIGN KEY (`idapertura`) REFERENCES `apertura` (`idapertura_principal`) ON UPDATE CASCADE,
  CONSTRAINT `fk_subapertura_ua` FOREIGN KEY (`usuario_apertura`) REFERENCES `usuarios` (`usuario`) ON UPDATE CASCADE,
  CONSTRAINT `fk_subapertura_uc` FOREIGN KEY (`usuario_cierre`) REFERENCES `usuarios` (`usuario`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for tipo_usuario
-- ----------------------------
DROP TABLE IF EXISTS `tipo_usuario`;
CREATE TABLE `tipo_usuario` (
  `idtipousuario` int(2) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`idtipousuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` int(2) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`usuario`),
  KEY `fk_usuario_tipo` (`tipo`),
  CONSTRAINT `fk_usuario_tipo` FOREIGN KEY (`tipo`) REFERENCES `tipo_usuario` (`idtipousuario`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- ----------------------------
-- Table structure for ventas
-- ----------------------------
DROP TABLE IF EXISTS `ventas`;
CREATE TABLE `ventas` (
  `idventa` int(5) NOT NULL AUTO_INCREMENT,
  `hora_inicio` timestamp NOT NULL DEFAULT current_timestamp(),
  `fecha_final` date DEFAULT NULL,
  `hora_final` time DEFAULT NULL,
  `status` int(1) NOT NULL,
  `idmesa` int(2) NOT NULL,
  `idmesero` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `idcliente` int(2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `metodo_pago` int(2) NOT NULL,
  `propina` bit(1) NOT NULL DEFAULT b'0',
  `monto_propina` decimal(10,2) NOT NULL,
  `porcentaje_propina` decimal(10,2) NOT NULL,
  `personas` int(10) NOT NULL,
  `idsubapertura` int(5) NOT NULL,
  `cuanto_pago` decimal(10,2) NOT NULL,
  `cambio` decimal(10,2) NOT NULL,
  PRIMARY KEY (`idventa`),
  KEY `fk_venta_cliente` (`idcliente`),
  KEY `fk_venta_mesero` (`idmesero`),
  KEY `fk_venta_idsuba` (`idsubapertura`),
  KEY `fk_venta_mesa` (`idmesa`),
  CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`idcliente`) REFERENCES `clientes` (`idcliente`) ON UPDATE CASCADE,
  CONSTRAINT `fk_venta_idsuba` FOREIGN KEY (`idsubapertura`) REFERENCES `sub_apertura` (`idsub_apertura`) ON UPDATE CASCADE,
  CONSTRAINT `fk_venta_mesa` FOREIGN KEY (`idmesa`) REFERENCES `mesas` (`idmesa`) ON UPDATE CASCADE,
  CONSTRAINT `fk_venta_mesero` FOREIGN KEY (`idmesero`) REFERENCES `meseros` (`idmesero`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
